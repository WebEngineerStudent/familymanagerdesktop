﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FamilyManager
{
    class LoginUsernameException : Exception
    {
        //Declaration of the bad username variable
        private string badUsername;
        //Declaration of the message variable
        private string message;

        //Declaration of the constructor
        public LoginUsernameException(string badUsername, string message) : base(message)
        {
            this.badUsername = badUsername;
        }

        //Declaration of the getter
        public string getBadUsername()
        {
            return badUsername;
        }
    }
}
