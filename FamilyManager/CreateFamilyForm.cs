﻿using MaterialSkin.Controls;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FamilyManager
{
    public partial class CreateFamilyForm : MaterialForm
    {
        public string familyName;

        public CreateFamilyForm()
        {
            InitializeComponent();
        }

        private void createTheFamilyButton_Click(object sender, EventArgs e)
        {
            familyName = createFamilyNameInput.Text;
            createTheFamilyButton.DialogResult = DialogResult.OK;
            createTheFamilyButton.PerformClick();
        }
     }
}
