﻿namespace FamilyManager
{
    partial class CreateFamilyForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.inputPanel = new System.Windows.Forms.Panel();
            this.cancelCreateFamily = new System.Windows.Forms.Button();
            this.createTheFamilyButton = new System.Windows.Forms.Button();
            this.familyNameLabel = new System.Windows.Forms.Label();
            this.createFamilyNameInput = new System.Windows.Forms.TextBox();
            this.createFamilyIcon = new System.Windows.Forms.PictureBox();
            this.errorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this.inputPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.createFamilyIcon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            this.SuspendLayout();
            // 
            // inputPanel
            // 
            this.inputPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.inputPanel.BackColor = System.Drawing.SystemColors.ControlDark;
            this.inputPanel.Controls.Add(this.cancelCreateFamily);
            this.inputPanel.Controls.Add(this.createTheFamilyButton);
            this.inputPanel.Controls.Add(this.familyNameLabel);
            this.inputPanel.Controls.Add(this.createFamilyNameInput);
            this.inputPanel.Location = new System.Drawing.Point(214, 283);
            this.inputPanel.Name = "inputPanel";
            this.inputPanel.Size = new System.Drawing.Size(349, 152);
            this.inputPanel.TabIndex = 3;
            // 
            // cancelCreateFamily
            // 
            this.cancelCreateFamily.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cancelCreateFamily.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelCreateFamily.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.cancelCreateFamily.Location = new System.Drawing.Point(194, 112);
            this.cancelCreateFamily.Name = "cancelCreateFamily";
            this.cancelCreateFamily.Size = new System.Drawing.Size(152, 37);
            this.cancelCreateFamily.TabIndex = 3;
            this.cancelCreateFamily.Text = "Mégse";
            this.cancelCreateFamily.UseVisualStyleBackColor = true;
            // 
            // createTheFamilyButton
            // 
            this.createTheFamilyButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.createTheFamilyButton.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.createTheFamilyButton.Location = new System.Drawing.Point(4, 112);
            this.createTheFamilyButton.Name = "createTheFamilyButton";
            this.createTheFamilyButton.Size = new System.Drawing.Size(152, 37);
            this.createTheFamilyButton.TabIndex = 2;
            this.createTheFamilyButton.Text = "+ Család létrehozása";
            this.createTheFamilyButton.UseVisualStyleBackColor = true;
            this.createTheFamilyButton.Click += new System.EventHandler(this.createTheFamilyButton_Click);
            // 
            // familyNameLabel
            // 
            this.familyNameLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.familyNameLabel.AutoSize = true;
            this.familyNameLabel.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.familyNameLabel.Location = new System.Drawing.Point(107, 15);
            this.familyNameLabel.Name = "familyNameLabel";
            this.familyNameLabel.Size = new System.Drawing.Size(121, 25);
            this.familyNameLabel.TabIndex = 1;
            this.familyNameLabel.Text = "Család neve:";
            // 
            // createFamilyNameInput
            // 
            this.createFamilyNameInput.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.createFamilyNameInput.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.createFamilyNameInput.Location = new System.Drawing.Point(89, 57);
            this.createFamilyNameInput.Name = "createFamilyNameInput";
            this.createFamilyNameInput.Size = new System.Drawing.Size(171, 29);
            this.createFamilyNameInput.TabIndex = 0;
            // 
            // createFamilyIcon
            // 
            this.createFamilyIcon.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.createFamilyIcon.BackgroundImage = global::FamilyManager.Properties.Resources.createFamilyIcon;
            this.createFamilyIcon.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.createFamilyIcon.Location = new System.Drawing.Point(290, 73);
            this.createFamilyIcon.Name = "createFamilyIcon";
            this.createFamilyIcon.Size = new System.Drawing.Size(184, 157);
            this.createFamilyIcon.TabIndex = 2;
            this.createFamilyIcon.TabStop = false;
            // 
            // errorProvider
            // 
            this.errorProvider.ContainerControl = this;
            // 
            // CreateFamilyForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.inputPanel);
            this.Controls.Add(this.createFamilyIcon);
            this.Name = "CreateFamilyForm";
            this.Text = "CreateFamilyForm";
            this.inputPanel.ResumeLayout(false);
            this.inputPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.createFamilyIcon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel inputPanel;
        private System.Windows.Forms.Button cancelCreateFamily;
        private System.Windows.Forms.Button createTheFamilyButton;
        private System.Windows.Forms.Label familyNameLabel;
        private System.Windows.Forms.TextBox createFamilyNameInput;
        private System.Windows.Forms.PictureBox createFamilyIcon;
        private System.Windows.Forms.ErrorProvider errorProvider;
    }
}