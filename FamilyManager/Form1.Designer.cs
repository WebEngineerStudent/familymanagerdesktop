﻿namespace FamilyManager
{
    partial class LoadingForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl = new MaterialSkin.Controls.MaterialTabControl();
            this.ModeSelectorTab = new System.Windows.Forms.TabPage();
            this.exitButton = new MaterialSkin.Controls.MaterialRaisedButton();
            this.userButton = new MaterialSkin.Controls.MaterialRaisedButton();
            this.adminButton = new MaterialSkin.Controls.MaterialRaisedButton();
            this.userPicture = new System.Windows.Forms.PictureBox();
            this.adminPicture = new System.Windows.Forms.PictureBox();
            this.modeLabel = new System.Windows.Forms.Label();
            this.UserLoginTab = new System.Windows.Forms.TabPage();
            this.registerButton = new MaterialSkin.Controls.MaterialRaisedButton();
            this.loginButton = new MaterialSkin.Controls.MaterialRaisedButton();
            this.backButton = new MaterialSkin.Controls.MaterialRaisedButton();
            this.passwordInput = new System.Windows.Forms.TextBox();
            this.usernameInput = new System.Windows.Forms.TextBox();
            this.passwordLabel = new System.Windows.Forms.Label();
            this.usernameLabel = new System.Windows.Forms.Label();
            this.userIconPicture = new System.Windows.Forms.PictureBox();
            this.loginLabel = new System.Windows.Forms.Label();
            this.AdminLoginTab = new System.Windows.Forms.TabPage();
            this.adminLoginButton = new MaterialSkin.Controls.MaterialRaisedButton();
            this.backButton2 = new MaterialSkin.Controls.MaterialRaisedButton();
            this.adminPasswordInput = new System.Windows.Forms.TextBox();
            this.adminUsernameInput = new System.Windows.Forms.TextBox();
            this.adminPasswordLabel = new System.Windows.Forms.Label();
            this.adminUsernameLabel = new System.Windows.Forms.Label();
            this.adminIconPicture = new System.Windows.Forms.PictureBox();
            this.login2Label = new System.Windows.Forms.Label();
            this.RegisterTab = new System.Windows.Forms.TabPage();
            this.positionInput = new System.Windows.Forms.TextBox();
            this.emailInput = new System.Windows.Forms.TextBox();
            this.positionLabel = new System.Windows.Forms.Label();
            this.emailLabel = new System.Windows.Forms.Label();
            this.registerPasswordInput = new System.Windows.Forms.TextBox();
            this.registerUsernameInput = new System.Windows.Forms.TextBox();
            this.registerPasswordLabel = new System.Windows.Forms.Label();
            this.registerUsernameLabel = new System.Windows.Forms.Label();
            this.registrationButton = new MaterialSkin.Controls.MaterialRaisedButton();
            this.backButton3 = new MaterialSkin.Controls.MaterialRaisedButton();
            this.lastNameInput = new System.Windows.Forms.TextBox();
            this.firstNameInput = new System.Windows.Forms.TextBox();
            this.lastNameLabel = new System.Windows.Forms.Label();
            this.firstNameLabel = new System.Windows.Forms.Label();
            this.registerLabel = new System.Windows.Forms.Label();
            this.tabControl.SuspendLayout();
            this.ModeSelectorTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.userPicture)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.adminPicture)).BeginInit();
            this.UserLoginTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.userIconPicture)).BeginInit();
            this.AdminLoginTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.adminIconPicture)).BeginInit();
            this.RegisterTab.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl
            // 
            this.tabControl.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tabControl.Controls.Add(this.ModeSelectorTab);
            this.tabControl.Controls.Add(this.UserLoginTab);
            this.tabControl.Controls.Add(this.AdminLoginTab);
            this.tabControl.Controls.Add(this.RegisterTab);
            this.tabControl.Depth = 0;
            this.tabControl.Location = new System.Drawing.Point(0, 64);
            this.tabControl.MouseState = MaterialSkin.MouseState.HOVER;
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(800, 388);
            this.tabControl.TabIndex = 0;
            // 
            // ModeSelectorTab
            // 
            this.ModeSelectorTab.Controls.Add(this.exitButton);
            this.ModeSelectorTab.Controls.Add(this.userButton);
            this.ModeSelectorTab.Controls.Add(this.adminButton);
            this.ModeSelectorTab.Controls.Add(this.userPicture);
            this.ModeSelectorTab.Controls.Add(this.adminPicture);
            this.ModeSelectorTab.Controls.Add(this.modeLabel);
            this.ModeSelectorTab.Location = new System.Drawing.Point(4, 22);
            this.ModeSelectorTab.Name = "ModeSelectorTab";
            this.ModeSelectorTab.Padding = new System.Windows.Forms.Padding(3);
            this.ModeSelectorTab.Size = new System.Drawing.Size(792, 362);
            this.ModeSelectorTab.TabIndex = 0;
            this.ModeSelectorTab.Text = "ModeSelector";
            this.ModeSelectorTab.UseVisualStyleBackColor = true;
            // 
            // exitButton
            // 
            this.exitButton.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.exitButton.BackColor = System.Drawing.Color.DarkCyan;
            this.exitButton.Depth = 0;
            this.exitButton.Location = new System.Drawing.Point(339, 321);
            this.exitButton.MouseState = MaterialSkin.MouseState.HOVER;
            this.exitButton.Name = "exitButton";
            this.exitButton.Primary = true;
            this.exitButton.Size = new System.Drawing.Size(135, 31);
            this.exitButton.TabIndex = 5;
            this.exitButton.Text = "Kilépés";
            this.exitButton.UseVisualStyleBackColor = false;
            this.exitButton.Click += new System.EventHandler(this.exitButton_Click);
            // 
            // userButton
            // 
            this.userButton.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.userButton.BackColor = System.Drawing.Color.DarkCyan;
            this.userButton.Depth = 0;
            this.userButton.Location = new System.Drawing.Point(562, 239);
            this.userButton.MouseState = MaterialSkin.MouseState.HOVER;
            this.userButton.Name = "userButton";
            this.userButton.Primary = true;
            this.userButton.Size = new System.Drawing.Size(135, 31);
            this.userButton.TabIndex = 4;
            this.userButton.Text = "Felhasználó";
            this.userButton.UseVisualStyleBackColor = false;
            this.userButton.Click += new System.EventHandler(this.userButton_Click);
            // 
            // adminButton
            // 
            this.adminButton.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.adminButton.BackColor = System.Drawing.Color.DarkCyan;
            this.adminButton.Depth = 0;
            this.adminButton.Location = new System.Drawing.Point(93, 239);
            this.adminButton.MouseState = MaterialSkin.MouseState.HOVER;
            this.adminButton.Name = "adminButton";
            this.adminButton.Primary = true;
            this.adminButton.Size = new System.Drawing.Size(135, 31);
            this.adminButton.TabIndex = 3;
            this.adminButton.Text = "Rendszergazda";
            this.adminButton.UseVisualStyleBackColor = false;
            this.adminButton.Click += new System.EventHandler(this.adminButton_Click);
            // 
            // userPicture
            // 
            this.userPicture.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.userPicture.BackgroundImage = global::FamilyManager.Properties.Resources.userIcon;
            this.userPicture.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.userPicture.Location = new System.Drawing.Point(562, 82);
            this.userPicture.Name = "userPicture";
            this.userPicture.Size = new System.Drawing.Size(135, 125);
            this.userPicture.TabIndex = 2;
            this.userPicture.TabStop = false;
            // 
            // adminPicture
            // 
            this.adminPicture.BackgroundImage = global::FamilyManager.Properties.Resources.adminIcon;
            this.adminPicture.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.adminPicture.Location = new System.Drawing.Point(93, 82);
            this.adminPicture.Name = "adminPicture";
            this.adminPicture.Size = new System.Drawing.Size(135, 125);
            this.adminPicture.TabIndex = 1;
            this.adminPicture.TabStop = false;
            // 
            // modeLabel
            // 
            this.modeLabel.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.modeLabel.AutoSize = true;
            this.modeLabel.Font = new System.Drawing.Font("Segoe UI", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.modeLabel.Location = new System.Drawing.Point(111, 0);
            this.modeLabel.Name = "modeLabel";
            this.modeLabel.Size = new System.Drawing.Size(553, 47);
            this.modeLabel.TabIndex = 0;
            this.modeLabel.Text = "A felhasználási mód kiválasztása";
            this.modeLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // UserLoginTab
            // 
            this.UserLoginTab.Controls.Add(this.registerButton);
            this.UserLoginTab.Controls.Add(this.loginButton);
            this.UserLoginTab.Controls.Add(this.backButton);
            this.UserLoginTab.Controls.Add(this.passwordInput);
            this.UserLoginTab.Controls.Add(this.usernameInput);
            this.UserLoginTab.Controls.Add(this.passwordLabel);
            this.UserLoginTab.Controls.Add(this.usernameLabel);
            this.UserLoginTab.Controls.Add(this.userIconPicture);
            this.UserLoginTab.Controls.Add(this.loginLabel);
            this.UserLoginTab.Location = new System.Drawing.Point(4, 22);
            this.UserLoginTab.Name = "UserLoginTab";
            this.UserLoginTab.Padding = new System.Windows.Forms.Padding(3);
            this.UserLoginTab.Size = new System.Drawing.Size(792, 362);
            this.UserLoginTab.TabIndex = 1;
            this.UserLoginTab.Text = "UserLogin";
            this.UserLoginTab.UseVisualStyleBackColor = true;
            // 
            // registerButton
            // 
            this.registerButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.registerButton.BackColor = System.Drawing.Color.DarkCyan;
            this.registerButton.Depth = 0;
            this.registerButton.Location = new System.Drawing.Point(399, 284);
            this.registerButton.MouseState = MaterialSkin.MouseState.HOVER;
            this.registerButton.Name = "registerButton";
            this.registerButton.Primary = true;
            this.registerButton.Size = new System.Drawing.Size(131, 34);
            this.registerButton.TabIndex = 8;
            this.registerButton.Text = "Regisztráció";
            this.registerButton.UseVisualStyleBackColor = false;
            this.registerButton.Click += new System.EventHandler(this.registerButton_Click);
            // 
            // loginButton
            // 
            this.loginButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.loginButton.BackColor = System.Drawing.Color.DarkCyan;
            this.loginButton.Depth = 0;
            this.loginButton.Location = new System.Drawing.Point(225, 284);
            this.loginButton.MouseState = MaterialSkin.MouseState.HOVER;
            this.loginButton.Name = "loginButton";
            this.loginButton.Primary = true;
            this.loginButton.Size = new System.Drawing.Size(131, 34);
            this.loginButton.TabIndex = 7;
            this.loginButton.Text = "Bejelentkezés";
            this.loginButton.UseVisualStyleBackColor = false;
            this.loginButton.Click += new System.EventHandler(this.loginButton_Click);
            // 
            // backButton
            // 
            this.backButton.BackColor = System.Drawing.Color.DarkCyan;
            this.backButton.Depth = 0;
            this.backButton.Location = new System.Drawing.Point(8, 6);
            this.backButton.MouseState = MaterialSkin.MouseState.HOVER;
            this.backButton.Name = "backButton";
            this.backButton.Primary = true;
            this.backButton.Size = new System.Drawing.Size(75, 23);
            this.backButton.TabIndex = 6;
            this.backButton.Text = "Vissza";
            this.backButton.UseVisualStyleBackColor = false;
            this.backButton.Click += new System.EventHandler(this.backButton_Click);
            // 
            // passwordInput
            // 
            this.passwordInput.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.passwordInput.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.passwordInput.Location = new System.Drawing.Point(313, 235);
            this.passwordInput.Name = "passwordInput";
            this.passwordInput.PasswordChar = '*';
            this.passwordInput.Size = new System.Drawing.Size(160, 25);
            this.passwordInput.TabIndex = 5;
            // 
            // usernameInput
            // 
            this.usernameInput.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.usernameInput.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.usernameInput.Location = new System.Drawing.Point(313, 202);
            this.usernameInput.Name = "usernameInput";
            this.usernameInput.Size = new System.Drawing.Size(160, 25);
            this.usernameInput.TabIndex = 4;
            // 
            // passwordLabel
            // 
            this.passwordLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.passwordLabel.AutoSize = true;
            this.passwordLabel.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.passwordLabel.Location = new System.Drawing.Point(222, 243);
            this.passwordLabel.Name = "passwordLabel";
            this.passwordLabel.Size = new System.Drawing.Size(49, 17);
            this.passwordLabel.TabIndex = 3;
            this.passwordLabel.Text = "Jelszó:";
            // 
            // usernameLabel
            // 
            this.usernameLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.usernameLabel.AutoSize = true;
            this.usernameLabel.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.usernameLabel.Location = new System.Drawing.Point(201, 202);
            this.usernameLabel.Name = "usernameLabel";
            this.usernameLabel.Size = new System.Drawing.Size(106, 17);
            this.usernameLabel.TabIndex = 2;
            this.usernameLabel.Text = "Felhasználónév:";
            // 
            // userIconPicture
            // 
            this.userIconPicture.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.userIconPicture.BackgroundImage = global::FamilyManager.Properties.Resources.userIcon;
            this.userIconPicture.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.userIconPicture.Location = new System.Drawing.Point(326, 53);
            this.userIconPicture.Name = "userIconPicture";
            this.userIconPicture.Size = new System.Drawing.Size(138, 132);
            this.userIconPicture.TabIndex = 1;
            this.userIconPicture.TabStop = false;
            // 
            // loginLabel
            // 
            this.loginLabel.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.loginLabel.AutoSize = true;
            this.loginLabel.Font = new System.Drawing.Font("Segoe UI", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.loginLabel.Location = new System.Drawing.Point(318, 3);
            this.loginLabel.Name = "loginLabel";
            this.loginLabel.Size = new System.Drawing.Size(146, 47);
            this.loginLabel.TabIndex = 0;
            this.loginLabel.Text = "Belépés";
            // 
            // AdminLoginTab
            // 
            this.AdminLoginTab.Controls.Add(this.adminLoginButton);
            this.AdminLoginTab.Controls.Add(this.backButton2);
            this.AdminLoginTab.Controls.Add(this.adminPasswordInput);
            this.AdminLoginTab.Controls.Add(this.adminUsernameInput);
            this.AdminLoginTab.Controls.Add(this.adminPasswordLabel);
            this.AdminLoginTab.Controls.Add(this.adminUsernameLabel);
            this.AdminLoginTab.Controls.Add(this.adminIconPicture);
            this.AdminLoginTab.Controls.Add(this.login2Label);
            this.AdminLoginTab.Location = new System.Drawing.Point(4, 22);
            this.AdminLoginTab.Name = "AdminLoginTab";
            this.AdminLoginTab.Padding = new System.Windows.Forms.Padding(3);
            this.AdminLoginTab.Size = new System.Drawing.Size(792, 362);
            this.AdminLoginTab.TabIndex = 2;
            this.AdminLoginTab.Text = "AdminLogin";
            this.AdminLoginTab.UseVisualStyleBackColor = true;
            // 
            // adminLoginButton
            // 
            this.adminLoginButton.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.adminLoginButton.BackColor = System.Drawing.Color.DarkCyan;
            this.adminLoginButton.Depth = 0;
            this.adminLoginButton.Location = new System.Drawing.Point(320, 299);
            this.adminLoginButton.MouseState = MaterialSkin.MouseState.HOVER;
            this.adminLoginButton.Name = "adminLoginButton";
            this.adminLoginButton.Primary = true;
            this.adminLoginButton.Size = new System.Drawing.Size(195, 39);
            this.adminLoginButton.TabIndex = 15;
            this.adminLoginButton.Text = "Bejelentkezés";
            this.adminLoginButton.UseVisualStyleBackColor = false;
            this.adminLoginButton.Click += new System.EventHandler(this.adminLoginButton_Click);
            // 
            // backButton2
            // 
            this.backButton2.BackColor = System.Drawing.Color.DarkCyan;
            this.backButton2.Depth = 0;
            this.backButton2.Location = new System.Drawing.Point(8, 6);
            this.backButton2.MouseState = MaterialSkin.MouseState.HOVER;
            this.backButton2.Name = "backButton2";
            this.backButton2.Primary = true;
            this.backButton2.Size = new System.Drawing.Size(75, 23);
            this.backButton2.TabIndex = 14;
            this.backButton2.Text = "Vissza";
            this.backButton2.UseVisualStyleBackColor = false;
            this.backButton2.Click += new System.EventHandler(this.backButton2_Click);
            // 
            // adminPasswordInput
            // 
            this.adminPasswordInput.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.adminPasswordInput.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.adminPasswordInput.Location = new System.Drawing.Point(337, 247);
            this.adminPasswordInput.Name = "adminPasswordInput";
            this.adminPasswordInput.PasswordChar = '*';
            this.adminPasswordInput.Size = new System.Drawing.Size(160, 25);
            this.adminPasswordInput.TabIndex = 13;
            // 
            // adminUsernameInput
            // 
            this.adminUsernameInput.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.adminUsernameInput.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.adminUsernameInput.Location = new System.Drawing.Point(337, 214);
            this.adminUsernameInput.Name = "adminUsernameInput";
            this.adminUsernameInput.Size = new System.Drawing.Size(160, 25);
            this.adminUsernameInput.TabIndex = 12;
            // 
            // adminPasswordLabel
            // 
            this.adminPasswordLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.adminPasswordLabel.AutoSize = true;
            this.adminPasswordLabel.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.adminPasswordLabel.Location = new System.Drawing.Point(246, 255);
            this.adminPasswordLabel.Name = "adminPasswordLabel";
            this.adminPasswordLabel.Size = new System.Drawing.Size(49, 17);
            this.adminPasswordLabel.TabIndex = 11;
            this.adminPasswordLabel.Text = "Jelszó:";
            // 
            // adminUsernameLabel
            // 
            this.adminUsernameLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.adminUsernameLabel.AutoSize = true;
            this.adminUsernameLabel.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.adminUsernameLabel.Location = new System.Drawing.Point(225, 214);
            this.adminUsernameLabel.Name = "adminUsernameLabel";
            this.adminUsernameLabel.Size = new System.Drawing.Size(106, 17);
            this.adminUsernameLabel.TabIndex = 10;
            this.adminUsernameLabel.Text = "Felhasználónév:";
            // 
            // adminIconPicture
            // 
            this.adminIconPicture.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.adminIconPicture.BackgroundImage = global::FamilyManager.Properties.Resources.adminIcon;
            this.adminIconPicture.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.adminIconPicture.Location = new System.Drawing.Point(350, 64);
            this.adminIconPicture.Name = "adminIconPicture";
            this.adminIconPicture.Size = new System.Drawing.Size(138, 132);
            this.adminIconPicture.TabIndex = 9;
            this.adminIconPicture.TabStop = false;
            // 
            // login2Label
            // 
            this.login2Label.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.login2Label.AutoSize = true;
            this.login2Label.Font = new System.Drawing.Font("Segoe UI", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.login2Label.Location = new System.Drawing.Point(342, 3);
            this.login2Label.Name = "login2Label";
            this.login2Label.Size = new System.Drawing.Size(146, 47);
            this.login2Label.TabIndex = 8;
            this.login2Label.Text = "Belépés";
            // 
            // RegisterTab
            // 
            this.RegisterTab.Controls.Add(this.positionInput);
            this.RegisterTab.Controls.Add(this.emailInput);
            this.RegisterTab.Controls.Add(this.positionLabel);
            this.RegisterTab.Controls.Add(this.emailLabel);
            this.RegisterTab.Controls.Add(this.registerPasswordInput);
            this.RegisterTab.Controls.Add(this.registerUsernameInput);
            this.RegisterTab.Controls.Add(this.registerPasswordLabel);
            this.RegisterTab.Controls.Add(this.registerUsernameLabel);
            this.RegisterTab.Controls.Add(this.registrationButton);
            this.RegisterTab.Controls.Add(this.backButton3);
            this.RegisterTab.Controls.Add(this.lastNameInput);
            this.RegisterTab.Controls.Add(this.firstNameInput);
            this.RegisterTab.Controls.Add(this.lastNameLabel);
            this.RegisterTab.Controls.Add(this.firstNameLabel);
            this.RegisterTab.Controls.Add(this.registerLabel);
            this.RegisterTab.Location = new System.Drawing.Point(4, 22);
            this.RegisterTab.Name = "RegisterTab";
            this.RegisterTab.Padding = new System.Windows.Forms.Padding(3);
            this.RegisterTab.Size = new System.Drawing.Size(792, 362);
            this.RegisterTab.TabIndex = 3;
            this.RegisterTab.Text = "Register";
            this.RegisterTab.UseVisualStyleBackColor = true;
            // 
            // positionInput
            // 
            this.positionInput.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.positionInput.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.positionInput.Location = new System.Drawing.Point(347, 258);
            this.positionInput.Name = "positionInput";
            this.positionInput.Size = new System.Drawing.Size(160, 25);
            this.positionInput.TabIndex = 30;
            // 
            // emailInput
            // 
            this.emailInput.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.emailInput.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.emailInput.Location = new System.Drawing.Point(347, 222);
            this.emailInput.Name = "emailInput";
            this.emailInput.Size = new System.Drawing.Size(160, 25);
            this.emailInput.TabIndex = 29;
            // 
            // positionLabel
            // 
            this.positionLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.positionLabel.AutoSize = true;
            this.positionLabel.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.positionLabel.Location = new System.Drawing.Point(245, 261);
            this.positionLabel.Name = "positionLabel";
            this.positionLabel.Size = new System.Drawing.Size(73, 17);
            this.positionLabel.TabIndex = 28;
            this.positionLabel.Text = "Szerepkör:";
            // 
            // emailLabel
            // 
            this.emailLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.emailLabel.AutoSize = true;
            this.emailLabel.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.emailLabel.Location = new System.Drawing.Point(272, 225);
            this.emailLabel.Name = "emailLabel";
            this.emailLabel.Size = new System.Drawing.Size(46, 17);
            this.emailLabel.TabIndex = 27;
            this.emailLabel.Text = "Email:";
            // 
            // registerPasswordInput
            // 
            this.registerPasswordInput.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.registerPasswordInput.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.registerPasswordInput.Location = new System.Drawing.Point(347, 182);
            this.registerPasswordInput.Name = "registerPasswordInput";
            this.registerPasswordInput.PasswordChar = '*';
            this.registerPasswordInput.Size = new System.Drawing.Size(160, 25);
            this.registerPasswordInput.TabIndex = 26;
            // 
            // registerUsernameInput
            // 
            this.registerUsernameInput.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.registerUsernameInput.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.registerUsernameInput.Location = new System.Drawing.Point(347, 146);
            this.registerUsernameInput.Name = "registerUsernameInput";
            this.registerUsernameInput.Size = new System.Drawing.Size(160, 25);
            this.registerUsernameInput.TabIndex = 25;
            // 
            // registerPasswordLabel
            // 
            this.registerPasswordLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.registerPasswordLabel.AutoSize = true;
            this.registerPasswordLabel.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.registerPasswordLabel.Location = new System.Drawing.Point(269, 182);
            this.registerPasswordLabel.Name = "registerPasswordLabel";
            this.registerPasswordLabel.Size = new System.Drawing.Size(49, 17);
            this.registerPasswordLabel.TabIndex = 24;
            this.registerPasswordLabel.Text = "Jelszó:";
            // 
            // registerUsernameLabel
            // 
            this.registerUsernameLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.registerUsernameLabel.AutoSize = true;
            this.registerUsernameLabel.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.registerUsernameLabel.Location = new System.Drawing.Point(215, 149);
            this.registerUsernameLabel.Name = "registerUsernameLabel";
            this.registerUsernameLabel.Size = new System.Drawing.Size(106, 17);
            this.registerUsernameLabel.TabIndex = 23;
            this.registerUsernameLabel.Text = "Felhasználónév:";
            // 
            // registrationButton
            // 
            this.registrationButton.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.registrationButton.BackColor = System.Drawing.Color.DarkCyan;
            this.registrationButton.Depth = 0;
            this.registrationButton.Location = new System.Drawing.Point(338, 313);
            this.registrationButton.MouseState = MaterialSkin.MouseState.HOVER;
            this.registrationButton.Name = "registrationButton";
            this.registrationButton.Primary = true;
            this.registrationButton.Size = new System.Drawing.Size(195, 39);
            this.registrationButton.TabIndex = 22;
            this.registrationButton.Text = "Regisztráció";
            this.registrationButton.UseVisualStyleBackColor = false;
            this.registrationButton.Click += new System.EventHandler(this.registrationButton_Click);
            // 
            // backButton3
            // 
            this.backButton3.Depth = 0;
            this.backButton3.Location = new System.Drawing.Point(8, 6);
            this.backButton3.MouseState = MaterialSkin.MouseState.HOVER;
            this.backButton3.Name = "backButton3";
            this.backButton3.Primary = true;
            this.backButton3.Size = new System.Drawing.Size(75, 23);
            this.backButton3.TabIndex = 21;
            this.backButton3.Text = "Vissza";
            this.backButton3.UseVisualStyleBackColor = true;
            this.backButton3.Click += new System.EventHandler(this.backButton3_Click);
            // 
            // lastNameInput
            // 
            this.lastNameInput.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lastNameInput.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lastNameInput.Location = new System.Drawing.Point(347, 106);
            this.lastNameInput.Name = "lastNameInput";
            this.lastNameInput.Size = new System.Drawing.Size(160, 25);
            this.lastNameInput.TabIndex = 20;
            // 
            // firstNameInput
            // 
            this.firstNameInput.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.firstNameInput.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.firstNameInput.Location = new System.Drawing.Point(347, 70);
            this.firstNameInput.Name = "firstNameInput";
            this.firstNameInput.Size = new System.Drawing.Size(160, 25);
            this.firstNameInput.TabIndex = 19;
            // 
            // lastNameLabel
            // 
            this.lastNameLabel.AutoSize = true;
            this.lastNameLabel.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lastNameLabel.Location = new System.Drawing.Point(240, 109);
            this.lastNameLabel.Name = "lastNameLabel";
            this.lastNameLabel.Size = new System.Drawing.Size(78, 17);
            this.lastNameLabel.TabIndex = 18;
            this.lastNameLabel.Text = "Keresztnév:";
            // 
            // firstNameLabel
            // 
            this.firstNameLabel.AutoSize = true;
            this.firstNameLabel.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.firstNameLabel.Location = new System.Drawing.Point(240, 73);
            this.firstNameLabel.Name = "firstNameLabel";
            this.firstNameLabel.Size = new System.Drawing.Size(81, 17);
            this.firstNameLabel.TabIndex = 17;
            this.firstNameLabel.Text = "Vezetéknév:";
            // 
            // registerLabel
            // 
            this.registerLabel.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.registerLabel.AutoSize = true;
            this.registerLabel.Font = new System.Drawing.Font("Segoe UI", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.registerLabel.Location = new System.Drawing.Point(313, 3);
            this.registerLabel.Name = "registerLabel";
            this.registerLabel.Size = new System.Drawing.Size(220, 47);
            this.registerLabel.TabIndex = 16;
            this.registerLabel.Text = "Regisztráció";
            // 
            // LoadingForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Tomato;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.tabControl);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.Name = "LoadingForm";
            this.Sizable = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FamilyManager";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tabControl.ResumeLayout(false);
            this.ModeSelectorTab.ResumeLayout(false);
            this.ModeSelectorTab.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.userPicture)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.adminPicture)).EndInit();
            this.UserLoginTab.ResumeLayout(false);
            this.UserLoginTab.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.userIconPicture)).EndInit();
            this.AdminLoginTab.ResumeLayout(false);
            this.AdminLoginTab.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.adminIconPicture)).EndInit();
            this.RegisterTab.ResumeLayout(false);
            this.RegisterTab.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private MaterialSkin.Controls.MaterialTabControl tabControl;
        private System.Windows.Forms.TabPage ModeSelectorTab;
        private System.Windows.Forms.TabPage UserLoginTab;
        private System.Windows.Forms.TabPage AdminLoginTab;
        private System.Windows.Forms.TabPage RegisterTab;
        private System.Windows.Forms.Label modeLabel;
        private System.Windows.Forms.PictureBox adminPicture;
        private System.Windows.Forms.PictureBox userPicture;
        private MaterialSkin.Controls.MaterialRaisedButton adminButton;
        private MaterialSkin.Controls.MaterialRaisedButton userButton;
        private MaterialSkin.Controls.MaterialRaisedButton exitButton;
        private System.Windows.Forms.Label loginLabel;
        private System.Windows.Forms.PictureBox userIconPicture;
        private System.Windows.Forms.Label usernameLabel;
        private System.Windows.Forms.Label passwordLabel;
        private System.Windows.Forms.TextBox usernameInput;
        private System.Windows.Forms.TextBox passwordInput;
        private MaterialSkin.Controls.MaterialRaisedButton backButton;
        private MaterialSkin.Controls.MaterialRaisedButton loginButton;
        private MaterialSkin.Controls.MaterialRaisedButton registerButton;
        private MaterialSkin.Controls.MaterialRaisedButton adminLoginButton;
        private MaterialSkin.Controls.MaterialRaisedButton backButton2;
        private System.Windows.Forms.TextBox adminPasswordInput;
        private System.Windows.Forms.TextBox adminUsernameInput;
        private System.Windows.Forms.Label adminPasswordLabel;
        private System.Windows.Forms.Label adminUsernameLabel;
        private System.Windows.Forms.PictureBox adminIconPicture;
        private System.Windows.Forms.Label login2Label;
        private MaterialSkin.Controls.MaterialRaisedButton registrationButton;
        private MaterialSkin.Controls.MaterialRaisedButton backButton3;
        private System.Windows.Forms.TextBox lastNameInput;
        private System.Windows.Forms.TextBox firstNameInput;
        private System.Windows.Forms.Label lastNameLabel;
        private System.Windows.Forms.Label firstNameLabel;
        private System.Windows.Forms.Label registerLabel;
        private System.Windows.Forms.TextBox registerPasswordInput;
        private System.Windows.Forms.TextBox registerUsernameInput;
        private System.Windows.Forms.Label registerPasswordLabel;
        private System.Windows.Forms.Label registerUsernameLabel;
        private System.Windows.Forms.TextBox positionInput;
        private System.Windows.Forms.TextBox emailInput;
        private System.Windows.Forms.Label positionLabel;
        private System.Windows.Forms.Label emailLabel;
    }
}

