﻿using MaterialSkin;
using MaterialSkin.Controls;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress;

namespace FamilyManager
{
    public partial class AdminLoggedinForm : MaterialForm
    {
        //Variables
        #region Variables
        private string Username;
        private string Email;

        Database db = new Database();
        DataTable dt = new DataTable();
        DataTable dtFamily = new DataTable();
        DataTable dtPrograms = new DataTable();
        DataTable dtExercises = new DataTable();
        DataTable dtRoutines = new DataTable();
        DataTable diagramDT = new DataTable();
        private MySqlDataAdapter dataAdapter;
        private MySqlDataAdapter dataAdapterFamily;
        private MySqlDataAdapter dataAdapterPrograms;
        private MySqlDataAdapter dataAdapterExercises;
        private MySqlDataAdapter dataAdapterRoutines;
        private MySqlDataAdapter dataAdapterDiagram;
        private MySqlCommandBuilder commandBuilder;
        private MySqlCommandBuilder commandBuilderFamily;
        private MySqlCommandBuilder commandBuilderPrograms;
        private MySqlCommandBuilder commandBuilderExercises;
        private MySqlCommandBuilder commandBuilderRoutines;
        private MySqlCommandBuilder commandBuilderDiagram;
        MySqlCommand cmd;
        private bool changed = false;
        int tabindex;
        #endregion


        public AdminLoggedinForm(string Username, string Email)
        {
            this.Username = Username;
            this.Email = Email;

            InitializeComponent();
        }

        //Do on load
        private void AdminLoggedinForm_Load(object sender, EventArgs e)
        {

            //Write out datas to labels
            adminUsernameLabel.Text = Username;
            adminEmailLabel.Text = Email;

            this.Show();

            loadUsersDatas();
            loadFamilyDatas();
            loadProgramDatas();
            loadExerciseDatas();
            loadRoutinesDatas();
            loadStaticsDiagram();
        }

        private void getTabcontrolTabIndex() {
            tabindex = menuTabSelector.TabIndex;
            admonTabControl.SelectTab(tabindex);
        }

        //Load users
        #region LoadUsers
        private void loadUsersDatas() {
            try
            {
                usersDataGridView.Rows.Clear();
                db.OpenConnection();
                cmd = new MySqlCommand("SELECT * from users", db.connection);
                dataAdapter = new MySqlDataAdapter(cmd);
                commandBuilder = new MySqlCommandBuilder(dataAdapter);
                dataAdapter.Fill(dt);
                usersDataGridView.DataSource = dt;
                usersDataGridView.ReadOnly = true;
                usersDataGridView.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            }
            catch (Exception loadEx)
            {
                MessageBox.Show(loadEx.ToString());
            }
        }
        #endregion

        //Load familys
        #region LoadFamilys
        private void loadFamilyDatas() {
            try
            {
                familysDataGridView.Rows.Clear();
                cmd = new MySqlCommand("SELECT * from familys", db.connection);
                dataAdapterFamily = new MySqlDataAdapter(cmd);
                commandBuilderFamily = new MySqlCommandBuilder(dataAdapterFamily);
                dataAdapterFamily.Fill(dtFamily);
                familysDataGridView.DataSource = dtFamily;
                familysDataGridView.ReadOnly = true;
                familysDataGridView.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            }
            catch (Exception loadEx)
            {
                MessageBox.Show(loadEx.ToString());
            }
        }
        #endregion

        //Load programs
        #region LoadPrograms
        private void loadProgramDatas()
        {
            try
            {
                programsDataGridView.Rows.Clear();
                cmd = new MySqlCommand("SELECT * from programs", db.connection);
                dataAdapterPrograms = new MySqlDataAdapter(cmd);
                commandBuilderPrograms = new MySqlCommandBuilder(dataAdapterPrograms);
                dataAdapterPrograms.Fill(dtPrograms);
                programsDataGridView.DataSource = dtPrograms;
                programsDataGridView.ReadOnly = true;
                programsDataGridView.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            }
            catch (Exception loadEx)
            {
                MessageBox.Show(loadEx.ToString());
            }
        }
        #endregion

        //Load exercises
        #region LoadExercises
        private void loadExerciseDatas()
        {
            try
            {
                exercisesDataGridView.Rows.Clear();
                cmd = new MySqlCommand("SELECT * from todolist", db.connection);
                dataAdapterExercises = new MySqlDataAdapter(cmd);
                commandBuilderExercises = new MySqlCommandBuilder(dataAdapterExercises);
                dataAdapterExercises.Fill(dtExercises);
                exercisesDataGridView.DataSource = dtExercises;
                exercisesDataGridView.ReadOnly = true;
                exercisesDataGridView.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            }
            catch (Exception loadEx)
            {
                MessageBox.Show(loadEx.ToString());
            }
        }
        #endregion

        //Load routine
        #region LoadRoutine
        private void loadRoutinesDatas()
        {
            try
            {
                routinesDataGridView.Rows.Clear();
                cmd = new MySqlCommand("SELECT * from exercises", db.connection);
                dataAdapterRoutines = new MySqlDataAdapter(cmd);
                commandBuilderRoutines = new MySqlCommandBuilder(dataAdapterRoutines);
                dataAdapterRoutines.Fill(dtRoutines);
                routinesDataGridView.DataSource = dtRoutines;
                routinesDataGridView.ReadOnly = true;
                routinesDataGridView.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            }
            catch (Exception loadEx)
            {
                MessageBox.Show(loadEx.ToString());
            }
        }
        #endregion

        //Load diagram
        #region LoadDiagram
        private void loadStaticsDiagram()
        {
            try
            {
                DataTable tempDT = new DataTable();

                cmd = new MySqlCommand("SELECT COUNT(users.Id) AS 'Azonosito' from users", db.connection);
                dataAdapterDiagram = new MySqlDataAdapter(cmd);
                commandBuilderDiagram = new MySqlCommandBuilder(dataAdapterDiagram);
                dataAdapterDiagram.Fill(diagramDT);

                cmd = new MySqlCommand("SELECT COUNT(users.Id) AS 'SzulokAzonosito' from users WHERE Position='Apa' OR Position='Anya'", db.connection);
                dataAdapterDiagram = new MySqlDataAdapter(cmd);
                commandBuilderDiagram = new MySqlCommandBuilder(dataAdapterDiagram);
                dataAdapterDiagram.Fill(tempDT);
                diagramDT.Merge(tempDT);

                cmd = new MySqlCommand("SELECT COUNT(users.Id) AS 'NagyszulokAzonosito' from users WHERE Position='Nagyapa' OR Position='Nagymama'", db.connection);
                dataAdapterDiagram = new MySqlDataAdapter(cmd);
                commandBuilderDiagram = new MySqlCommandBuilder(dataAdapterDiagram);
                dataAdapterDiagram.Fill(tempDT);
                diagramDT.Merge(tempDT);

                cmd = new MySqlCommand("SELECT COUNT(users.Id) AS 'GyerekekAzonosito' from users WHERE Position='Bátyj' OR Position='Húg'", db.connection);
                dataAdapterDiagram = new MySqlDataAdapter(cmd);
                commandBuilderDiagram = new MySqlCommandBuilder(dataAdapterDiagram);
                dataAdapterDiagram.Fill(tempDT);
                diagramDT.Merge(tempDT);

                statisticChart.DataSource = diagramDT;

               /*statisticChart.Series[0].Points.DataBindXY(
                    new[] { "Összes felhasználó" },
                    new[] { diagramDT.Rows[4][1]}
                );*/

                statisticChart.Series[1].Points.DataBindXY(
                    new[] {"Szülők", "Nagyszülők", "Gyerekek"},
                    new[] {diagramDT.Rows[1][1], diagramDT.Rows[3][1], diagramDT.Rows[2][1]}
                );

                statisticChart.DataBind();


            }
            catch (Exception loadDi)
            {
                MessageBox.Show(loadDi.ToString());
            }
        }
        #endregion

        //Cell values changes functions
        #region CellValueChanges Functions
        private void familysDataGridView_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            changed = true;
        }

        private void usersDataGridView_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            changed = true;
        }
        #endregion

        //Do on searching user
        private void shearchUserButton_Click(object sender, EventArgs e)
        {

        }

        //Do on add new user to users
        private void newUserButton_Click(object sender, EventArgs e)
        {
            #region AddNewUser
            newUserButton.Visible = false;
            editUserButton.Visible = false;
            deleteUserButton.Visible = false;
            reloadFamily.Visible = false;
            saveUsersButton.Visible = true;
            cancelUsersButton.Visible = true;

            usersDataGridView.AllowUserToAddRows = true;
            usersDataGridView.SelectionMode = DataGridViewSelectionMode.CellSelect;

            int sor = usersDataGridView.Rows.Count - 1;

            usersDataGridView.Rows[sor].Cells[1].Selected = true;

            usersDataGridView.Rows[sor].Cells[1].Value = "Írja ide az új adatot";

            usersDataGridView.ReadOnly = false;

            for (int i = 0; i < sor; i = i + 1)
            {
                usersDataGridView.Rows[i].ReadOnly = true;
            }

            usersDataGridView.FirstDisplayedScrollingRowIndex = sor;

            usersDataGridView.Columns[0].ReadOnly = true;
            changed = true;
            #endregion
        }

        //Do on edit user
        private void editUserButton_Click(object sender, EventArgs e)
        {
            #region EditUser
            //gombok beállítása 
            saveUsersButton.Visible = true;
            cancelUsersButton.Visible = true;
            shearchUserButton.Visible = false;
            newUserButton.Visible = false;
            editUserButton.Visible = false;
            deleteUserButton.Visible = false;
            //dataGridView beállítása 
            usersDataGridView.ReadOnly = false;
            usersDataGridView.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            usersDataGridView.AllowUserToDeleteRows = false;

            //módosítás beállítása      
            changed = true;
            #endregion
        }

        //Do on delete user
        private void deleteUserButton_Click(object sender, EventArgs e)
        {
            #region DeleteUser
            if (MessageBox.Show(
                    "Valóban törölni akarja a sort?",
                    "Törlés",
                    MessageBoxButtons.YesNo,
                    MessageBoxIcon.Exclamation
                ) == DialogResult.Yes)
            {
                try
                {
                    int sor = usersDataGridView.SelectedRows[0].Index;
                    usersDataGridView.Rows.RemoveAt(sor);
                    changed = false;
                    dataAdapter.DeleteCommand = commandBuilder.GetDeleteCommand();
                    dataAdapter.Update(dt);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            }
            #endregion
        }

        //Do on reload users
        private void reloadUsersButton_Click(object sender, EventArgs e)
        {
            #region ReLoadUsers
            try
            {
                dt.Clear();
                usersDataGridView.DataSource = dt;
                loadUsersDatas();
            }
            catch (Exception loadEx)
            {
                MessageBox.Show(loadEx.ToString());
            }
            #endregion
        }

        //Do on logout from users
        private void adminLogoutUsersButton_Click(object sender, EventArgs e)
        {
            #region LogoutFromUsers
            if (changed)
            {
                if (MessageBox.Show(
                    "Nem mentett adatok vannak! Valóban ki akar lépni?",
                    "Kilépés",
                    MessageBoxButtons.YesNo,
                    MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    db.CloseConnection();
                    LoadingForm lf = new LoadingForm();
                    lf.Show();
                    this.Close();
                }
            }
            else
            {
                db.CloseConnection();
                this.Close();
            }
            #endregion
        }

        //Do on save user changes
        private void saveUsersButton_Click(object sender, EventArgs e)
        {
            #region SaveUser
            saveUsersButton.Visible = false;
            cancelUsersButton.Visible = false;
            shearchUserButton.Visible = true;
            newUserButton.Visible = true;
            editUserButton.Visible = true;
            deleteUserButton.Visible = true;
            reloadFamily.Visible = true;

            if (changed)
            {
                dataAdapter.UpdateCommand = commandBuilder.GetUpdateCommand();
                dataAdapter.Update(dt);
                usersDataGridView.SelectionMode = DataGridViewSelectionMode.FullRowSelect;

                changed = false;


                dt.Clear();
                usersDataGridView.DataSource = dt;
                loadUsersDatas();
            }
            else
            {
                MessageBox.Show(
                    "Nem lett módosítva egy adat sem.",
                    "Módosítás",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
                saveUsersButton.Enabled = false;
            }
            #endregion
        }

        //Do on cancel save users
        private void cancelUsersButton_Click(object sender, EventArgs e)
        {
            #region CancelSaveUser
            saveUsersButton.Visible = false;
            cancelUsersButton.Visible = false;
            shearchUserButton.Visible = true;
            newUserButton.Visible = true;
            editUserButton.Visible = true;
            deleteUserButton.Visible = true;

            reloadFamily.PerformClick();

            changed = false;
            #endregion
        }

        //Do on add new family
        private void addNewFamilyButton_Click(object sender, EventArgs e)
        {
            #region AddNewFamily
            addNewFamilyButton.Visible = false;
            editFamilyButton.Visible = false;
            deleteFamilyButton.Visible = false;
            reloadFamily.Visible = false;
            saveFamilyButton.Visible = true;
            cancelFamilyButton.Visible = true;

            familysDataGridView.AllowUserToAddRows = true;
            familysDataGridView.SelectionMode = DataGridViewSelectionMode.CellSelect;

            int sor = familysDataGridView.Rows.Count - 1;

            familysDataGridView.Rows[sor].Cells[1].Selected = true;

            familysDataGridView.Rows[sor].Cells[1].Value = "Írja ide az új adatot";

            familysDataGridView.ReadOnly = false;

            for (int i = 0; i < sor; i = i + 1)
            {
                familysDataGridView.Rows[i].ReadOnly = true;
            }

            familysDataGridView.FirstDisplayedScrollingRowIndex = sor;

            familysDataGridView.Columns[0].ReadOnly = true;
            changed = true;
            #endregion
        }

        //Do on edit family
        private void editFamilyButton_Click(object sender, EventArgs e)
        {
            #region EditFamily
            //gombok beállítása 
            saveFamilyButton.Visible = true;
            cancelFamilyButton.Visible = true;
            searchFamilyButton.Visible = false;
            addNewFamilyButton.Visible = false;
            deleteFamilyButton.Visible = false;
            //dataGridView beállítása 
            familysDataGridView.ReadOnly = false;
            familysDataGridView.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            familysDataGridView.AllowUserToDeleteRows = false;

            //módosítás beállítása      
            changed = true;
            #endregion
        }

        //Do on delete family
        private void deleteFamilyButton_Click(object sender, EventArgs e)
        {
            #region DeleteFamily
            if (MessageBox.Show(
                    "Valóban törölni akarja a sort?",
                    "Törlés",
                    MessageBoxButtons.YesNo,
                    MessageBoxIcon.Exclamation
                ) == DialogResult.Yes)
            {
                try
                {
                    int sor = familysDataGridView.SelectedRows[0].Index;
                    familysDataGridView.Rows.RemoveAt(sor);
                    changed = false;
                    dataAdapterFamily.DeleteCommand = commandBuilderFamily.GetDeleteCommand();
                    dataAdapterFamily.Update(dtFamily);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            }
            #endregion
        }

        //Do on save family
        private void saveFamilyButton_Click(object sender, EventArgs e)
        {
            #region SaveFamily
            saveFamilyButton.Visible = false;
            cancelFamilyButton.Visible = false;
            searchFamilyButton.Visible = true;
            addNewFamilyButton.Visible = true;
            editFamilyButton.Visible = true;
            deleteFamilyButton.Visible = true;
            reloadFamily.Visible = true;

            if (changed)
            {
                dataAdapterFamily.UpdateCommand = commandBuilderFamily.GetUpdateCommand();
                dataAdapterFamily.Update(dtFamily);
                familysDataGridView.SelectionMode = DataGridViewSelectionMode.FullRowSelect;

                changed = false;


                reloadFamily.PerformClick();
            }
            else
            {
                MessageBox.Show(
                    "Nem lett módosítva egy adat sem.",
                    "Módosítás",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
            }
            #endregion
        }

        //Don on cancel save family
        private void cancelFamilyButton_Click(object sender, EventArgs e)
        {
            #region CancelSaveFamily
            saveFamilyButton.Visible = false;
            cancelFamilyButton.Visible = false;
            searchFamilyButton.Visible = true;
            addNewFamilyButton.Visible = true;
            editFamilyButton.Visible = true;
            deleteFamilyButton.Visible = true;

            reloadFamily.PerformClick();

            changed = false;
            #endregion
        }

        //Do on reloadFamily
        private void reloadFamily_Click(object sender, EventArgs e)
        {
            #region ReLoadFamilys
            try
            {
                dtFamily.Clear();
                familysDataGridView.DataSource = dtFamily;
                loadFamilyDatas();
            }
            catch (Exception loadEx)
            {
                MessageBox.Show(loadEx.ToString());
            }
            #endregion
        }

        //Do on search family
        private void searchFamilyButton_Click(object sender, EventArgs e)
        {

        }

        private void menuTabSelector_Click(object sender, EventArgs e)
        {
            getTabcontrolTabIndex();
        }
    }
}
