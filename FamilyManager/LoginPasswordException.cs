﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FamilyManager
{
    class LoginPasswordException : Exception
    {
        //Declaration of the bad password variable
        private string badPassword;
        //Declaration of the message variable
        private string message;

        //Declaration of the constructor
        public LoginPasswordException(string badPassword, string message) : base(message)
        {
            this.badPassword = badPassword;
        }

        //Declaration of the getter
        public string getBadPassword()
        {
            return badPassword;
        }
    }
}
