﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace FamilyManager
{
    public class LoginPasswordValidator
    {
        //Declaration of the password variable
        private string password;

        //Declaration of the constructor
        public LoginPasswordValidator(string password)
        {
            this.password = password;
        }

        /// <summary>
        /// Check the password
        /// </summary>
        /// <param name="password">To check password</param>
        /// <returns>Is password okay</returns>
        /// <exception cref="LoginPasswordException">When password is wrong</exception>
        #region PasswordCheck
        public bool ValidPassword()
        {
            bool ok = true;
            if (!ValidPasswordEmpty())
            {
                throw new LoginPasswordException(password, "Ki kell töltened a jelszó mezőt!");
            }
            if (!ValidPasswordAtleast())
            {
                throw new LoginPasswordException(password, "A jelszónak minimum 8 karakterből kell állnia!");
            }
            if (!ValidPasswordWhiteSpace())
            {
                throw new LoginUsernameException(password, "A jelszó nem tartalmazhat szóközt!");
            }
            if (!ValidPasswordUpperLetter())
            {
                throw new LoginPasswordException(password, "A jelszónak tartalmaznia kell nagybetűket!");
            }
            if (!ValidPasswordLowerLetter())
            {
                throw new LoginPasswordException(password, "A jelszónak tartalmaznia kell kisbetűket!");
            }
            if (!ValidPasswordNumber())
            {
                throw new LoginPasswordException(password, "A jelszónak tartalmaznia kell számokat!");
            }
            return ok;

        }
        #endregion

        #region ValidatorMethods

        //Check is password empty method
        public bool ValidPasswordEmpty()
        {
            if (string.IsNullOrEmpty(password))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        //Check is password have lowercase letters method
        public bool ValidPasswordLowerLetter()
        {

            if (Regex.IsMatch(password, @"^(?=.*[a-zöüóőúéáű])"))
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        //Check is password have enought characters
        public bool ValidPasswordWhiteSpace()
        {
            password.Trim();
            if (password.Contains(' '))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        //Check is password have uppercase letters method
        public bool ValidPasswordUpperLetter()
        {

            if (Regex.IsMatch(password, @"^(?=.*[A-ZÖÜÓŐÚÉÁŰ])"))
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        //Check is password have numbers method
        public bool ValidPasswordNumber()
        {

            if (Regex.IsMatch(password, @"^(?=.*[0-9])"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        //Check is password have a minimum number of characters method
        public bool ValidPasswordAtleast()
        {

            if (password.Length >= 8)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion
    }
}
