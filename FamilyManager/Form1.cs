﻿using CryptSharp;
using MaterialSkin;
using MaterialSkin.Controls;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FamilyManager
{
    public partial class LoadingForm : MaterialForm
    {
        public LoadingForm()
        {
            /*Thread t = new Thread(new ThreadStart(Splash));
            t.Start();
            //Loading data
            string str = string.Empty;
            for (int i = 0; i < 50000; i++) {
                str += i.ToString();
            }
            //Complate
            t.Abort();
            this.Activate();*/

            InitializeComponent();
        }

        //Configure splash function
       /* void Splash() {
            SplashScreen.SplashForm frm = new SplashScreen.SplashForm();
            frm.AppName = "FamilyManager";
            frm.BackColor = Color.DarkOrange;
            frm.ForeColor = Color.DarkOrange;
            frm.Icon = Properties.Resources.icon;
            frm.ShowIcon = true;
            frm.ShowInTaskbar = true;
            Application.Run(frm);
        }*/

        //Do on load
        private void Form1_Load(object sender, EventArgs e)
        {
            this.Activate();
        }

        //Do on exit
        private void exitButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        //Open user login tab
        private void userButton_Click(object sender, EventArgs e)
        {
            tabControl.SelectTab(1);
        }

        //Open admin login tab
        private void adminButton_Click(object sender, EventArgs e)
        {
            tabControl.SelectTab(2);
        }

        //Do on back from userlogin
        private void backButton_Click(object sender, EventArgs e)
        {
            tabControl.SelectTab(0);
        }

        //Do on user login
        private void loginButton_Click(object sender, EventArgs e)
        {
            bool noErrorInUsername = false;
            bool noErrorInPassword = false;

            //Declarate the variables
            string username = usernameInput.Text;
            string password = passwordInput.Text;

            //Make validators
            LoginUsernameValidator UV = new LoginUsernameValidator(username);
            LoginPasswordValidator PV = new LoginPasswordValidator(password);

            //Run username validation
            try
            {
                UV.ValidUsername();
                noErrorInUsername = true;
            }
            catch (LoginUsernameException Ue)
            {
                MessageBox.Show(Ue.ToString());
                noErrorInUsername = false;
            }

            //Run password validation
            try
            {
                PV.ValidPassword();
                noErrorInPassword = true;
            }
            catch (LoginPasswordException Pe)
            {
                MessageBox.Show(Pe.ToString());
                noErrorInPassword = false;
            }

            //Logging in

            if (noErrorInUsername == true && noErrorInPassword == true)
            {
                try
                {
                    int i = 0;

                    Database db = new Database();
                    db.OpenConnection();
                    string query = "SELECT * FROM users WHERE Username='" + username + "'";
                    MySqlCommand command = new MySqlCommand(query, db.connection);
                    command.ExecuteNonQuery();
                    DataTable dt = new DataTable();
                    MySqlDataAdapter da = new MySqlDataAdapter(command);
                    da.Fill(dt);
                    i = Convert.ToInt32(dt.Rows.Count.ToString());

                    if (i == 0)
                    {
                        MessageBox.Show("Nem található felhasználó ilyen felhasználónévvel.");
                    }
                    else
                    {
                        foreach (DataRow row in dt.Rows)
                        {
                            if (Crypter.CheckPassword(password, row["Password"].ToString()))
                            {
                                UserLoggedinForm ulf = new UserLoggedinForm(row["Username"].ToString());
                                this.Hide();
                                ulf.Show();
                            }
                            else
                            {
                                MessageBox.Show("Nem található felhasználó ilyen jelszóval.");
                            }
                        }
                    }

                    db.CloseConnection();
                }
                catch (Exception dbE)
                {
                    MessageBox.Show(dbE.ToString());
                }

            }
        }

        //Do on registration
        private void registerButton_Click(object sender, EventArgs e)
        {
            tabControl.SelectTab(3);
        }

        //Do on back from adminlogin
        private void backButton2_Click(object sender, EventArgs e)
        {
            tabControl.SelectTab(0);
        }

        //Do on admin login
        private void adminLoginButton_Click(object sender, EventArgs e)
        {
            bool noErrorInAdminUsername = false;
            bool noErrorInAdminPassword = false;

            //Declarate the variables
            string adminUsername = adminUsernameInput.Text;
            string adminPassword = adminPasswordInput.Text;

            //Make validators
            LoginUsernameValidator UV = new LoginUsernameValidator(adminUsername);
            LoginPasswordValidator PV = new LoginPasswordValidator(adminPassword);

            //Run username validation
            try
            {
                UV.ValidUsername();
                noErrorInAdminUsername = true;
            }
            catch (LoginUsernameException Ue)
            {
                MessageBox.Show(Ue.ToString());
                noErrorInAdminUsername = false;
            }

            //Run password validation
            try
            {
                PV.ValidPassword();
                noErrorInAdminPassword = true;
            }
            catch (LoginPasswordException Pe)
            {
                MessageBox.Show(Pe.ToString());
                noErrorInAdminPassword = false;
            }

            //Logging in

            if (noErrorInAdminUsername == true && noErrorInAdminPassword == true)
            {
                try
                {
                    int i = 0;

                    Database db = new Database();
                    db.OpenConnection();
                    MySqlCommand command = db.connection.CreateCommand();
                    command.CommandType = CommandType.Text;
                    command.CommandText = "SELECT * FROM users WHERE Username='" + adminUsername + "'";
                    command.ExecuteNonQuery();
                    DataTable dt = new DataTable();
                    MySqlDataAdapter da = new MySqlDataAdapter(command);
                    da.Fill(dt);
                    i = Convert.ToInt32(dt.Rows.Count.ToString());

                    if (i == 0)
                    {
                        MessageBox.Show("Nem található felhasználó ilyen felhasználónévvel.");
                    }
                    else
                    {
                        foreach (DataRow row in dt.Rows)
                        {
                            if (Crypter.CheckPassword(adminPassword, row["Password"].ToString()))
                            {
                                AdminLoggedinForm alf = new AdminLoggedinForm(row["Username"].ToString(), row["Email"].ToString());
                                this.Hide();
                                alf.Show();
                            }
                            else
                            {
                                MessageBox.Show("Nem található felhasználó ilyen jelszóval.");
                            }
                        }
                    }
                }
                catch (Exception dbE)
                {
                    MessageBox.Show(dbE.ToString());
                }

            }
        }

        //Do on back from register
        private void backButton3_Click(object sender, EventArgs e)
        {
            tabControl.SelectTab(0);
        }

        //Do on registration
        private void registrationButton_Click(object sender, EventArgs e)
        {

        }
    }
}
