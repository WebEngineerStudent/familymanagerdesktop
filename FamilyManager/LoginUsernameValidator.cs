﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace FamilyManager
{
    public class LoginUsernameValidator
    {
        //Declaration of the username variable
        private string username;

        //Declaration of the constructor
        public LoginUsernameValidator(string username)
        {
            this.username = username;
        }

        /// <summary>
        /// Check the username
        /// </summary>
        /// <param name="username">To check username</param>
        /// <returns>Is username okay</returns>
        /// <exception cref="LoginUsernameException">When username is wrong</exception>
        #region UsernameCheck
        public bool ValidUsername()
        {
            bool ok = true;

            if (!ValidUsernameEmpty())
            {
                throw new LoginUsernameException(username, "Ki kell töltened a felhasználónév mezőt!");
            }
            if (!ValidUsernameAtleast())
            {
                throw new LoginUsernameException(username, "A felhasználónévnek minimum 4 karakternek kell lennie!");
            }
            if (!ValidUsernameWhiteSpace())
            {
                throw new LoginUsernameException(username, "A felhasználónév nem tartalmazhat szóközt!");
            }
            if (!ValidUsernameUpperLetter())
            {
                throw new LoginUsernameException(username, "A felhasználónévnek tartalmazni kell nagybetűt!");
            }
            if (!ValidUsernameLowerLetter())
            {
                throw new LoginUsernameException(username, "A felhasználónévnek tartalmazni kell kisbetűt!");
            }

            return ok;
        }
        #endregion

        #region ValidatorMethods

        //Check is username empty method
        public bool ValidUsernameEmpty()
        {
            if (string.IsNullOrEmpty(username))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        //Check is username have enought characters
        public bool ValidUsernameAtleast()
        {
            if (username.Length >= 4)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        //Check is username have enought characters
        public bool ValidUsernameWhiteSpace()
        {
            username.Trim();
            if (username.Contains(' '))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        //Check is username have uppercase letters
        public bool ValidUsernameUpperLetter()
        {
            if (Regex.IsMatch(username, @"^(?=.*[A-ZÖÜÓŐÚÉÁŰ])"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        //Check is username have lowercase letters
        public bool ValidUsernameLowerLetter()
        {
            if (Regex.IsMatch(username, @"^(?=.*[a-zöüóőúéáű])"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        #endregion
    }
}
