﻿using CryptSharp;
using MaterialSkin.Controls;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FamilyManager
{
    public partial class UserLoggedinForm : MaterialForm
    {
        String Username;
        int userID;
        int familyID;
        

        public UserLoggedinForm(string Username)
        {
            InitializeComponent();

            this.Username = Username;

            #region Create Listviews
            //Declare familys list view
            familysListView.Columns.Add("Felhasználónév");
            familysListView.Columns.Add("Vezetéknév");
            familysListView.Columns.Add("Keresztnév");
            familysListView.Columns.Add("Szerepkör");

            familysListView.Columns[0].Width = -2;
            familysListView.Columns[1].Width = -2;
            familysListView.Columns[2].Width = -2;
            familysListView.Columns[3].Width = -2;

            //Declarate programs list view
            familyProgramsListView.Columns.Add("Kép");
            familyProgramsListView.Columns.Add("Program név");
            familyProgramsListView.Columns.Add("Program leírás");
            familyProgramsListView.Columns.Add("Program dátuma");
            familyProgramsListView.Columns.Add("Tetszik");
            familyProgramsListView.Columns.Add("Nem Tetszik");

            familyProgramsListView.Columns[0].Width = -2;
            familyProgramsListView.Columns[1].Width = -2;
            familyProgramsListView.Columns[2].Width = -2;
            familyProgramsListView.Columns[3].Width = -2;
            familyProgramsListView.Columns[4].Width = -2;
            familyProgramsListView.Columns[5].Width = -2;
            
            //Declare to do exercises list view
            toDoExercisesListView.Columns.Add("Feladat neve");
            toDoExercisesListView.Columns.Add("Feladat leírása");
            toDoExercisesListView.Columns.Add("Feladat határideje");

            toDoExercisesListView.Columns[0].Width = -2;
            toDoExercisesListView.Columns[1].Width = -2;
            toDoExercisesListView.Columns[2].Width = -2;

            //Declare done exercise list view
            doneExercisesListView.Columns.Add("Feladat neve");
            doneExercisesListView.Columns.Add("Feladat leírása");
            doneExercisesListView.Columns.Add("Feladat határideje");

            doneExercisesListView.Columns[0].Width = -2;
            doneExercisesListView.Columns[1].Width = -2;
            doneExercisesListView.Columns[2].Width = -2;
            #endregion
        }

        //Do on load
        private void UserLoggedinForm_Load(object sender, EventArgs e)
        {
            this.Show();
            #region OnLoad
            try
            {
                int i = 0;

                Database db = new Database();
                db.OpenConnection();
                MySqlCommand command = db.connection.CreateCommand();
                command.CommandType = CommandType.Text;
                command.CommandText = "select * from users where username='" + Username + "'";
                command.ExecuteNonQuery();
                DataTable dt = new DataTable();
                MySqlDataAdapter da = new MySqlDataAdapter(command);
                da.Fill(dt);
                i = Convert.ToInt32(dt.Rows.Count.ToString());

                if (i == 0)
                {
                    MessageBox.Show("Nem található felhasználó ilyen felhasználónévvel.");
                }
                else
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        userProfilePicture.ImageLocation = row["profilePicture"].ToString();
                        userProfilePicture.SizeMode = PictureBoxSizeMode.StretchImage;
                        loggedInUsernameLabel.Text = row["Username"].ToString();
                        loggedInUserFirstnameLabel.Text = row["First_Name"].ToString();
                        loggedinUserLastnameLabel.Text = row["Last_Name"].ToString();
                        loggedinUserEmailLabel.Text = row["Email"].ToString();
                        userID = Convert.ToInt32(row["Id"]);
                    }
                }
            }
            catch (Exception getUserDatasEx) {
                MessageBox.Show(getUserDatasEx.ToString());
            }
            #endregion

            ownNameLabel.Text = Username;
        }
        //Do on logout
        private void selectLogoutButton_Click(object sender, EventArgs e)
        {

        }

        #region Tabcontrol Navigation

        //Do on select family tile
        private void selectFamilysButton_Click(object sender, EventArgs e)
        {
            #region FamilysLoad
            try
            {
                familysListView.Items.Clear();

                int i = 0;

                Database db = new Database();
                db.OpenConnection();
                MySqlCommand command = db.connection.CreateCommand();
                command.CommandType = CommandType.Text;
                command.CommandText = "SELECT * FROM familytouser INNER JOIN users ON users.Id = familytouser.userID INNER JOIN familys ON familytouser.familyID = familys.familyID  WHERE familytouser.familyID=(SELECT familyID FROM familytouser WHERE userID="+ userID +")";
                command.ExecuteNonQuery();
                DataTable dt = new DataTable();
                MySqlDataAdapter da = new MySqlDataAdapter(command);
                da.Fill(dt);
                i = Convert.ToInt32(dt.Rows.Count.ToString());

                if (i == 0)
                {
                    familysListView.Visible = false;
                    exitFromFamilyButton.Visible = false;
                    familyNameLabel.Visible = false;

                    connectToFamilyPanel.Visible = true;
                    createFamilyPanel.Visible = true;
                    createFamilyButton.Enabled = true;
                    connectToFamilyButton.Enabled = true;
                }
                else
                {
                    familysListView.Visible = true;
                    exitFromFamilyButton.Visible = true;
                    familyNameLabel.Visible = true;

                    connectToFamilyPanel.Visible = false;
                    createFamilyPanel.Visible = false;
                    createFamilyButton.Enabled = false;
                    connectToFamilyButton.Enabled = false;

                    foreach (DataRow row in dt.Rows)
                    {
                        ListViewItem familyItem = new ListViewItem(row["Username"].ToString());
                        familyItem.SubItems.Add(row["First_Name"].ToString());
                        familyItem.SubItems.Add(row["Last_Name"].ToString());
                        familyItem.SubItems.Add(row["Position"].ToString());
                        familysListView.Items.Add(familyItem);
                        familyNameLabel.Text = row["familyName"].ToString();
                    }
                }
            }
            catch (Exception familysE)
            {
                MessageBox.Show(familysE.ToString());
            }
            #endregion

            materialTabControl1.SelectTab(1);
        }

        //Do on select programs tile
        private void selectProgramsButton_Click(object sender, EventArgs e)
        {
            #region ProgramsLoad
            try
            {
                familyProgramsListView.Items.Clear();

                int i = 0;
                int likes = 0;
                int dislikes = 0;

                Database db = new Database();
                db.OpenConnection();
                MySqlCommand command = db.connection.CreateCommand();
                command.CommandType = CommandType.Text;
                command.CommandText = "SELECT * FROM programs INNER JOIN users ON programs.userID = users.Id INNER JOIN familys ON programs.familyID = familys.familyID INNER JOIN likes ON programs.programID = likes.programID WHERE programs.familyID=(SELECT familyID FROM familytouser WHERE userID=" + userID + ")";
                command.ExecuteNonQuery();
                DataTable dt = new DataTable();
                MySqlDataAdapter da = new MySqlDataAdapter(command);
                da.Fill(dt);
                i = Convert.ToInt32(dt.Rows.Count.ToString());

                if (i == 0)
                {
                    
                }
                else
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        if (Convert.ToBoolean(row["liked"]))
                        {
                            likes++;
                        }
                        else {
                            dislikes++;
                        }
                        ListViewItem programsItem = new ListViewItem(row["profilePicture"].ToString());
                        programsItem.SubItems.Add(row["programName"].ToString());
                        programsItem.SubItems.Add(row["programDescription"].ToString());
                        programsItem.SubItems.Add(row["programDate"].ToString());
                        programsItem.SubItems.Add(likes.ToString());
                        programsItem.SubItems.Add(dislikes.ToString());
                        familyProgramsListView.Items.Add(programsItem);
                        likes = 0;
                        dislikes = 0;
                    }
                }
            }
            catch (Exception programsE)
            {
                MessageBox.Show(programsE.ToString());
            }
            #endregion
            materialTabControl1.SelectTab(3);
        }

        //Do on select routine tile
        private void selectRoutineButton_Click(object sender, EventArgs e)
        {
<<<<<<< HEAD
            #region LoadRutines

            try {
=======
            #region LoadRoutines
            try
            {
>>>>>>> 7331d53293423a79ded621d27a66d34463476285
                int i = 0;

                DataTable temp = new DataTable();
                temp.Columns.Add("Username");
                DataRow tempRow = temp.NewRow();
                tempRow["Username"] = "Válasszon családtagot...";
                temp.Rows.Add(tempRow);

                Database db = new Database();
                db.OpenConnection();
                MySqlCommand command = db.connection.CreateCommand();
                command.CommandType = CommandType.Text;
                command.CommandText = "SELECT Username FROM users INNER JOIN familytouser ON users.Id = familytouser.userID INNER JOIN familys ON familytouser.familyID = familys.familyID  WHERE familytouser.familyID=(SELECT familyID FROM familytouser WHERE userID=" + userID + ")";
                command.ExecuteNonQuery();
                DataTable dt = new DataTable();
                MySqlDataAdapter da = new MySqlDataAdapter(command);
                da.Fill(dt);
                temp.Merge(dt);
                i = Convert.ToInt32(dt.Rows.Count.ToString());

                if (i > 0)
                {
                    familyMembersComboBox.DataSource= temp;
                    familyMembersComboBox.DisplayMember="Username";
                }
                else {

                }

                materialTabControl1.SelectTab(2);

            } catch (Exception loadRutineMembersEx) {
                MessageBox.Show(loadRutineMembersEx.ToString());
            }
<<<<<<< HEAD
=======

            try
            {
                Database db = new Database();
                db.OpenConnection();
                MySqlCommand command = db.connection.CreateCommand();
                command.CommandType = CommandType.Text;
                command.CommandText = "SELECT exerciseName, exerciseCell FROM exercises INNER JOIN users ON exercises.userID = users.Id WHERE Username='" + Username + "'";
                command.ExecuteNonQuery();
                DataTable dt = new DataTable();
                MySqlDataAdapter da = new MySqlDataAdapter(command);
                da.Fill(dt);
                ownRoutineDataGrid.DataSource = dt;
            }
            catch (Exception loadOwnRoutineEx) {
                MessageBox.Show(loadOwnRoutineEx.ToString());
            }
>>>>>>> 7331d53293423a79ded621d27a66d34463476285
            #endregion
        }

        //Do on select exercise tile
        private void selectExercisesButton_Click(object sender, EventArgs e)
        {
            #region ExercisesLoad
            try
            {
                toDoExercisesListView.Items.Clear();
                doneExercisesListView.Items.Clear();

                int i = 0;

                Database db = new Database();
                db.OpenConnection();
                MySqlCommand command = db.connection.CreateCommand();
                command.CommandType = CommandType.Text;
                command.CommandText = "SELECT * FROM todolist WHERE toUserID=" + userID;
                command.ExecuteNonQuery();
                DataTable dt = new DataTable();
                MySqlDataAdapter da = new MySqlDataAdapter(command);
                da.Fill(dt);
                i = Convert.ToInt32(dt.Rows.Count.ToString());

                if (i == 0)
                {

                }
                else
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        if (Convert.ToInt32(row["isDone"]) == 0)
                        {
                            ListViewItem exercisesItem = new ListViewItem(row["exerciseName"].ToString());
                            exercisesItem.SubItems.Add(row["exerciseDescription"].ToString());
                            exercisesItem.SubItems.Add(row["deadLineDate"].ToString());
                            toDoExercisesListView.Items.Add(exercisesItem);
                        }
                        else if (Convert.ToInt32(row["isDone"]) == 1) {
                            ListViewItem doneExercisesItem = new ListViewItem(row["exerciseName"].ToString());
                            doneExercisesItem.SubItems.Add(row["exerciseDescription"].ToString());
                            doneExercisesItem.SubItems.Add(row["deadLineDate"].ToString());
                            doneExercisesListView.Items.Add(doneExercisesItem);
                        }
                    }
                }
            }
            catch (Exception exerciseE)
            {
                MessageBox.Show(exerciseE.ToString());
            }
            #endregion
            materialTabControl1.SelectTab(4);
        }

        //Do on select galery tile
        private void selectGalleryButton_Click(object sender, EventArgs e)
        {
            materialTabControl1.SelectTab(5);
        }

        //Do on select chatroom tile
        private void selectChatroomButton_Click(object sender, EventArgs e)
        {
            materialTabControl1.SelectTab(6);
        }

        //Do on step back from family page
        private void backFromFamilysButton_Click(object sender, EventArgs e)
        {
            materialTabControl1.SelectTab(0);
        }

        //Do on step back from rotuine page
        private void backFromRutioneButtom_Click(object sender, EventArgs e)
        {
            materialTabControl1.SelectTab(0);
        }

        //Do on step back from programs page
        private void backFromProgramsButton_Click(object sender, EventArgs e)
        {
            materialTabControl1.SelectTab(0);
        }

        //Do on step back from exercises page
        private void backFromExercisesButton_Click(object sender, EventArgs e)
        {
            materialTabControl1.SelectTab(0);
        }

        //Do on step back from galery page
        private void backFromGaleryButton_Click(object sender, EventArgs e)
        {
            materialTabControl1.SelectTab(0);
        }

        //Do on step back from chat page
        private void backFromChatButton_Click(object sender, EventArgs e)
        {
            materialTabControl1.SelectTab(0);
        }

        #endregion

        //Create family button
        private void createFamilyButton_Click(object sender, EventArgs e)
        {
            #region Create family
            CreateFamilyForm createFamilyDialog = new CreateFamilyForm();

            // Show createFamilyDialog as a modal dialog and determine if DialogResult = OK.
            if (createFamilyDialog.ShowDialog(this) == DialogResult.OK)
            {
                try
                {
                    string createdFamilyName = createFamilyDialog.familyName;
                    Database db = new Database();
                    db.OpenConnection();
                    MySqlCommand command = db.connection.CreateCommand();
                    command.CommandType = CommandType.Text;
                    command.CommandText = "INSERT INTO familys (familyName, adminID) VALUES ('"+ createdFamilyName + "', "+userID+")";
                    command.ExecuteNonQuery();
                    MySqlCommand command2 = db.connection.CreateCommand();
                    command2.CommandType = CommandType.Text;
                    command2.CommandText = "SELECT familyID FROM familys WHERE familyName ='" + createdFamilyName + "'";
                    command2.ExecuteNonQuery();
                    DataTable dt = new DataTable();
                    MySqlDataAdapter da = new MySqlDataAdapter(command2);
                    da.Fill(dt);
                    int i = Convert.ToInt32(dt.Rows.Count.ToString());

                    if (i == 1)
                    {
                        foreach (DataRow row in dt.Rows)
                        {
                            familyID = Convert.ToInt32(row["familyID"]);
                        }
                        MySqlCommand command3 = db.connection.CreateCommand();
                        command3.CommandType = CommandType.Text;
                        command3.CommandText = "INSERT INTO familytouser (familyID, userID) VALUES (" + familyID + ", " + userID + ")";
                        command3.ExecuteNonQuery();
                        materialTabControl1.SelectTab(0);
                    }
                    else {
                        MessageBox.Show("Hiba a család létrehozása közben.");
                    }
                }
                catch (Exception createFamilyEx) {
                    MessageBox.Show(createFamilyEx.ToString());
                }
            }
            else
            {
                
            }
            createFamilyDialog.Dispose();
            #endregion
        }

        //Connect to family button
        private void connectToFamilyButton_Click(object sender, EventArgs e)
        {
            #region Connect to family
            ConnectFamilyForm connectFamilyDialog = new ConnectFamilyForm();

            // Show connectFamilyDialog as a modal dialog and determine if DialogResult = OK.
            if (connectFamilyDialog.ShowDialog(this) == DialogResult.OK)
            {
                try
                {
                    int searchedFamilyID = connectFamilyDialog.selectedFamilyID;
                    Database db = new Database();
                    db.OpenConnection();
                    MySqlCommand command = db.connection.CreateCommand();
                    command.CommandType = CommandType.Text;
                    command.CommandText = "INSERT INTO familytouser (familyID, userID) VALUES (" + searchedFamilyID + ", " + userID + ")";
                    command.ExecuteNonQuery();
                    materialTabControl1.SelectTab(0);
                }
                catch (Exception connectFamilyEx)
                {
                    MessageBox.Show(connectFamilyEx.ToString());
                }
            }
            else
            {

            }
            connectFamilyDialog.Dispose();
            #endregion
        }

        private void familyMembersComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            #region RoutineCombobox Index Change
            string selectedMember = familyMembersComboBox.Text;

            if (selectedMember == "Válasszon családtagot...")
            {
                selectedMemeberNameLabel.Visible = false;
                selectedMemeberNameLabel.Text = "";
                memberRoutineDataGrid.DataSource = null;
            }
            else
            {
                selectedMemeberNameLabel.Visible = true;
                try
                {
                    int i = 0;

                    Database db = new Database();
                    db.OpenConnection();
                    MySqlCommand command = db.connection.CreateCommand();
                    command.CommandType = CommandType.Text;
                    command.CommandText = "SELECT * FROM exercises INNER JOIN users ON exercises.userID = users.Id WHERE Username='" + selectedMember + "'";
                    command.ExecuteNonQuery();
                    DataTable dt = new DataTable();
                    MySqlDataAdapter da = new MySqlDataAdapter(command);
                    da.Fill(dt);
                    i = Convert.ToInt32(dt.Rows.Count.ToString());

                    if (i > 0)
                    {
                        foreach (DataRow row in dt.Rows)
                        {
                            selectedMemeberNameLabel.Text = row["Last_Name"].ToString() + " " + row["First_Name"].ToString();
                        }

                        MySqlCommand command2 = db.connection.CreateCommand();
                        command2.CommandType = CommandType.Text;
                        command2.CommandText = "SELECT exerciseName, exerciseCell FROM exercises INNER JOIN users ON exercises.userID = users.Id WHERE Username='" + selectedMember + "'";
                        command2.ExecuteNonQuery();
                        DataTable dtRoutine = new DataTable();
                        MySqlDataAdapter dataAdapterRoutine = new MySqlDataAdapter(command2);
                        dataAdapterRoutine.Fill(dtRoutine);
                        memberRoutineDataGrid.DataSource=dtRoutine;
                    }
                    else
                    {
                        foreach (DataRow row in dt.Rows)
                        {
                            selectedMemeberNameLabel.Text = row["Last_Name"].ToString() + " " + row["First_Name"].ToString();
                        }
                    }

                    materialTabControl1.SelectTab(2);

                }
                catch (Exception loadRutineMembersEx)
                {
                    MessageBox.Show(loadRutineMembersEx.ToString());
                }
            }
            #endregion
        }

        private void createProgramButton_Click(object sender, EventArgs e)
        {
            string programName = programNameInput.Text;
            string programDescription = programDescriptionInput.Text;
            DateTime programDate = programDatePicker.Value;

            if (programName != "" && programDate != null && programDescription != "")
            {
                Database db = new Database();
                db.OpenConnection();
                MySqlCommand command = db.connection.CreateCommand();
                command.CommandType = CommandType.Text;
                command.CommandText = "INSERT INTO programs (familyID, userID, programName, programDate, programDescription) VALUES (" + familyID + ", " + userID + ", " + programName + ", " + programDate + ", " + programDescription + ")";
                command.ExecuteNonQuery();

                selectProgramsButton.PerformClick();
            }
            else {

            }
        }
    }

}
