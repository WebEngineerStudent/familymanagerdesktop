﻿namespace FamilyManager
{
    partial class UserLoggedinForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.loggedInUserUsernameLabel = new System.Windows.Forms.Label();
            this.loggedInUserFirstnameLabel = new System.Windows.Forms.Label();
            this.loggedinUserLastnameLabel = new System.Windows.Forms.Label();
            this.loggedinUserEmailLabel = new System.Windows.Forms.Label();
            this.userDatasPanel = new System.Windows.Forms.Panel();
            this.selectLogoutButton = new System.Windows.Forms.Button();
            this.loggedInUsernameLabel = new System.Windows.Forms.Label();
            this.userProfilePicture = new System.Windows.Forms.PictureBox();
            this.materialTabControl1 = new MaterialSkin.Controls.MaterialTabControl();
            this.homeTab = new System.Windows.Forms.TabPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.selectFamilysButton = new System.Windows.Forms.Button();
            this.selectExercisesButton = new System.Windows.Forms.Button();
            this.selectChatroomButton = new System.Windows.Forms.Button();
            this.selectProgramsButton = new System.Windows.Forms.Button();
            this.selectRoutineButton = new System.Windows.Forms.Button();
            this.selectGalleryButton = new System.Windows.Forms.Button();
            this.familyTab = new System.Windows.Forms.TabPage();
            this.connectToFamilyPanel = new System.Windows.Forms.Panel();
            this.connectToFamilyButton = new System.Windows.Forms.Button();
            this.connectFamilyLabel = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.createFamilyPanel = new System.Windows.Forms.Panel();
            this.createFamilyButton = new System.Windows.Forms.Button();
            this.createFamilyLabel = new System.Windows.Forms.Label();
            this.createFamilyPicture = new System.Windows.Forms.PictureBox();
            this.exitFromFamilyButton = new System.Windows.Forms.Button();
            this.backFromFamilysButton = new System.Windows.Forms.Button();
            this.familyNameLabel = new System.Windows.Forms.Label();
            this.familysListView = new MaterialSkin.Controls.MaterialListView();
            this.routineTab = new System.Windows.Forms.TabPage();
            this.familyMembersComboBoxLabel = new System.Windows.Forms.Label();
            this.familyMembersComboBox = new System.Windows.Forms.ComboBox();
            this.routinePanel = new System.Windows.Forms.Panel();
<<<<<<< HEAD
=======
            this.deleteOwnRoutineButton = new System.Windows.Forms.Button();
            this.editOwnRoutineButton = new System.Windows.Forms.Button();
            this.addOwnRoutineButton = new System.Windows.Forms.Button();
            this.ownNameLabel = new System.Windows.Forms.Label();
            this.ownRoutineDataGrid = new System.Windows.Forms.DataGridView();
            this.memberRoutineDataGrid = new System.Windows.Forms.DataGridView();
>>>>>>> 7331d53293423a79ded621d27a66d34463476285
            this.selectedMemeberNameLabel = new System.Windows.Forms.Label();
            this.backFromRutioneButtom = new System.Windows.Forms.Button();
            this.programsTab = new System.Windows.Forms.TabPage();
            this.createFamilyProgramPanel = new System.Windows.Forms.Panel();
            this.programDatePicker = new System.Windows.Forms.DateTimePicker();
            this.createProgramButton = new System.Windows.Forms.Button();
            this.programDateLabel = new System.Windows.Forms.Label();
            this.programDescriptionInput = new System.Windows.Forms.TextBox();
            this.programDescriptionLabel = new System.Windows.Forms.Label();
            this.programNameInput = new System.Windows.Forms.TextBox();
            this.programNameLabel = new System.Windows.Forms.Label();
            this.createProgramLabel = new System.Windows.Forms.Label();
            this.familyProgramsPanel = new System.Windows.Forms.Panel();
            this.familyProgramsListView = new MaterialSkin.Controls.MaterialListView();
            this.backFromProgramsButton = new System.Windows.Forms.Button();
            this.exercisesTab = new System.Windows.Forms.TabPage();
            this.exercisesHolderPanel = new System.Windows.Forms.Panel();
            this.doneExerciseLabel = new System.Windows.Forms.Label();
            this.toDoExercisesLabel = new System.Windows.Forms.Label();
            this.doneExercisesListView = new MaterialSkin.Controls.MaterialListView();
            this.toDoExercisesListView = new MaterialSkin.Controls.MaterialListView();
            this.giveExercisePanel = new System.Windows.Forms.Panel();
            this.exerciseDescriptionInput = new System.Windows.Forms.TextBox();
            this.targetUserComboBox = new System.Windows.Forms.ComboBox();
            this.giveExerciseButton = new System.Windows.Forms.Button();
            this.exerciseDescriptionLabel = new System.Windows.Forms.Label();
            this.exerciseNameInput = new System.Windows.Forms.TextBox();
            this.exerciseNameLabel = new System.Windows.Forms.Label();
            this.targetUserLabel = new System.Windows.Forms.Label();
            this.giveExerciseLabel = new System.Windows.Forms.Label();
            this.backFromExercisesButton = new System.Windows.Forms.Button();
            this.galeryTab = new System.Windows.Forms.TabPage();
            this.backFromGaleryButton = new System.Windows.Forms.Button();
            this.chatroomTab = new System.Windows.Forms.TabPage();
            this.backFromChatButton = new System.Windows.Forms.Button();
<<<<<<< HEAD
=======
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
>>>>>>> 7331d53293423a79ded621d27a66d34463476285
            this.userDatasPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.userProfilePicture)).BeginInit();
            this.materialTabControl1.SuspendLayout();
            this.homeTab.SuspendLayout();
            this.panel1.SuspendLayout();
            this.familyTab.SuspendLayout();
            this.connectToFamilyPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.createFamilyPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.createFamilyPicture)).BeginInit();
            this.routineTab.SuspendLayout();
            this.routinePanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ownRoutineDataGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memberRoutineDataGrid)).BeginInit();
            this.programsTab.SuspendLayout();
            this.createFamilyProgramPanel.SuspendLayout();
            this.familyProgramsPanel.SuspendLayout();
            this.exercisesTab.SuspendLayout();
            this.exercisesHolderPanel.SuspendLayout();
            this.giveExercisePanel.SuspendLayout();
            this.galeryTab.SuspendLayout();
            this.chatroomTab.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel5.SuspendLayout();
            this.SuspendLayout();
            // 
            // loggedInUserUsernameLabel
            // 
            this.loggedInUserUsernameLabel.AutoSize = true;
            this.loggedInUserUsernameLabel.Location = new System.Drawing.Point(82, 73);
            this.loggedInUserUsernameLabel.Name = "loggedInUserUsernameLabel";
            this.loggedInUserUsernameLabel.Size = new System.Drawing.Size(0, 13);
            this.loggedInUserUsernameLabel.TabIndex = 1;
            // 
            // loggedInUserFirstnameLabel
            // 
            this.loggedInUserFirstnameLabel.AutoSize = true;
            this.loggedInUserFirstnameLabel.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.loggedInUserFirstnameLabel.Location = new System.Drawing.Point(82, 30);
            this.loggedInUserFirstnameLabel.Name = "loggedInUserFirstnameLabel";
            this.loggedInUserFirstnameLabel.Size = new System.Drawing.Size(40, 20);
            this.loggedInUserFirstnameLabel.TabIndex = 2;
            this.loggedInUserFirstnameLabel.Text = "First";
            // 
            // loggedinUserLastnameLabel
            // 
            this.loggedinUserLastnameLabel.AutoSize = true;
            this.loggedinUserLastnameLabel.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.loggedinUserLastnameLabel.Location = new System.Drawing.Point(129, 30);
            this.loggedinUserLastnameLabel.Name = "loggedinUserLastnameLabel";
            this.loggedinUserLastnameLabel.Size = new System.Drawing.Size(38, 20);
            this.loggedinUserLastnameLabel.TabIndex = 3;
            this.loggedinUserLastnameLabel.Text = "Last";
            // 
            // loggedinUserEmailLabel
            // 
            this.loggedinUserEmailLabel.AutoSize = true;
            this.loggedinUserEmailLabel.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.loggedinUserEmailLabel.Location = new System.Drawing.Point(82, 50);
            this.loggedinUserEmailLabel.Name = "loggedinUserEmailLabel";
            this.loggedinUserEmailLabel.Size = new System.Drawing.Size(47, 20);
            this.loggedinUserEmailLabel.TabIndex = 4;
            this.loggedinUserEmailLabel.Text = "Email";
            // 
            // userDatasPanel
            // 
            this.userDatasPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.userDatasPanel.BackColor = System.Drawing.Color.OrangeRed;
            this.userDatasPanel.Controls.Add(this.selectLogoutButton);
            this.userDatasPanel.Controls.Add(this.loggedInUsernameLabel);
            this.userDatasPanel.Controls.Add(this.userProfilePicture);
            this.userDatasPanel.Controls.Add(this.loggedinUserEmailLabel);
            this.userDatasPanel.Controls.Add(this.loggedInUserFirstnameLabel);
            this.userDatasPanel.Controls.Add(this.loggedinUserLastnameLabel);
            this.userDatasPanel.Location = new System.Drawing.Point(-2, 63);
            this.userDatasPanel.Name = "userDatasPanel";
            this.userDatasPanel.Size = new System.Drawing.Size(905, 75);
            this.userDatasPanel.TabIndex = 5;
            // 
            // selectLogoutButton
            // 
            this.selectLogoutButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.selectLogoutButton.BackColor = System.Drawing.Color.DarkOrange;
            this.selectLogoutButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.selectLogoutButton.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.selectLogoutButton.Location = new System.Drawing.Point(767, 19);
            this.selectLogoutButton.Name = "selectLogoutButton";
            this.selectLogoutButton.Size = new System.Drawing.Size(124, 34);
            this.selectLogoutButton.TabIndex = 7;
            this.selectLogoutButton.Text = "Kijelentkezés";
            this.selectLogoutButton.UseVisualStyleBackColor = false;
            this.selectLogoutButton.Click += new System.EventHandler(this.selectLogoutButton_Click);
            // 
            // loggedInUsernameLabel
            // 
            this.loggedInUsernameLabel.AutoSize = true;
            this.loggedInUsernameLabel.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.loggedInUsernameLabel.Location = new System.Drawing.Point(83, 10);
            this.loggedInUsernameLabel.Name = "loggedInUsernameLabel";
            this.loggedInUsernameLabel.Size = new System.Drawing.Size(80, 20);
            this.loggedInUsernameLabel.TabIndex = 6;
            this.loggedInUsernameLabel.Text = "Username";
            // 
            // userProfilePicture
            // 
            this.userProfilePicture.BackColor = System.Drawing.Color.Transparent;
            this.userProfilePicture.Location = new System.Drawing.Point(12, 10);
            this.userProfilePicture.Name = "userProfilePicture";
            this.userProfilePicture.Size = new System.Drawing.Size(64, 58);
            this.userProfilePicture.TabIndex = 0;
            this.userProfilePicture.TabStop = false;
            // 
            // materialTabControl1
            // 
            this.materialTabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.materialTabControl1.Controls.Add(this.homeTab);
            this.materialTabControl1.Controls.Add(this.familyTab);
            this.materialTabControl1.Controls.Add(this.routineTab);
            this.materialTabControl1.Controls.Add(this.programsTab);
            this.materialTabControl1.Controls.Add(this.exercisesTab);
            this.materialTabControl1.Controls.Add(this.galeryTab);
            this.materialTabControl1.Controls.Add(this.chatroomTab);
            this.materialTabControl1.Depth = 0;
            this.materialTabControl1.Location = new System.Drawing.Point(1, 144);
            this.materialTabControl1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialTabControl1.Name = "materialTabControl1";
            this.materialTabControl1.SelectedIndex = 0;
            this.materialTabControl1.Size = new System.Drawing.Size(899, 356);
            this.materialTabControl1.TabIndex = 6;
            // 
            // homeTab
            // 
            this.homeTab.Controls.Add(this.panel1);
            this.homeTab.Location = new System.Drawing.Point(4, 22);
            this.homeTab.Name = "homeTab";
            this.homeTab.Padding = new System.Windows.Forms.Padding(3);
            this.homeTab.Size = new System.Drawing.Size(891, 330);
            this.homeTab.TabIndex = 0;
            this.homeTab.Text = "Home";
            this.homeTab.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.selectFamilysButton);
            this.panel1.Controls.Add(this.selectExercisesButton);
            this.panel1.Controls.Add(this.selectChatroomButton);
            this.panel1.Controls.Add(this.selectProgramsButton);
            this.panel1.Controls.Add(this.selectRoutineButton);
            this.panel1.Controls.Add(this.selectGalleryButton);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(885, 324);
            this.panel1.TabIndex = 12;
            // 
            // selectFamilysButton
            // 
            this.selectFamilysButton.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.selectFamilysButton.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.selectFamilysButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.selectFamilysButton.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.selectFamilysButton.Image = global::FamilyManager.Properties.Resources.icon;
            this.selectFamilysButton.Location = new System.Drawing.Point(194, 28);
            this.selectFamilysButton.Name = "selectFamilysButton";
            this.selectFamilysButton.Size = new System.Drawing.Size(151, 140);
            this.selectFamilysButton.TabIndex = 6;
            this.selectFamilysButton.Text = "Család";
            this.selectFamilysButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.selectFamilysButton.UseVisualStyleBackColor = false;
            this.selectFamilysButton.Click += new System.EventHandler(this.selectFamilysButton_Click);
            // 
            // selectExercisesButton
            // 
            this.selectExercisesButton.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.selectExercisesButton.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.selectExercisesButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.selectExercisesButton.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.selectExercisesButton.Image = global::FamilyManager.Properties.Resources.icon;
            this.selectExercisesButton.Location = new System.Drawing.Point(194, 175);
            this.selectExercisesButton.Name = "selectExercisesButton";
            this.selectExercisesButton.Size = new System.Drawing.Size(151, 129);
            this.selectExercisesButton.TabIndex = 9;
            this.selectExercisesButton.Text = "Feladatok";
            this.selectExercisesButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.selectExercisesButton.UseVisualStyleBackColor = false;
            this.selectExercisesButton.Click += new System.EventHandler(this.selectExercisesButton_Click);
            // 
            // selectChatroomButton
            // 
            this.selectChatroomButton.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.selectChatroomButton.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.selectChatroomButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.selectChatroomButton.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.selectChatroomButton.Image = global::FamilyManager.Properties.Resources.icon;
            this.selectChatroomButton.Location = new System.Drawing.Point(525, 175);
            this.selectChatroomButton.Name = "selectChatroomButton";
            this.selectChatroomButton.Size = new System.Drawing.Size(146, 129);
            this.selectChatroomButton.TabIndex = 11;
            this.selectChatroomButton.Text = "Társalgó";
            this.selectChatroomButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.selectChatroomButton.UseVisualStyleBackColor = false;
            this.selectChatroomButton.Click += new System.EventHandler(this.selectChatroomButton_Click);
            // 
            // selectProgramsButton
            // 
            this.selectProgramsButton.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.selectProgramsButton.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.selectProgramsButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.selectProgramsButton.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.selectProgramsButton.Image = global::FamilyManager.Properties.Resources.icon;
            this.selectProgramsButton.Location = new System.Drawing.Point(525, 28);
            this.selectProgramsButton.Name = "selectProgramsButton";
            this.selectProgramsButton.Size = new System.Drawing.Size(146, 140);
            this.selectProgramsButton.TabIndex = 8;
            this.selectProgramsButton.Text = "Családi programok";
            this.selectProgramsButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.selectProgramsButton.UseVisualStyleBackColor = false;
            this.selectProgramsButton.Click += new System.EventHandler(this.selectProgramsButton_Click);
            // 
            // selectRoutineButton
            // 
            this.selectRoutineButton.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.selectRoutineButton.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.selectRoutineButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.selectRoutineButton.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.selectRoutineButton.Image = global::FamilyManager.Properties.Resources.icon;
            this.selectRoutineButton.Location = new System.Drawing.Point(357, 28);
            this.selectRoutineButton.Name = "selectRoutineButton";
            this.selectRoutineButton.Size = new System.Drawing.Size(153, 140);
            this.selectRoutineButton.TabIndex = 7;
            this.selectRoutineButton.Text = "Napi rutin";
            this.selectRoutineButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.selectRoutineButton.UseVisualStyleBackColor = false;
            this.selectRoutineButton.Click += new System.EventHandler(this.selectRoutineButton_Click);
            // 
            // selectGalleryButton
            // 
            this.selectGalleryButton.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.selectGalleryButton.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.selectGalleryButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.selectGalleryButton.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.selectGalleryButton.Image = global::FamilyManager.Properties.Resources.icon;
            this.selectGalleryButton.Location = new System.Drawing.Point(357, 175);
            this.selectGalleryButton.Name = "selectGalleryButton";
            this.selectGalleryButton.Size = new System.Drawing.Size(153, 129);
            this.selectGalleryButton.TabIndex = 10;
            this.selectGalleryButton.Text = "Galéria";
            this.selectGalleryButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.selectGalleryButton.UseVisualStyleBackColor = false;
            this.selectGalleryButton.Click += new System.EventHandler(this.selectGalleryButton_Click);
            // 
            // familyTab
            // 
            this.familyTab.Controls.Add(this.connectToFamilyPanel);
            this.familyTab.Controls.Add(this.createFamilyPanel);
            this.familyTab.Controls.Add(this.exitFromFamilyButton);
            this.familyTab.Controls.Add(this.backFromFamilysButton);
            this.familyTab.Controls.Add(this.familyNameLabel);
            this.familyTab.Controls.Add(this.familysListView);
            this.familyTab.Location = new System.Drawing.Point(4, 22);
            this.familyTab.Name = "familyTab";
            this.familyTab.Padding = new System.Windows.Forms.Padding(3);
            this.familyTab.Size = new System.Drawing.Size(891, 330);
            this.familyTab.TabIndex = 1;
            this.familyTab.Text = "Család";
            this.familyTab.UseVisualStyleBackColor = true;
            // 
            // connectToFamilyPanel
            // 
            this.connectToFamilyPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.connectToFamilyPanel.BackColor = System.Drawing.Color.Silver;
            this.connectToFamilyPanel.Controls.Add(this.connectToFamilyButton);
            this.connectToFamilyPanel.Controls.Add(this.connectFamilyLabel);
            this.connectToFamilyPanel.Controls.Add(this.pictureBox1);
            this.connectToFamilyPanel.Location = new System.Drawing.Point(486, 77);
            this.connectToFamilyPanel.Name = "connectToFamilyPanel";
            this.connectToFamilyPanel.Size = new System.Drawing.Size(282, 245);
            this.connectToFamilyPanel.TabIndex = 5;
            // 
            // connectToFamilyButton
            // 
            this.connectToFamilyButton.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.connectToFamilyButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.connectToFamilyButton.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.connectToFamilyButton.Location = new System.Drawing.Point(47, 181);
            this.connectToFamilyButton.Name = "connectToFamilyButton";
            this.connectToFamilyButton.Size = new System.Drawing.Size(178, 49);
            this.connectToFamilyButton.TabIndex = 2;
            this.connectToFamilyButton.Text = "Csatlakozás";
            this.connectToFamilyButton.UseVisualStyleBackColor = false;
            this.connectToFamilyButton.Click += new System.EventHandler(this.connectToFamilyButton_Click);
            // 
            // connectFamilyLabel
            // 
            this.connectFamilyLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.connectFamilyLabel.AutoSize = true;
            this.connectFamilyLabel.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.connectFamilyLabel.Location = new System.Drawing.Point(28, 148);
            this.connectFamilyLabel.Name = "connectFamilyLabel";
            this.connectFamilyLabel.Size = new System.Drawing.Size(228, 30);
            this.connectFamilyLabel.TabIndex = 1;
            this.connectFamilyLabel.Text = "Csatlakozás családhoz";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = global::FamilyManager.Properties.Resources.searchFamilyIcon;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(60, 22);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(153, 123);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // createFamilyPanel
            // 
            this.createFamilyPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.createFamilyPanel.BackColor = System.Drawing.Color.Silver;
            this.createFamilyPanel.Controls.Add(this.createFamilyButton);
            this.createFamilyPanel.Controls.Add(this.createFamilyLabel);
            this.createFamilyPanel.Controls.Add(this.createFamilyPicture);
            this.createFamilyPanel.Location = new System.Drawing.Point(126, 77);
            this.createFamilyPanel.Name = "createFamilyPanel";
            this.createFamilyPanel.Size = new System.Drawing.Size(282, 245);
            this.createFamilyPanel.TabIndex = 4;
            // 
            // createFamilyButton
            // 
            this.createFamilyButton.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.createFamilyButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.createFamilyButton.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.createFamilyButton.Location = new System.Drawing.Point(47, 181);
            this.createFamilyButton.Name = "createFamilyButton";
            this.createFamilyButton.Size = new System.Drawing.Size(178, 49);
            this.createFamilyButton.TabIndex = 2;
            this.createFamilyButton.Text = "+ Létrehozás";
            this.createFamilyButton.UseVisualStyleBackColor = false;
            this.createFamilyButton.Click += new System.EventHandler(this.createFamilyButton_Click);
            // 
            // createFamilyLabel
            // 
            this.createFamilyLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.createFamilyLabel.AutoSize = true;
            this.createFamilyLabel.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.createFamilyLabel.Location = new System.Drawing.Point(42, 148);
            this.createFamilyLabel.Name = "createFamilyLabel";
            this.createFamilyLabel.Size = new System.Drawing.Size(193, 30);
            this.createFamilyLabel.TabIndex = 1;
            this.createFamilyLabel.Text = "Család létrehozása";
            // 
            // createFamilyPicture
            // 
            this.createFamilyPicture.BackgroundImage = global::FamilyManager.Properties.Resources.createFamilyIcon;
            this.createFamilyPicture.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.createFamilyPicture.Location = new System.Drawing.Point(60, 22);
            this.createFamilyPicture.Name = "createFamilyPicture";
            this.createFamilyPicture.Size = new System.Drawing.Size(153, 123);
            this.createFamilyPicture.TabIndex = 0;
            this.createFamilyPicture.TabStop = false;
            // 
            // exitFromFamilyButton
            // 
            this.exitFromFamilyButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.exitFromFamilyButton.BackColor = System.Drawing.Color.OrangeRed;
            this.exitFromFamilyButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.exitFromFamilyButton.Location = new System.Drawing.Point(773, 9);
            this.exitFromFamilyButton.Name = "exitFromFamilyButton";
            this.exitFromFamilyButton.Size = new System.Drawing.Size(112, 48);
            this.exitFromFamilyButton.TabIndex = 3;
            this.exitFromFamilyButton.Text = "Kilépés a családból";
            this.exitFromFamilyButton.UseVisualStyleBackColor = false;
            // 
            // backFromFamilysButton
            // 
            this.backFromFamilysButton.BackColor = System.Drawing.Color.DarkOrange;
            this.backFromFamilysButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.backFromFamilysButton.Location = new System.Drawing.Point(3, 3);
            this.backFromFamilysButton.Name = "backFromFamilysButton";
            this.backFromFamilysButton.Size = new System.Drawing.Size(112, 48);
            this.backFromFamilysButton.TabIndex = 2;
            this.backFromFamilysButton.Text = "Vissza";
            this.backFromFamilysButton.UseVisualStyleBackColor = false;
            this.backFromFamilysButton.Click += new System.EventHandler(this.backFromFamilysButton_Click);
            // 
            // familyNameLabel
            // 
            this.familyNameLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.familyNameLabel.AutoSize = true;
            this.familyNameLabel.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.familyNameLabel.Location = new System.Drawing.Point(369, 22);
            this.familyNameLabel.Name = "familyNameLabel";
            this.familyNameLabel.Size = new System.Drawing.Size(94, 20);
            this.familyNameLabel.TabIndex = 1;
            this.familyNameLabel.Text = "Familyname";
            // 
            // familysListView
            // 
            this.familysListView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.familysListView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.familysListView.Depth = 0;
            this.familysListView.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F);
            this.familysListView.FullRowSelect = true;
            this.familysListView.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.familysListView.Location = new System.Drawing.Point(1, 77);
            this.familysListView.MouseLocation = new System.Drawing.Point(-1, -1);
            this.familysListView.MouseState = MaterialSkin.MouseState.OUT;
            this.familysListView.Name = "familysListView";
            this.familysListView.OwnerDraw = true;
            this.familysListView.Size = new System.Drawing.Size(887, 253);
            this.familysListView.TabIndex = 0;
            this.familysListView.UseCompatibleStateImageBehavior = false;
            this.familysListView.View = System.Windows.Forms.View.Details;
            // 
            // routineTab
            // 
            this.routineTab.Controls.Add(this.familyMembersComboBoxLabel);
            this.routineTab.Controls.Add(this.familyMembersComboBox);
            this.routineTab.Controls.Add(this.routinePanel);
            this.routineTab.Controls.Add(this.backFromRutioneButtom);
            this.routineTab.Location = new System.Drawing.Point(4, 22);
            this.routineTab.Name = "routineTab";
            this.routineTab.Size = new System.Drawing.Size(891, 330);
            this.routineTab.TabIndex = 2;
            this.routineTab.Text = "Napi rutin";
            this.routineTab.UseVisualStyleBackColor = true;
            // 
            // familyMembersComboBoxLabel
            // 
            this.familyMembersComboBoxLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.familyMembersComboBoxLabel.AutoSize = true;
            this.familyMembersComboBoxLabel.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.familyMembersComboBoxLabel.Location = new System.Drawing.Point(611, 19);
            this.familyMembersComboBoxLabel.Name = "familyMembersComboBoxLabel";
            this.familyMembersComboBoxLabel.Size = new System.Drawing.Size(81, 20);
            this.familyMembersComboBoxLabel.TabIndex = 6;
            this.familyMembersComboBoxLabel.Text = "Családtag:";
            // 
            // familyMembersComboBox
            // 
            this.familyMembersComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.familyMembersComboBox.FormattingEnabled = true;
            this.familyMembersComboBox.Location = new System.Drawing.Point(698, 18);
            this.familyMembersComboBox.Name = "familyMembersComboBox";
            this.familyMembersComboBox.Size = new System.Drawing.Size(183, 21);
            this.familyMembersComboBox.TabIndex = 5;
            this.familyMembersComboBox.SelectedIndexChanged += new System.EventHandler(this.familyMembersComboBox_SelectedIndexChanged);
            this.familyMembersComboBox.SelectedValueChanged += new System.EventHandler(this.familyMembersComboBox_SelectedIndexChanged);
            // 
            // routinePanel
            // 
            this.routinePanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.routinePanel.Controls.Add(this.panel3);
            this.routinePanel.Controls.Add(this.panel2);
            this.routinePanel.Location = new System.Drawing.Point(5, 66);
            this.routinePanel.Name = "routinePanel";
            this.routinePanel.Size = new System.Drawing.Size(883, 264);
            this.routinePanel.TabIndex = 4;
            // 
<<<<<<< HEAD
            // selectedMemeberNameLabel
            // 
            this.selectedMemeberNameLabel.AutoSize = true;
            this.selectedMemeberNameLabel.Location = new System.Drawing.Point(4, 4);
=======
            // deleteOwnRoutineButton
            // 
            this.deleteOwnRoutineButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.deleteOwnRoutineButton.BackColor = System.Drawing.Color.Coral;
            this.deleteOwnRoutineButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.deleteOwnRoutineButton.Location = new System.Drawing.Point(321, 106);
            this.deleteOwnRoutineButton.Name = "deleteOwnRoutineButton";
            this.deleteOwnRoutineButton.Size = new System.Drawing.Size(75, 23);
            this.deleteOwnRoutineButton.TabIndex = 6;
            this.deleteOwnRoutineButton.Text = "Törlés";
            this.deleteOwnRoutineButton.UseVisualStyleBackColor = false;
            // 
            // editOwnRoutineButton
            // 
            this.editOwnRoutineButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.editOwnRoutineButton.BackColor = System.Drawing.Color.Coral;
            this.editOwnRoutineButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.editOwnRoutineButton.Location = new System.Drawing.Point(321, 65);
            this.editOwnRoutineButton.Name = "editOwnRoutineButton";
            this.editOwnRoutineButton.Size = new System.Drawing.Size(75, 23);
            this.editOwnRoutineButton.TabIndex = 5;
            this.editOwnRoutineButton.Text = "Módosít";
            this.editOwnRoutineButton.UseVisualStyleBackColor = false;
            // 
            // addOwnRoutineButton
            // 
            this.addOwnRoutineButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.addOwnRoutineButton.BackColor = System.Drawing.Color.Coral;
            this.addOwnRoutineButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.addOwnRoutineButton.Location = new System.Drawing.Point(321, 27);
            this.addOwnRoutineButton.Name = "addOwnRoutineButton";
            this.addOwnRoutineButton.Size = new System.Drawing.Size(75, 23);
            this.addOwnRoutineButton.TabIndex = 4;
            this.addOwnRoutineButton.Text = "Új";
            this.addOwnRoutineButton.UseVisualStyleBackColor = false;
            // 
            // ownNameLabel
            // 
            this.ownNameLabel.AutoSize = true;
            this.ownNameLabel.Location = new System.Drawing.Point(3, 11);
            this.ownNameLabel.Name = "ownNameLabel";
            this.ownNameLabel.Size = new System.Drawing.Size(35, 13);
            this.ownNameLabel.TabIndex = 3;
            this.ownNameLabel.Text = "label1";
            // 
            // ownRoutineDataGrid
            // 
            this.ownRoutineDataGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ownRoutineDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ownRoutineDataGrid.Location = new System.Drawing.Point(6, 27);
            this.ownRoutineDataGrid.Name = "ownRoutineDataGrid";
            this.ownRoutineDataGrid.Size = new System.Drawing.Size(277, 228);
            this.ownRoutineDataGrid.TabIndex = 2;
            // 
            // memberRoutineDataGrid
            // 
            this.memberRoutineDataGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.memberRoutineDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.memberRoutineDataGrid.Location = new System.Drawing.Point(3, 27);
            this.memberRoutineDataGrid.Name = "memberRoutineDataGrid";
            this.memberRoutineDataGrid.Size = new System.Drawing.Size(369, 227);
            this.memberRoutineDataGrid.TabIndex = 1;
            // 
            // selectedMemeberNameLabel
            // 
            this.selectedMemeberNameLabel.AutoSize = true;
            this.selectedMemeberNameLabel.Location = new System.Drawing.Point(3, 11);
>>>>>>> 7331d53293423a79ded621d27a66d34463476285
            this.selectedMemeberNameLabel.Name = "selectedMemeberNameLabel";
            this.selectedMemeberNameLabel.Size = new System.Drawing.Size(35, 13);
            this.selectedMemeberNameLabel.TabIndex = 0;
            this.selectedMemeberNameLabel.Text = "label1";
            this.selectedMemeberNameLabel.Visible = false;
            // 
            // backFromRutioneButtom
            // 
            this.backFromRutioneButtom.BackColor = System.Drawing.Color.DarkOrange;
            this.backFromRutioneButtom.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.backFromRutioneButtom.Location = new System.Drawing.Point(3, 3);
            this.backFromRutioneButtom.Name = "backFromRutioneButtom";
            this.backFromRutioneButtom.Size = new System.Drawing.Size(112, 48);
            this.backFromRutioneButtom.TabIndex = 3;
            this.backFromRutioneButtom.Text = "Vissza";
            this.backFromRutioneButtom.UseVisualStyleBackColor = false;
            this.backFromRutioneButtom.Click += new System.EventHandler(this.backFromRutioneButtom_Click);
            // 
            // programsTab
            // 
            this.programsTab.Controls.Add(this.createFamilyProgramPanel);
            this.programsTab.Controls.Add(this.familyProgramsPanel);
            this.programsTab.Controls.Add(this.backFromProgramsButton);
            this.programsTab.Location = new System.Drawing.Point(4, 22);
            this.programsTab.Name = "programsTab";
            this.programsTab.Size = new System.Drawing.Size(891, 330);
            this.programsTab.TabIndex = 3;
            this.programsTab.Text = "Családi programok";
            this.programsTab.UseVisualStyleBackColor = true;
            // 
            // createFamilyProgramPanel
            // 
            this.createFamilyProgramPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.createFamilyProgramPanel.Controls.Add(this.programDatePicker);
            this.createFamilyProgramPanel.Controls.Add(this.createProgramButton);
            this.createFamilyProgramPanel.Controls.Add(this.programDateLabel);
            this.createFamilyProgramPanel.Controls.Add(this.programDescriptionInput);
            this.createFamilyProgramPanel.Controls.Add(this.programDescriptionLabel);
            this.createFamilyProgramPanel.Controls.Add(this.programNameInput);
            this.createFamilyProgramPanel.Controls.Add(this.programNameLabel);
            this.createFamilyProgramPanel.Controls.Add(this.createProgramLabel);
            this.createFamilyProgramPanel.Location = new System.Drawing.Point(126, 3);
            this.createFamilyProgramPanel.Name = "createFamilyProgramPanel";
            this.createFamilyProgramPanel.Size = new System.Drawing.Size(762, 48);
            this.createFamilyProgramPanel.TabIndex = 6;
            // 
            // programDatePicker
            // 
            this.programDatePicker.Location = new System.Drawing.Point(550, 16);
            this.programDatePicker.Name = "programDatePicker";
            this.programDatePicker.Size = new System.Drawing.Size(121, 20);
            this.programDatePicker.TabIndex = 1;
            // 
            // createProgramButton
            // 
            this.createProgramButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.createProgramButton.BackColor = System.Drawing.Color.DarkCyan;
            this.createProgramButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.createProgramButton.Location = new System.Drawing.Point(677, 3);
            this.createProgramButton.Name = "createProgramButton";
            this.createProgramButton.Size = new System.Drawing.Size(81, 42);
            this.createProgramButton.TabIndex = 7;
            this.createProgramButton.Text = "Létrehozás";
            this.createProgramButton.UseVisualStyleBackColor = false;
            this.createProgramButton.Click += new System.EventHandler(this.createProgramButton_Click);
            // 
            // programDateLabel
            // 
            this.programDateLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.programDateLabel.AutoSize = true;
            this.programDateLabel.Location = new System.Drawing.Point(503, 20);
            this.programDateLabel.Name = "programDateLabel";
            this.programDateLabel.Size = new System.Drawing.Size(41, 13);
            this.programDateLabel.TabIndex = 5;
            this.programDateLabel.Text = "Dátum:";
            // 
            // programDescriptionInput
            // 
            this.programDescriptionInput.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.programDescriptionInput.Location = new System.Drawing.Point(377, 16);
            this.programDescriptionInput.Name = "programDescriptionInput";
            this.programDescriptionInput.Size = new System.Drawing.Size(121, 20);
            this.programDescriptionInput.TabIndex = 4;
            // 
            // programDescriptionLabel
            // 
            this.programDescriptionLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.programDescriptionLabel.AutoSize = true;
            this.programDescriptionLabel.Location = new System.Drawing.Point(331, 20);
            this.programDescriptionLabel.Name = "programDescriptionLabel";
            this.programDescriptionLabel.Size = new System.Drawing.Size(40, 13);
            this.programDescriptionLabel.TabIndex = 3;
            this.programDescriptionLabel.Text = "Leírás:";
            // 
            // programNameInput
            // 
            this.programNameInput.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.programNameInput.Location = new System.Drawing.Point(204, 16);
            this.programNameInput.Name = "programNameInput";
            this.programNameInput.Size = new System.Drawing.Size(121, 20);
            this.programNameInput.TabIndex = 2;
            // 
            // programNameLabel
            // 
            this.programNameLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.programNameLabel.AutoSize = true;
            this.programNameLabel.Location = new System.Drawing.Point(168, 19);
            this.programNameLabel.Name = "programNameLabel";
            this.programNameLabel.Size = new System.Drawing.Size(30, 13);
            this.programNameLabel.TabIndex = 1;
            this.programNameLabel.Text = "Név:";
            // 
            // createProgramLabel
            // 
            this.createProgramLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.createProgramLabel.AutoSize = true;
            this.createProgramLabel.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.createProgramLabel.Location = new System.Drawing.Point(3, 13);
            this.createProgramLabel.Name = "createProgramLabel";
            this.createProgramLabel.Size = new System.Drawing.Size(158, 20);
            this.createProgramLabel.TabIndex = 0;
            this.createProgramLabel.Text = "Program létrehozása:";
            // 
            // familyProgramsPanel
            // 
            this.familyProgramsPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.familyProgramsPanel.Controls.Add(this.familyProgramsListView);
            this.familyProgramsPanel.Location = new System.Drawing.Point(5, 76);
            this.familyProgramsPanel.Name = "familyProgramsPanel";
            this.familyProgramsPanel.Size = new System.Drawing.Size(883, 251);
            this.familyProgramsPanel.TabIndex = 5;
            // 
            // familyProgramsListView
            // 
            this.familyProgramsListView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.familyProgramsListView.Depth = 0;
            this.familyProgramsListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.familyProgramsListView.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F);
            this.familyProgramsListView.FullRowSelect = true;
            this.familyProgramsListView.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.familyProgramsListView.Location = new System.Drawing.Point(0, 0);
            this.familyProgramsListView.MouseLocation = new System.Drawing.Point(-1, -1);
            this.familyProgramsListView.MouseState = MaterialSkin.MouseState.OUT;
            this.familyProgramsListView.Name = "familyProgramsListView";
            this.familyProgramsListView.OwnerDraw = true;
            this.familyProgramsListView.Size = new System.Drawing.Size(883, 251);
            this.familyProgramsListView.TabIndex = 0;
            this.familyProgramsListView.UseCompatibleStateImageBehavior = false;
            this.familyProgramsListView.View = System.Windows.Forms.View.Details;
            // 
            // backFromProgramsButton
            // 
            this.backFromProgramsButton.BackColor = System.Drawing.Color.DarkOrange;
            this.backFromProgramsButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.backFromProgramsButton.Location = new System.Drawing.Point(3, 3);
            this.backFromProgramsButton.Name = "backFromProgramsButton";
            this.backFromProgramsButton.Size = new System.Drawing.Size(112, 48);
            this.backFromProgramsButton.TabIndex = 4;
            this.backFromProgramsButton.Text = "Vissza";
            this.backFromProgramsButton.UseVisualStyleBackColor = false;
            this.backFromProgramsButton.Click += new System.EventHandler(this.backFromProgramsButton_Click);
            // 
            // exercisesTab
            // 
            this.exercisesTab.Controls.Add(this.exercisesHolderPanel);
            this.exercisesTab.Controls.Add(this.giveExercisePanel);
            this.exercisesTab.Controls.Add(this.backFromExercisesButton);
            this.exercisesTab.Location = new System.Drawing.Point(4, 22);
            this.exercisesTab.Name = "exercisesTab";
            this.exercisesTab.Size = new System.Drawing.Size(891, 330);
            this.exercisesTab.TabIndex = 4;
            this.exercisesTab.Text = "Feladatok";
            this.exercisesTab.UseVisualStyleBackColor = true;
            // 
            // exercisesHolderPanel
            // 
            this.exercisesHolderPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.exercisesHolderPanel.Controls.Add(this.panel5);
            this.exercisesHolderPanel.Controls.Add(this.panel4);
            this.exercisesHolderPanel.Location = new System.Drawing.Point(3, 57);
            this.exercisesHolderPanel.Name = "exercisesHolderPanel";
            this.exercisesHolderPanel.Size = new System.Drawing.Size(885, 270);
            this.exercisesHolderPanel.TabIndex = 8;
            // 
            // doneExerciseLabel
            // 
            this.doneExerciseLabel.AutoSize = true;
            this.doneExerciseLabel.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.doneExerciseLabel.Location = new System.Drawing.Point(3, 0);
            this.doneExerciseLabel.Name = "doneExerciseLabel";
            this.doneExerciseLabel.Size = new System.Drawing.Size(148, 20);
            this.doneExerciseLabel.TabIndex = 3;
            this.doneExerciseLabel.Text = "Elvégzett feladatok:";
            // 
            // toDoExercisesLabel
            // 
            this.toDoExercisesLabel.AutoSize = true;
            this.toDoExercisesLabel.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.toDoExercisesLabel.Location = new System.Drawing.Point(3, 0);
            this.toDoExercisesLabel.Name = "toDoExercisesLabel";
            this.toDoExercisesLabel.Size = new System.Drawing.Size(163, 20);
            this.toDoExercisesLabel.TabIndex = 2;
            this.toDoExercisesLabel.Text = "Elvégzendő feladatok:";
            // 
            // doneExercisesListView
            // 
            this.doneExercisesListView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.doneExercisesListView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.doneExercisesListView.Depth = 0;
            this.doneExercisesListView.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F);
            this.doneExercisesListView.FullRowSelect = true;
            this.doneExercisesListView.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.doneExercisesListView.Location = new System.Drawing.Point(7, 23);
            this.doneExercisesListView.MouseLocation = new System.Drawing.Point(-1, -1);
            this.doneExercisesListView.MouseState = MaterialSkin.MouseState.OUT;
            this.doneExercisesListView.Name = "doneExercisesListView";
            this.doneExercisesListView.OwnerDraw = true;
            this.doneExercisesListView.Size = new System.Drawing.Size(371, 236);
            this.doneExercisesListView.TabIndex = 1;
            this.doneExercisesListView.UseCompatibleStateImageBehavior = false;
            this.doneExercisesListView.View = System.Windows.Forms.View.Details;
            // 
            // toDoExercisesListView
            // 
            this.toDoExercisesListView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.toDoExercisesListView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.toDoExercisesListView.Depth = 0;
            this.toDoExercisesListView.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F);
            this.toDoExercisesListView.FullRowSelect = true;
            this.toDoExercisesListView.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.toDoExercisesListView.Location = new System.Drawing.Point(3, 23);
            this.toDoExercisesListView.MouseLocation = new System.Drawing.Point(-1, -1);
            this.toDoExercisesListView.MouseState = MaterialSkin.MouseState.OUT;
            this.toDoExercisesListView.Name = "toDoExercisesListView";
            this.toDoExercisesListView.OwnerDraw = true;
            this.toDoExercisesListView.Size = new System.Drawing.Size(412, 236);
            this.toDoExercisesListView.TabIndex = 0;
            this.toDoExercisesListView.UseCompatibleStateImageBehavior = false;
            this.toDoExercisesListView.View = System.Windows.Forms.View.Details;
            // 
            // giveExercisePanel
            // 
            this.giveExercisePanel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.giveExercisePanel.Controls.Add(this.exerciseDescriptionInput);
            this.giveExercisePanel.Controls.Add(this.targetUserComboBox);
            this.giveExercisePanel.Controls.Add(this.giveExerciseButton);
            this.giveExercisePanel.Controls.Add(this.exerciseDescriptionLabel);
            this.giveExercisePanel.Controls.Add(this.exerciseNameInput);
            this.giveExercisePanel.Controls.Add(this.exerciseNameLabel);
            this.giveExercisePanel.Controls.Add(this.targetUserLabel);
            this.giveExercisePanel.Controls.Add(this.giveExerciseLabel);
            this.giveExercisePanel.Location = new System.Drawing.Point(126, 3);
            this.giveExercisePanel.Name = "giveExercisePanel";
            this.giveExercisePanel.Size = new System.Drawing.Size(762, 48);
            this.giveExercisePanel.TabIndex = 7;
            this.giveExercisePanel.Visible = false;
            // 
            // exerciseDescriptionInput
            // 
            this.exerciseDescriptionInput.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.exerciseDescriptionInput.Location = new System.Drawing.Point(549, 17);
            this.exerciseDescriptionInput.Name = "exerciseDescriptionInput";
            this.exerciseDescriptionInput.Size = new System.Drawing.Size(122, 20);
            this.exerciseDescriptionInput.TabIndex = 9;
            // 
            // targetUserComboBox
            // 
            this.targetUserComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.targetUserComboBox.FormattingEnabled = true;
            this.targetUserComboBox.Location = new System.Drawing.Point(205, 16);
            this.targetUserComboBox.Name = "targetUserComboBox";
            this.targetUserComboBox.Size = new System.Drawing.Size(120, 21);
            this.targetUserComboBox.TabIndex = 8;
            // 
            // giveExerciseButton
            // 
            this.giveExerciseButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.giveExerciseButton.BackColor = System.Drawing.Color.DarkCyan;
            this.giveExerciseButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.giveExerciseButton.Location = new System.Drawing.Point(677, 3);
            this.giveExerciseButton.Name = "giveExerciseButton";
            this.giveExerciseButton.Size = new System.Drawing.Size(81, 42);
            this.giveExerciseButton.TabIndex = 7;
            this.giveExerciseButton.Text = "Kiosztás";
            this.giveExerciseButton.UseVisualStyleBackColor = false;
            // 
            // exerciseDescriptionLabel
            // 
            this.exerciseDescriptionLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.exerciseDescriptionLabel.AutoSize = true;
            this.exerciseDescriptionLabel.Location = new System.Drawing.Point(503, 20);
            this.exerciseDescriptionLabel.Name = "exerciseDescriptionLabel";
            this.exerciseDescriptionLabel.Size = new System.Drawing.Size(40, 13);
            this.exerciseDescriptionLabel.TabIndex = 5;
            this.exerciseDescriptionLabel.Text = "Leírás:";
            // 
            // exerciseNameInput
            // 
            this.exerciseNameInput.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.exerciseNameInput.Location = new System.Drawing.Point(377, 16);
            this.exerciseNameInput.Name = "exerciseNameInput";
            this.exerciseNameInput.Size = new System.Drawing.Size(121, 20);
            this.exerciseNameInput.TabIndex = 4;
            // 
            // exerciseNameLabel
            // 
            this.exerciseNameLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.exerciseNameLabel.AutoSize = true;
            this.exerciseNameLabel.Location = new System.Drawing.Point(331, 20);
            this.exerciseNameLabel.Name = "exerciseNameLabel";
            this.exerciseNameLabel.Size = new System.Drawing.Size(30, 13);
            this.exerciseNameLabel.TabIndex = 3;
            this.exerciseNameLabel.Text = "Név:";
            // 
            // targetUserLabel
            // 
            this.targetUserLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.targetUserLabel.AutoSize = true;
            this.targetUserLabel.Location = new System.Drawing.Point(136, 20);
            this.targetUserLabel.Name = "targetUserLabel";
            this.targetUserLabel.Size = new System.Drawing.Size(62, 13);
            this.targetUserLabel.TabIndex = 1;
            this.targetUserLabel.Text = "Célszemély:";
            // 
            // giveExerciseLabel
            // 
            this.giveExerciseLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.giveExerciseLabel.AutoSize = true;
            this.giveExerciseLabel.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.giveExerciseLabel.Location = new System.Drawing.Point(3, 13);
            this.giveExerciseLabel.Name = "giveExerciseLabel";
            this.giveExerciseLabel.Size = new System.Drawing.Size(132, 20);
            this.giveExerciseLabel.TabIndex = 0;
            this.giveExerciseLabel.Text = "Feladat kiosztása:";
            // 
            // backFromExercisesButton
            // 
            this.backFromExercisesButton.BackColor = System.Drawing.Color.DarkOrange;
            this.backFromExercisesButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.backFromExercisesButton.Location = new System.Drawing.Point(3, 3);
            this.backFromExercisesButton.Name = "backFromExercisesButton";
            this.backFromExercisesButton.Size = new System.Drawing.Size(112, 48);
            this.backFromExercisesButton.TabIndex = 5;
            this.backFromExercisesButton.Text = "Vissza";
            this.backFromExercisesButton.UseVisualStyleBackColor = false;
            this.backFromExercisesButton.Click += new System.EventHandler(this.backFromExercisesButton_Click);
            // 
            // galeryTab
            // 
            this.galeryTab.Controls.Add(this.backFromGaleryButton);
            this.galeryTab.Location = new System.Drawing.Point(4, 22);
            this.galeryTab.Name = "galeryTab";
            this.galeryTab.Size = new System.Drawing.Size(891, 330);
            this.galeryTab.TabIndex = 5;
            this.galeryTab.Text = "Galéria";
            this.galeryTab.UseVisualStyleBackColor = true;
            // 
            // backFromGaleryButton
            // 
            this.backFromGaleryButton.BackColor = System.Drawing.Color.DarkOrange;
            this.backFromGaleryButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.backFromGaleryButton.Location = new System.Drawing.Point(3, 3);
            this.backFromGaleryButton.Name = "backFromGaleryButton";
            this.backFromGaleryButton.Size = new System.Drawing.Size(112, 48);
            this.backFromGaleryButton.TabIndex = 6;
            this.backFromGaleryButton.Text = "Vissza";
            this.backFromGaleryButton.UseVisualStyleBackColor = false;
            this.backFromGaleryButton.Click += new System.EventHandler(this.backFromGaleryButton_Click);
            // 
            // chatroomTab
            // 
            this.chatroomTab.Controls.Add(this.backFromChatButton);
            this.chatroomTab.Location = new System.Drawing.Point(4, 22);
            this.chatroomTab.Name = "chatroomTab";
            this.chatroomTab.Size = new System.Drawing.Size(891, 330);
            this.chatroomTab.TabIndex = 6;
            this.chatroomTab.Text = "Társalgó";
            this.chatroomTab.UseVisualStyleBackColor = true;
            // 
            // backFromChatButton
            // 
            this.backFromChatButton.BackColor = System.Drawing.Color.DarkOrange;
            this.backFromChatButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.backFromChatButton.Location = new System.Drawing.Point(3, 3);
            this.backFromChatButton.Name = "backFromChatButton";
            this.backFromChatButton.Size = new System.Drawing.Size(112, 48);
            this.backFromChatButton.TabIndex = 7;
            this.backFromChatButton.Text = "Vissza";
            this.backFromChatButton.UseVisualStyleBackColor = false;
            this.backFromChatButton.Click += new System.EventHandler(this.backFromChatButton_Click);
            // 
<<<<<<< HEAD
=======
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.Controls.Add(this.selectedMemeberNameLabel);
            this.panel2.Controls.Add(this.memberRoutineDataGrid);
            this.panel2.Location = new System.Drawing.Point(3, 4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(375, 257);
            this.panel2.TabIndex = 7;
            // 
            // panel3
            // 
            this.panel3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel3.Controls.Add(this.ownNameLabel);
            this.panel3.Controls.Add(this.ownRoutineDataGrid);
            this.panel3.Controls.Add(this.deleteOwnRoutineButton);
            this.panel3.Controls.Add(this.addOwnRoutineButton);
            this.panel3.Controls.Add(this.editOwnRoutineButton);
            this.panel3.Location = new System.Drawing.Point(447, 4);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(429, 257);
            this.panel3.TabIndex = 7;
            // 
            // panel4
            // 
            this.panel4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel4.Controls.Add(this.toDoExercisesLabel);
            this.panel4.Controls.Add(this.toDoExercisesListView);
            this.panel4.Location = new System.Drawing.Point(4, 3);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(418, 262);
            this.panel4.TabIndex = 4;
            // 
            // panel5
            // 
            this.panel5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel5.Controls.Add(this.doneExerciseLabel);
            this.panel5.Controls.Add(this.doneExercisesListView);
            this.panel5.Location = new System.Drawing.Point(500, 3);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(381, 262);
            this.panel5.TabIndex = 5;
            // 
>>>>>>> 7331d53293423a79ded621d27a66d34463476285
            // UserLoggedinForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(901, 500);
            this.Controls.Add(this.materialTabControl1);
            this.Controls.Add(this.userDatasPanel);
            this.Controls.Add(this.loggedInUserUsernameLabel);
            this.Name = "UserLoggedinForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Family Manager";
            this.Load += new System.EventHandler(this.UserLoggedinForm_Load);
            this.userDatasPanel.ResumeLayout(false);
            this.userDatasPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.userProfilePicture)).EndInit();
            this.materialTabControl1.ResumeLayout(false);
            this.homeTab.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.familyTab.ResumeLayout(false);
            this.familyTab.PerformLayout();
            this.connectToFamilyPanel.ResumeLayout(false);
            this.connectToFamilyPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.createFamilyPanel.ResumeLayout(false);
            this.createFamilyPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.createFamilyPicture)).EndInit();
            this.routineTab.ResumeLayout(false);
            this.routineTab.PerformLayout();
            this.routinePanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ownRoutineDataGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memberRoutineDataGrid)).EndInit();
            this.programsTab.ResumeLayout(false);
            this.createFamilyProgramPanel.ResumeLayout(false);
            this.createFamilyProgramPanel.PerformLayout();
            this.familyProgramsPanel.ResumeLayout(false);
            this.exercisesTab.ResumeLayout(false);
            this.exercisesHolderPanel.ResumeLayout(false);
            this.giveExercisePanel.ResumeLayout(false);
            this.giveExercisePanel.PerformLayout();
            this.galeryTab.ResumeLayout(false);
            this.chatroomTab.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox userProfilePicture;
        private System.Windows.Forms.Label loggedInUserUsernameLabel;
        private System.Windows.Forms.Label loggedInUserFirstnameLabel;
        private System.Windows.Forms.Label loggedinUserLastnameLabel;
        private System.Windows.Forms.Label loggedinUserEmailLabel;
        private System.Windows.Forms.Panel userDatasPanel;
        private System.Windows.Forms.Label loggedInUsernameLabel;
        private System.Windows.Forms.Button selectLogoutButton;
        private MaterialSkin.Controls.MaterialTabControl materialTabControl1;
        private System.Windows.Forms.TabPage homeTab;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button selectFamilysButton;
        private System.Windows.Forms.Button selectExercisesButton;
        private System.Windows.Forms.Button selectChatroomButton;
        private System.Windows.Forms.Button selectProgramsButton;
        private System.Windows.Forms.Button selectRoutineButton;
        private System.Windows.Forms.Button selectGalleryButton;
        private System.Windows.Forms.TabPage familyTab;
        private System.Windows.Forms.TabPage routineTab;
        private System.Windows.Forms.TabPage programsTab;
        private System.Windows.Forms.TabPage exercisesTab;
        private System.Windows.Forms.TabPage galeryTab;
        private System.Windows.Forms.TabPage chatroomTab;
        private System.Windows.Forms.Button exitFromFamilyButton;
        private System.Windows.Forms.Button backFromFamilysButton;
        private System.Windows.Forms.Label familyNameLabel;
        private MaterialSkin.Controls.MaterialListView familysListView;
        private System.Windows.Forms.Button backFromRutioneButtom;
        private System.Windows.Forms.Button backFromProgramsButton;
        private System.Windows.Forms.Button backFromExercisesButton;
        private System.Windows.Forms.Button backFromGaleryButton;
        private System.Windows.Forms.Button backFromChatButton;
        private System.Windows.Forms.Panel createFamilyPanel;
        private System.Windows.Forms.Button createFamilyButton;
        private System.Windows.Forms.Label createFamilyLabel;
        private System.Windows.Forms.PictureBox createFamilyPicture;
        private System.Windows.Forms.Panel connectToFamilyPanel;
        private System.Windows.Forms.Button connectToFamilyButton;
        private System.Windows.Forms.Label connectFamilyLabel;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label familyMembersComboBoxLabel;
        private System.Windows.Forms.ComboBox familyMembersComboBox;
        private System.Windows.Forms.Panel routinePanel;
        private System.Windows.Forms.Panel familyProgramsPanel;
        private MaterialSkin.Controls.MaterialListView familyProgramsListView;
        private System.Windows.Forms.Panel createFamilyProgramPanel;
        private System.Windows.Forms.Label createProgramLabel;
        private System.Windows.Forms.TextBox programNameInput;
        private System.Windows.Forms.Label programNameLabel;
        private System.Windows.Forms.TextBox programDescriptionInput;
        private System.Windows.Forms.Label programDescriptionLabel;
        private System.Windows.Forms.Label programDateLabel;
        private System.Windows.Forms.Button createProgramButton;
        private System.Windows.Forms.DateTimePicker programDatePicker;
        private System.Windows.Forms.Panel giveExercisePanel;
        private System.Windows.Forms.TextBox exerciseDescriptionInput;
        private System.Windows.Forms.ComboBox targetUserComboBox;
        private System.Windows.Forms.Button giveExerciseButton;
        private System.Windows.Forms.Label exerciseDescriptionLabel;
        private System.Windows.Forms.TextBox exerciseNameInput;
        private System.Windows.Forms.Label exerciseNameLabel;
        private System.Windows.Forms.Label targetUserLabel;
        private System.Windows.Forms.Label giveExerciseLabel;
        private System.Windows.Forms.Panel exercisesHolderPanel;
        private System.Windows.Forms.Label toDoExercisesLabel;
        private MaterialSkin.Controls.MaterialListView doneExercisesListView;
        private MaterialSkin.Controls.MaterialListView toDoExercisesListView;
        private System.Windows.Forms.Label doneExerciseLabel;
        private System.Windows.Forms.Label selectedMemeberNameLabel;
        private System.Windows.Forms.Label ownNameLabel;
        private System.Windows.Forms.DataGridView ownRoutineDataGrid;
        private System.Windows.Forms.DataGridView memberRoutineDataGrid;
        private System.Windows.Forms.Button deleteOwnRoutineButton;
        private System.Windows.Forms.Button editOwnRoutineButton;
        private System.Windows.Forms.Button addOwnRoutineButton;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel5;
    }
}