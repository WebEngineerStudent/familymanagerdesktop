﻿namespace FamilyManager
{
    partial class ConnectFamilyForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.searchFamilyTab = new System.Windows.Forms.TabPage();
            this.inputPanel = new System.Windows.Forms.Panel();
            this.cancelSearchFamilyButton = new System.Windows.Forms.Button();
            this.createTheFamilyButton = new System.Windows.Forms.Button();
            this.familyNameLabel = new System.Windows.Forms.Label();
            this.searchFamilyNameInput = new System.Windows.Forms.TextBox();
            this.searchFamilyIcon = new System.Windows.Forms.PictureBox();
            this.findedFamilysTab = new System.Windows.Forms.TabPage();
            this.cancelSearchingButton = new System.Windows.Forms.Button();
            this.connectToSelectedFamilyButton = new System.Windows.Forms.Button();
            this.backToSearchButton = new System.Windows.Forms.Button();
            this.findedFamilysLabel = new System.Windows.Forms.Label();
            this.findedFamilysListView = new System.Windows.Forms.ListView();
            this.errorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this.tabControl.SuspendLayout();
            this.searchFamilyTab.SuspendLayout();
            this.inputPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.searchFamilyIcon)).BeginInit();
            this.findedFamilysTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl
            // 
            this.tabControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl.Controls.Add(this.searchFamilyTab);
            this.tabControl.Controls.Add(this.findedFamilysTab);
            this.tabControl.Location = new System.Drawing.Point(1, 62);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(798, 391);
            this.tabControl.TabIndex = 0;
            // 
            // searchFamilyTab
            // 
            this.searchFamilyTab.Controls.Add(this.inputPanel);
            this.searchFamilyTab.Controls.Add(this.searchFamilyIcon);
            this.searchFamilyTab.Location = new System.Drawing.Point(4, 22);
            this.searchFamilyTab.Name = "searchFamilyTab";
            this.searchFamilyTab.Padding = new System.Windows.Forms.Padding(3);
            this.searchFamilyTab.Size = new System.Drawing.Size(790, 365);
            this.searchFamilyTab.TabIndex = 0;
            this.searchFamilyTab.Text = "searchFamily";
            this.searchFamilyTab.UseVisualStyleBackColor = true;
            // 
            // inputPanel
            // 
            this.inputPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.inputPanel.BackColor = System.Drawing.SystemColors.ControlDark;
            this.inputPanel.Controls.Add(this.cancelSearchFamilyButton);
            this.inputPanel.Controls.Add(this.createTheFamilyButton);
            this.inputPanel.Controls.Add(this.familyNameLabel);
            this.inputPanel.Controls.Add(this.searchFamilyNameInput);
            this.inputPanel.Location = new System.Drawing.Point(221, 211);
            this.inputPanel.Name = "inputPanel";
            this.inputPanel.Size = new System.Drawing.Size(349, 152);
            this.inputPanel.TabIndex = 3;
            // 
            // cancelSearchFamilyButton
            // 
            this.cancelSearchFamilyButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cancelSearchFamilyButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelSearchFamilyButton.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.cancelSearchFamilyButton.Location = new System.Drawing.Point(194, 112);
            this.cancelSearchFamilyButton.Name = "cancelSearchFamilyButton";
            this.cancelSearchFamilyButton.Size = new System.Drawing.Size(152, 37);
            this.cancelSearchFamilyButton.TabIndex = 3;
            this.cancelSearchFamilyButton.Text = "Mégse";
            this.cancelSearchFamilyButton.UseVisualStyleBackColor = true;
            // 
            // createTheFamilyButton
            // 
            this.createTheFamilyButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.createTheFamilyButton.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.createTheFamilyButton.Location = new System.Drawing.Point(4, 112);
            this.createTheFamilyButton.Name = "createTheFamilyButton";
            this.createTheFamilyButton.Size = new System.Drawing.Size(152, 37);
            this.createTheFamilyButton.TabIndex = 2;
            this.createTheFamilyButton.Text = "Keresés";
            this.createTheFamilyButton.UseVisualStyleBackColor = true;
            this.createTheFamilyButton.Click += new System.EventHandler(this.createTheFamilyButton_Click);
            // 
            // familyNameLabel
            // 
            this.familyNameLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.familyNameLabel.AutoSize = true;
            this.familyNameLabel.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.familyNameLabel.Location = new System.Drawing.Point(107, 15);
            this.familyNameLabel.Name = "familyNameLabel";
            this.familyNameLabel.Size = new System.Drawing.Size(121, 25);
            this.familyNameLabel.TabIndex = 1;
            this.familyNameLabel.Text = "Család neve:";
            // 
            // searchFamilyNameInput
            // 
            this.searchFamilyNameInput.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.searchFamilyNameInput.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.searchFamilyNameInput.Location = new System.Drawing.Point(89, 57);
            this.searchFamilyNameInput.Name = "searchFamilyNameInput";
            this.searchFamilyNameInput.Size = new System.Drawing.Size(171, 29);
            this.searchFamilyNameInput.TabIndex = 0;
            // 
            // searchFamilyIcon
            // 
            this.searchFamilyIcon.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.searchFamilyIcon.BackgroundImage = global::FamilyManager.Properties.Resources.searchFamilyIcon;
            this.searchFamilyIcon.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.searchFamilyIcon.Location = new System.Drawing.Point(297, 1);
            this.searchFamilyIcon.Name = "searchFamilyIcon";
            this.searchFamilyIcon.Size = new System.Drawing.Size(184, 157);
            this.searchFamilyIcon.TabIndex = 2;
            this.searchFamilyIcon.TabStop = false;
            // 
            // findedFamilysTab
            // 
            this.findedFamilysTab.Controls.Add(this.cancelSearchingButton);
            this.findedFamilysTab.Controls.Add(this.connectToSelectedFamilyButton);
            this.findedFamilysTab.Controls.Add(this.backToSearchButton);
            this.findedFamilysTab.Controls.Add(this.findedFamilysLabel);
            this.findedFamilysTab.Controls.Add(this.findedFamilysListView);
            this.findedFamilysTab.Location = new System.Drawing.Point(4, 22);
            this.findedFamilysTab.Name = "findedFamilysTab";
            this.findedFamilysTab.Padding = new System.Windows.Forms.Padding(3);
            this.findedFamilysTab.Size = new System.Drawing.Size(790, 365);
            this.findedFamilysTab.TabIndex = 1;
            this.findedFamilysTab.Text = "findedFamilys";
            this.findedFamilysTab.UseVisualStyleBackColor = true;
            // 
            // cancelSearchingButton
            // 
            this.cancelSearchingButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cancelSearchingButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelSearchingButton.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.cancelSearchingButton.Location = new System.Drawing.Point(638, 327);
            this.cancelSearchingButton.Name = "cancelSearchingButton";
            this.cancelSearchingButton.Size = new System.Drawing.Size(145, 35);
            this.cancelSearchingButton.TabIndex = 4;
            this.cancelSearchingButton.Text = "Mégse";
            this.cancelSearchingButton.UseVisualStyleBackColor = true;
            // 
            // connectToSelectedFamilyButton
            // 
            this.connectToSelectedFamilyButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.connectToSelectedFamilyButton.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.connectToSelectedFamilyButton.Location = new System.Drawing.Point(580, 47);
            this.connectToSelectedFamilyButton.Name = "connectToSelectedFamilyButton";
            this.connectToSelectedFamilyButton.Size = new System.Drawing.Size(203, 31);
            this.connectToSelectedFamilyButton.TabIndex = 3;
            this.connectToSelectedFamilyButton.Text = "Csatlakozás a kijelölt családhoz";
            this.connectToSelectedFamilyButton.UseVisualStyleBackColor = true;
            this.connectToSelectedFamilyButton.Click += new System.EventHandler(this.connectToSelectedFamilyButton_Click);
            // 
            // backToSearchButton
            // 
            this.backToSearchButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.backToSearchButton.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.backToSearchButton.Location = new System.Drawing.Point(638, 112);
            this.backToSearchButton.Name = "backToSearchButton";
            this.backToSearchButton.Size = new System.Drawing.Size(145, 35);
            this.backToSearchButton.TabIndex = 2;
            this.backToSearchButton.Text = "Vissza a kereséshez";
            this.backToSearchButton.UseVisualStyleBackColor = true;
            this.backToSearchButton.Click += new System.EventHandler(this.backToSearchButton_Click);
            // 
            // findedFamilysLabel
            // 
            this.findedFamilysLabel.AutoSize = true;
            this.findedFamilysLabel.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.findedFamilysLabel.Location = new System.Drawing.Point(3, 12);
            this.findedFamilysLabel.Name = "findedFamilysLabel";
            this.findedFamilysLabel.Size = new System.Drawing.Size(269, 21);
            this.findedFamilysLabel.TabIndex = 1;
            this.findedFamilysLabel.Text = "Találatok a keresett család névvel:";
            // 
            // findedFamilysListView
            // 
            this.findedFamilysListView.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.findedFamilysListView.Location = new System.Drawing.Point(0, 47);
            this.findedFamilysListView.Name = "findedFamilysListView";
            this.findedFamilysListView.Size = new System.Drawing.Size(534, 318);
            this.findedFamilysListView.TabIndex = 0;
            this.findedFamilysListView.UseCompatibleStateImageBehavior = false;
            // 
            // errorProvider
            // 
            this.errorProvider.ContainerControl = this;
            // 
            // ConnectFamilyForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.tabControl);
            this.Name = "ConnectFamilyForm";
            this.Text = "ConnectFamilyForm";
            this.tabControl.ResumeLayout(false);
            this.searchFamilyTab.ResumeLayout(false);
            this.inputPanel.ResumeLayout(false);
            this.inputPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.searchFamilyIcon)).EndInit();
            this.findedFamilysTab.ResumeLayout(false);
            this.findedFamilysTab.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage searchFamilyTab;
        private System.Windows.Forms.Panel inputPanel;
        private System.Windows.Forms.Button cancelSearchFamilyButton;
        private System.Windows.Forms.Button createTheFamilyButton;
        private System.Windows.Forms.Label familyNameLabel;
        private System.Windows.Forms.TextBox searchFamilyNameInput;
        private System.Windows.Forms.PictureBox searchFamilyIcon;
        private System.Windows.Forms.TabPage findedFamilysTab;
        private System.Windows.Forms.Button cancelSearchingButton;
        private System.Windows.Forms.Button connectToSelectedFamilyButton;
        private System.Windows.Forms.Button backToSearchButton;
        private System.Windows.Forms.Label findedFamilysLabel;
        private System.Windows.Forms.ListView findedFamilysListView;
        private System.Windows.Forms.ErrorProvider errorProvider;
    }
}