﻿namespace FamilyManager
{
    partial class AdminLoggedinForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.admonTabControl = new MaterialSkin.Controls.MaterialTabControl();
            this.usersTab = new System.Windows.Forms.TabPage();
            this.usersButtonsPanel = new System.Windows.Forms.Panel();
            this.cancelUsersButton = new MaterialSkin.Controls.MaterialRaisedButton();
            this.saveUsersButton = new MaterialSkin.Controls.MaterialRaisedButton();
            this.reloadFamily = new MaterialSkin.Controls.MaterialRaisedButton();
            this.deleteUserButton = new MaterialSkin.Controls.MaterialRaisedButton();
            this.userSearchInput = new System.Windows.Forms.TextBox();
            this.editUserButton = new MaterialSkin.Controls.MaterialRaisedButton();
            this.shearchUserButton = new MaterialSkin.Controls.MaterialRaisedButton();
            this.newUserButton = new MaterialSkin.Controls.MaterialRaisedButton();
            this.usersDataGridView = new System.Windows.Forms.DataGridView();
            this.familysTab = new System.Windows.Forms.TabPage();
            this.panel2 = new System.Windows.Forms.Panel();
            this.cancelFamilyButton = new MaterialSkin.Controls.MaterialRaisedButton();
            this.saveFamilyButton = new MaterialSkin.Controls.MaterialRaisedButton();
            this.reloadFamilyButton = new MaterialSkin.Controls.MaterialRaisedButton();
            this.deleteFamilyButton = new MaterialSkin.Controls.MaterialRaisedButton();
            this.searchFamilyInput = new System.Windows.Forms.TextBox();
            this.editFamilyButton = new MaterialSkin.Controls.MaterialRaisedButton();
            this.searchFamilyButton = new MaterialSkin.Controls.MaterialRaisedButton();
            this.addNewFamilyButton = new MaterialSkin.Controls.MaterialRaisedButton();
            this.familysDataGridView = new System.Windows.Forms.DataGridView();
            this.programsTab = new System.Windows.Forms.TabPage();
            this.panel3 = new System.Windows.Forms.Panel();
            this.cancelProgramButton = new MaterialSkin.Controls.MaterialRaisedButton();
            this.saveProgramButton = new MaterialSkin.Controls.MaterialRaisedButton();
            this.reloadProgramsButton = new MaterialSkin.Controls.MaterialRaisedButton();
            this.deleteProgramButton = new MaterialSkin.Controls.MaterialRaisedButton();
            this.searchProgramInput = new System.Windows.Forms.TextBox();
            this.editProgramButton = new MaterialSkin.Controls.MaterialRaisedButton();
            this.searchProgramButton = new MaterialSkin.Controls.MaterialRaisedButton();
            this.newProgramButton = new MaterialSkin.Controls.MaterialRaisedButton();
            this.programsDataGridView = new System.Windows.Forms.DataGridView();
            this.exercisesTab = new System.Windows.Forms.TabPage();
            this.panel4 = new System.Windows.Forms.Panel();
            this.cancelExerciseButton = new MaterialSkin.Controls.MaterialRaisedButton();
            this.saveExerciseButton = new MaterialSkin.Controls.MaterialRaisedButton();
            this.reloadExerciseButton = new MaterialSkin.Controls.MaterialRaisedButton();
            this.deleteExerciseButton = new MaterialSkin.Controls.MaterialRaisedButton();
            this.searchExerciseInput = new System.Windows.Forms.TextBox();
            this.editExerciseButton = new MaterialSkin.Controls.MaterialRaisedButton();
            this.searchExerciseButton = new MaterialSkin.Controls.MaterialRaisedButton();
            this.newExerciseButton = new MaterialSkin.Controls.MaterialRaisedButton();
            this.exercisesDataGridView = new System.Windows.Forms.DataGridView();
            this.routinsTab = new System.Windows.Forms.TabPage();
            this.panel5 = new System.Windows.Forms.Panel();
            this.cancelRoutineButton = new MaterialSkin.Controls.MaterialRaisedButton();
            this.saveRoutineButton = new MaterialSkin.Controls.MaterialRaisedButton();
            this.reloadRoutineButton = new MaterialSkin.Controls.MaterialRaisedButton();
            this.deleteRoutineButton = new MaterialSkin.Controls.MaterialRaisedButton();
            this.searchRoutineInput = new System.Windows.Forms.TextBox();
            this.editRoutineButton = new MaterialSkin.Controls.MaterialRaisedButton();
            this.searchRoutineButton = new MaterialSkin.Controls.MaterialRaisedButton();
            this.newRoutineButton = new MaterialSkin.Controls.MaterialRaisedButton();
            this.routinesDataGridView = new System.Windows.Forms.DataGridView();
            this.staticsTab = new System.Windows.Forms.TabPage();
            this.statisticChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.panel1 = new System.Windows.Forms.Panel();
            this.adminLogoutUsersButton = new MaterialSkin.Controls.MaterialRaisedButton();
            this.adminEmailLabel = new System.Windows.Forms.Label();
            this.adminUsernameLabel = new System.Windows.Forms.Label();
            this.adminProfPict = new System.Windows.Forms.PictureBox();
            this.menuTabSelector = new MaterialSkin.Controls.MaterialTabSelector();
            this.tableLayoutPanel1.SuspendLayout();
            this.admonTabControl.SuspendLayout();
            this.usersTab.SuspendLayout();
            this.usersButtonsPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.usersDataGridView)).BeginInit();
            this.familysTab.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.familysDataGridView)).BeginInit();
            this.programsTab.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.programsDataGridView)).BeginInit();
            this.exercisesTab.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.exercisesDataGridView)).BeginInit();
            this.routinsTab.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.routinesDataGridView)).BeginInit();
            this.staticsTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.statisticChart)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.adminProfPict)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.admonTabControl, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.menuTabSelector, 0, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 60);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.92876F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 87.07124F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 70F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(927, 455);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // admonTabControl
            // 
            this.admonTabControl.Controls.Add(this.usersTab);
            this.admonTabControl.Controls.Add(this.familysTab);
            this.admonTabControl.Controls.Add(this.programsTab);
            this.admonTabControl.Controls.Add(this.exercisesTab);
            this.admonTabControl.Controls.Add(this.routinsTab);
            this.admonTabControl.Controls.Add(this.staticsTab);
            this.admonTabControl.Depth = 0;
            this.admonTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.admonTabControl.Location = new System.Drawing.Point(3, 52);
            this.admonTabControl.MouseState = MaterialSkin.MouseState.HOVER;
            this.admonTabControl.Name = "admonTabControl";
            this.admonTabControl.SelectedIndex = 0;
            this.admonTabControl.Size = new System.Drawing.Size(921, 329);
            this.admonTabControl.TabIndex = 1;
            // 
            // usersTab
            // 
            this.usersTab.Controls.Add(this.usersButtonsPanel);
            this.usersTab.Controls.Add(this.usersDataGridView);
            this.usersTab.Location = new System.Drawing.Point(4, 22);
            this.usersTab.Name = "usersTab";
            this.usersTab.Padding = new System.Windows.Forms.Padding(3);
            this.usersTab.Size = new System.Drawing.Size(913, 303);
            this.usersTab.TabIndex = 0;
            this.usersTab.Text = "Felhasználók";
            this.usersTab.UseVisualStyleBackColor = true;
            // 
            // usersButtonsPanel
            // 
            this.usersButtonsPanel.Controls.Add(this.cancelUsersButton);
            this.usersButtonsPanel.Controls.Add(this.saveUsersButton);
            this.usersButtonsPanel.Controls.Add(this.reloadFamily);
            this.usersButtonsPanel.Controls.Add(this.deleteUserButton);
            this.usersButtonsPanel.Controls.Add(this.userSearchInput);
            this.usersButtonsPanel.Controls.Add(this.editUserButton);
            this.usersButtonsPanel.Controls.Add(this.shearchUserButton);
            this.usersButtonsPanel.Controls.Add(this.newUserButton);
            this.usersButtonsPanel.Dock = System.Windows.Forms.DockStyle.Right;
            this.usersButtonsPanel.Location = new System.Drawing.Point(716, 3);
            this.usersButtonsPanel.Name = "usersButtonsPanel";
            this.usersButtonsPanel.Size = new System.Drawing.Size(194, 297);
            this.usersButtonsPanel.TabIndex = 2;
            // 
            // cancelUsersButton
            // 
            this.cancelUsersButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cancelUsersButton.AutoSize = true;
            this.cancelUsersButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.cancelUsersButton.Depth = 0;
            this.cancelUsersButton.Icon = null;
            this.cancelUsersButton.Location = new System.Drawing.Point(3, 228);
            this.cancelUsersButton.MouseState = MaterialSkin.MouseState.HOVER;
            this.cancelUsersButton.Name = "cancelUsersButton";
            this.cancelUsersButton.Primary = true;
            this.cancelUsersButton.Size = new System.Drawing.Size(66, 36);
            this.cancelUsersButton.TabIndex = 7;
            this.cancelUsersButton.Text = "Mégse";
            this.cancelUsersButton.UseVisualStyleBackColor = true;
            this.cancelUsersButton.Visible = false;
            this.cancelUsersButton.Click += new System.EventHandler(this.cancelUsersButton_Click);
            // 
            // saveUsersButton
            // 
            this.saveUsersButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.saveUsersButton.AutoSize = true;
            this.saveUsersButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.saveUsersButton.Depth = 0;
            this.saveUsersButton.Icon = null;
            this.saveUsersButton.Location = new System.Drawing.Point(3, 192);
            this.saveUsersButton.MouseState = MaterialSkin.MouseState.HOVER;
            this.saveUsersButton.Name = "saveUsersButton";
            this.saveUsersButton.Primary = true;
            this.saveUsersButton.Size = new System.Drawing.Size(75, 36);
            this.saveUsersButton.TabIndex = 6;
            this.saveUsersButton.Text = "Mentés";
            this.saveUsersButton.UseVisualStyleBackColor = true;
            this.saveUsersButton.Visible = false;
            this.saveUsersButton.Click += new System.EventHandler(this.saveUsersButton_Click);
            // 
            // reloadFamily
            // 
            this.reloadFamily.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.reloadFamily.AutoSize = true;
            this.reloadFamily.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.reloadFamily.Depth = 0;
            this.reloadFamily.Icon = null;
            this.reloadFamily.Location = new System.Drawing.Point(3, 264);
            this.reloadFamily.MouseState = MaterialSkin.MouseState.HOVER;
            this.reloadFamily.Name = "reloadFamily";
            this.reloadFamily.Primary = true;
            this.reloadFamily.Size = new System.Drawing.Size(86, 36);
            this.reloadFamily.TabIndex = 5;
            this.reloadFamily.Text = "Frissítés";
            this.reloadFamily.UseVisualStyleBackColor = true;
            this.reloadFamily.Click += new System.EventHandler(this.reloadUsersButton_Click);
            // 
            // deleteUserButton
            // 
            this.deleteUserButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.deleteUserButton.AutoSize = true;
            this.deleteUserButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.deleteUserButton.Depth = 0;
            this.deleteUserButton.Icon = null;
            this.deleteUserButton.Location = new System.Drawing.Point(3, 151);
            this.deleteUserButton.MouseState = MaterialSkin.MouseState.HOVER;
            this.deleteUserButton.Name = "deleteUserButton";
            this.deleteUserButton.Primary = true;
            this.deleteUserButton.Size = new System.Drawing.Size(71, 36);
            this.deleteUserButton.TabIndex = 4;
            this.deleteUserButton.Text = "Törlés";
            this.deleteUserButton.UseVisualStyleBackColor = true;
            this.deleteUserButton.Click += new System.EventHandler(this.deleteUserButton_Click);
            // 
            // userSearchInput
            // 
            this.userSearchInput.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.userSearchInput.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.userSearchInput.Location = new System.Drawing.Point(3, 0);
            this.userSearchInput.Name = "userSearchInput";
            this.userSearchInput.Size = new System.Drawing.Size(188, 29);
            this.userSearchInput.TabIndex = 1;
            // 
            // editUserButton
            // 
            this.editUserButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.editUserButton.AutoSize = true;
            this.editUserButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.editUserButton.Depth = 0;
            this.editUserButton.Icon = null;
            this.editUserButton.Location = new System.Drawing.Point(3, 115);
            this.editUserButton.MouseState = MaterialSkin.MouseState.HOVER;
            this.editUserButton.Name = "editUserButton";
            this.editUserButton.Primary = true;
            this.editUserButton.Size = new System.Drawing.Size(111, 36);
            this.editUserButton.TabIndex = 3;
            this.editUserButton.Text = "Szerkesztés";
            this.editUserButton.UseVisualStyleBackColor = true;
            this.editUserButton.Click += new System.EventHandler(this.editUserButton_Click);
            // 
            // shearchUserButton
            // 
            this.shearchUserButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.shearchUserButton.AutoSize = true;
            this.shearchUserButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.shearchUserButton.BackColor = System.Drawing.Color.DarkCyan;
            this.shearchUserButton.Depth = 0;
            this.shearchUserButton.Icon = null;
            this.shearchUserButton.Location = new System.Drawing.Point(3, 31);
            this.shearchUserButton.MouseState = MaterialSkin.MouseState.HOVER;
            this.shearchUserButton.Name = "shearchUserButton";
            this.shearchUserButton.Primary = true;
            this.shearchUserButton.Size = new System.Drawing.Size(70, 36);
            this.shearchUserButton.TabIndex = 2;
            this.shearchUserButton.Text = "Kersés";
            this.shearchUserButton.UseVisualStyleBackColor = false;
            this.shearchUserButton.Click += new System.EventHandler(this.shearchUserButton_Click);
            // 
            // newUserButton
            // 
            this.newUserButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.newUserButton.AutoSize = true;
            this.newUserButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.newUserButton.Depth = 0;
            this.newUserButton.Icon = null;
            this.newUserButton.Location = new System.Drawing.Point(3, 79);
            this.newUserButton.MouseState = MaterialSkin.MouseState.HOVER;
            this.newUserButton.Name = "newUserButton";
            this.newUserButton.Primary = true;
            this.newUserButton.Size = new System.Drawing.Size(135, 36);
            this.newUserButton.TabIndex = 0;
            this.newUserButton.Text = "Új felhasználó";
            this.newUserButton.UseVisualStyleBackColor = true;
            this.newUserButton.Click += new System.EventHandler(this.newUserButton_Click);
            // 
            // usersDataGridView
            // 
            this.usersDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.usersDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.usersDataGridView.Location = new System.Drawing.Point(3, 3);
            this.usersDataGridView.Name = "usersDataGridView";
            this.usersDataGridView.Size = new System.Drawing.Size(907, 297);
            this.usersDataGridView.TabIndex = 0;
            // 
            // familysTab
            // 
            this.familysTab.Controls.Add(this.panel2);
            this.familysTab.Controls.Add(this.familysDataGridView);
            this.familysTab.Location = new System.Drawing.Point(4, 22);
            this.familysTab.Name = "familysTab";
            this.familysTab.Padding = new System.Windows.Forms.Padding(3);
            this.familysTab.Size = new System.Drawing.Size(913, 303);
            this.familysTab.TabIndex = 1;
            this.familysTab.Text = "Családok";
            this.familysTab.UseVisualStyleBackColor = true;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.cancelFamilyButton);
            this.panel2.Controls.Add(this.saveFamilyButton);
            this.panel2.Controls.Add(this.reloadFamilyButton);
            this.panel2.Controls.Add(this.deleteFamilyButton);
            this.panel2.Controls.Add(this.searchFamilyInput);
            this.panel2.Controls.Add(this.editFamilyButton);
            this.panel2.Controls.Add(this.searchFamilyButton);
            this.panel2.Controls.Add(this.addNewFamilyButton);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel2.Location = new System.Drawing.Point(716, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(194, 297);
            this.panel2.TabIndex = 4;
            // 
            // cancelFamilyButton
            // 
            this.cancelFamilyButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cancelFamilyButton.AutoSize = true;
            this.cancelFamilyButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.cancelFamilyButton.Depth = 0;
            this.cancelFamilyButton.Icon = null;
            this.cancelFamilyButton.Location = new System.Drawing.Point(3, 228);
            this.cancelFamilyButton.MouseState = MaterialSkin.MouseState.HOVER;
            this.cancelFamilyButton.Name = "cancelFamilyButton";
            this.cancelFamilyButton.Primary = true;
            this.cancelFamilyButton.Size = new System.Drawing.Size(66, 36);
            this.cancelFamilyButton.TabIndex = 7;
            this.cancelFamilyButton.Text = "Mégse";
            this.cancelFamilyButton.UseVisualStyleBackColor = true;
            this.cancelFamilyButton.Visible = false;
            this.cancelFamilyButton.Click += new System.EventHandler(this.cancelFamilyButton_Click);
            // 
            // saveFamilyButton
            // 
            this.saveFamilyButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.saveFamilyButton.AutoSize = true;
            this.saveFamilyButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.saveFamilyButton.Depth = 0;
            this.saveFamilyButton.Icon = null;
            this.saveFamilyButton.Location = new System.Drawing.Point(3, 192);
            this.saveFamilyButton.MouseState = MaterialSkin.MouseState.HOVER;
            this.saveFamilyButton.Name = "saveFamilyButton";
            this.saveFamilyButton.Primary = true;
            this.saveFamilyButton.Size = new System.Drawing.Size(75, 36);
            this.saveFamilyButton.TabIndex = 6;
            this.saveFamilyButton.Text = "Mentés";
            this.saveFamilyButton.UseVisualStyleBackColor = true;
            this.saveFamilyButton.Visible = false;
            this.saveFamilyButton.Click += new System.EventHandler(this.saveFamilyButton_Click);
            // 
            // reloadFamilyButton
            // 
            this.reloadFamilyButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.reloadFamilyButton.AutoSize = true;
            this.reloadFamilyButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.reloadFamilyButton.Depth = 0;
            this.reloadFamilyButton.Icon = null;
            this.reloadFamilyButton.Location = new System.Drawing.Point(3, 264);
            this.reloadFamilyButton.MouseState = MaterialSkin.MouseState.HOVER;
            this.reloadFamilyButton.Name = "reloadFamilyButton";
            this.reloadFamilyButton.Primary = true;
            this.reloadFamilyButton.Size = new System.Drawing.Size(86, 36);
            this.reloadFamilyButton.TabIndex = 5;
            this.reloadFamilyButton.Text = "Frissítés";
            this.reloadFamilyButton.UseVisualStyleBackColor = true;
            this.reloadFamilyButton.Click += new System.EventHandler(this.reloadFamily_Click);
            // 
            // deleteFamilyButton
            // 
            this.deleteFamilyButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.deleteFamilyButton.AutoSize = true;
            this.deleteFamilyButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.deleteFamilyButton.Depth = 0;
            this.deleteFamilyButton.Icon = null;
            this.deleteFamilyButton.Location = new System.Drawing.Point(3, 151);
            this.deleteFamilyButton.MouseState = MaterialSkin.MouseState.HOVER;
            this.deleteFamilyButton.Name = "deleteFamilyButton";
            this.deleteFamilyButton.Primary = true;
            this.deleteFamilyButton.Size = new System.Drawing.Size(71, 36);
            this.deleteFamilyButton.TabIndex = 4;
            this.deleteFamilyButton.Text = "Törlés";
            this.deleteFamilyButton.UseVisualStyleBackColor = true;
            this.deleteFamilyButton.Click += new System.EventHandler(this.deleteFamilyButton_Click);
            // 
            // searchFamilyInput
            // 
            this.searchFamilyInput.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.searchFamilyInput.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.searchFamilyInput.Location = new System.Drawing.Point(3, 0);
            this.searchFamilyInput.Name = "searchFamilyInput";
            this.searchFamilyInput.Size = new System.Drawing.Size(188, 29);
            this.searchFamilyInput.TabIndex = 1;
            // 
            // editFamilyButton
            // 
            this.editFamilyButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.editFamilyButton.AutoSize = true;
            this.editFamilyButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.editFamilyButton.Depth = 0;
            this.editFamilyButton.Icon = null;
            this.editFamilyButton.Location = new System.Drawing.Point(3, 115);
            this.editFamilyButton.MouseState = MaterialSkin.MouseState.HOVER;
            this.editFamilyButton.Name = "editFamilyButton";
            this.editFamilyButton.Primary = true;
            this.editFamilyButton.Size = new System.Drawing.Size(111, 36);
            this.editFamilyButton.TabIndex = 3;
            this.editFamilyButton.Text = "Szerkesztés";
            this.editFamilyButton.UseVisualStyleBackColor = true;
            this.editFamilyButton.Click += new System.EventHandler(this.editFamilyButton_Click);
            // 
            // searchFamilyButton
            // 
            this.searchFamilyButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.searchFamilyButton.AutoSize = true;
            this.searchFamilyButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.searchFamilyButton.Depth = 0;
            this.searchFamilyButton.Icon = null;
            this.searchFamilyButton.Location = new System.Drawing.Point(3, 31);
            this.searchFamilyButton.MouseState = MaterialSkin.MouseState.HOVER;
            this.searchFamilyButton.Name = "searchFamilyButton";
            this.searchFamilyButton.Primary = true;
            this.searchFamilyButton.Size = new System.Drawing.Size(70, 36);
            this.searchFamilyButton.TabIndex = 2;
            this.searchFamilyButton.Text = "Kersés";
            this.searchFamilyButton.UseVisualStyleBackColor = true;
            this.searchFamilyButton.Click += new System.EventHandler(this.searchFamilyButton_Click);
            // 
            // addNewFamilyButton
            // 
            this.addNewFamilyButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.addNewFamilyButton.AutoSize = true;
            this.addNewFamilyButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.addNewFamilyButton.Depth = 0;
            this.addNewFamilyButton.Icon = null;
            this.addNewFamilyButton.Location = new System.Drawing.Point(3, 79);
            this.addNewFamilyButton.MouseState = MaterialSkin.MouseState.HOVER;
            this.addNewFamilyButton.Name = "addNewFamilyButton";
            this.addNewFamilyButton.Primary = true;
            this.addNewFamilyButton.Size = new System.Drawing.Size(93, 36);
            this.addNewFamilyButton.TabIndex = 0;
            this.addNewFamilyButton.Text = "Új család";
            this.addNewFamilyButton.UseVisualStyleBackColor = true;
            this.addNewFamilyButton.Click += new System.EventHandler(this.addNewFamilyButton_Click);
            // 
            // familysDataGridView
            // 
            this.familysDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.familysDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.familysDataGridView.Location = new System.Drawing.Point(3, 3);
            this.familysDataGridView.Name = "familysDataGridView";
            this.familysDataGridView.Size = new System.Drawing.Size(907, 297);
            this.familysDataGridView.TabIndex = 3;
            // 
            // programsTab
            // 
            this.programsTab.Controls.Add(this.panel3);
            this.programsTab.Controls.Add(this.programsDataGridView);
            this.programsTab.Location = new System.Drawing.Point(4, 22);
            this.programsTab.Name = "programsTab";
            this.programsTab.Size = new System.Drawing.Size(913, 303);
            this.programsTab.TabIndex = 2;
            this.programsTab.Text = "Programok";
            this.programsTab.UseVisualStyleBackColor = true;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.cancelProgramButton);
            this.panel3.Controls.Add(this.saveProgramButton);
            this.panel3.Controls.Add(this.reloadProgramsButton);
            this.panel3.Controls.Add(this.deleteProgramButton);
            this.panel3.Controls.Add(this.searchProgramInput);
            this.panel3.Controls.Add(this.editProgramButton);
            this.panel3.Controls.Add(this.searchProgramButton);
            this.panel3.Controls.Add(this.newProgramButton);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel3.Location = new System.Drawing.Point(719, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(194, 303);
            this.panel3.TabIndex = 4;
            // 
            // cancelProgramButton
            // 
            this.cancelProgramButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cancelProgramButton.AutoSize = true;
            this.cancelProgramButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.cancelProgramButton.Depth = 0;
            this.cancelProgramButton.Icon = null;
            this.cancelProgramButton.Location = new System.Drawing.Point(3, 234);
            this.cancelProgramButton.MouseState = MaterialSkin.MouseState.HOVER;
            this.cancelProgramButton.Name = "cancelProgramButton";
            this.cancelProgramButton.Primary = true;
            this.cancelProgramButton.Size = new System.Drawing.Size(66, 36);
            this.cancelProgramButton.TabIndex = 7;
            this.cancelProgramButton.Text = "Mégse";
            this.cancelProgramButton.UseVisualStyleBackColor = true;
            this.cancelProgramButton.Visible = false;
            // 
            // saveProgramButton
            // 
            this.saveProgramButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.saveProgramButton.AutoSize = true;
            this.saveProgramButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.saveProgramButton.Depth = 0;
            this.saveProgramButton.Icon = null;
            this.saveProgramButton.Location = new System.Drawing.Point(3, 198);
            this.saveProgramButton.MouseState = MaterialSkin.MouseState.HOVER;
            this.saveProgramButton.Name = "saveProgramButton";
            this.saveProgramButton.Primary = true;
            this.saveProgramButton.Size = new System.Drawing.Size(75, 36);
            this.saveProgramButton.TabIndex = 6;
            this.saveProgramButton.Text = "Mentés";
            this.saveProgramButton.UseVisualStyleBackColor = true;
            this.saveProgramButton.Visible = false;
            // 
            // reloadProgramsButton
            // 
            this.reloadProgramsButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.reloadProgramsButton.AutoSize = true;
            this.reloadProgramsButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.reloadProgramsButton.Depth = 0;
            this.reloadProgramsButton.Icon = null;
            this.reloadProgramsButton.Location = new System.Drawing.Point(3, 270);
            this.reloadProgramsButton.MouseState = MaterialSkin.MouseState.HOVER;
            this.reloadProgramsButton.Name = "reloadProgramsButton";
            this.reloadProgramsButton.Primary = true;
            this.reloadProgramsButton.Size = new System.Drawing.Size(86, 36);
            this.reloadProgramsButton.TabIndex = 5;
            this.reloadProgramsButton.Text = "Frissítés";
            this.reloadProgramsButton.UseVisualStyleBackColor = true;
            // 
            // deleteProgramButton
            // 
            this.deleteProgramButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.deleteProgramButton.AutoSize = true;
            this.deleteProgramButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.deleteProgramButton.Depth = 0;
            this.deleteProgramButton.Icon = null;
            this.deleteProgramButton.Location = new System.Drawing.Point(3, 151);
            this.deleteProgramButton.MouseState = MaterialSkin.MouseState.HOVER;
            this.deleteProgramButton.Name = "deleteProgramButton";
            this.deleteProgramButton.Primary = true;
            this.deleteProgramButton.Size = new System.Drawing.Size(71, 36);
            this.deleteProgramButton.TabIndex = 4;
            this.deleteProgramButton.Text = "Törlés";
            this.deleteProgramButton.UseVisualStyleBackColor = true;
            // 
            // searchProgramInput
            // 
            this.searchProgramInput.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.searchProgramInput.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.searchProgramInput.Location = new System.Drawing.Point(3, 0);
            this.searchProgramInput.Name = "searchProgramInput";
            this.searchProgramInput.Size = new System.Drawing.Size(188, 29);
            this.searchProgramInput.TabIndex = 1;
            // 
            // editProgramButton
            // 
            this.editProgramButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.editProgramButton.AutoSize = true;
            this.editProgramButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.editProgramButton.Depth = 0;
            this.editProgramButton.Icon = null;
            this.editProgramButton.Location = new System.Drawing.Point(3, 115);
            this.editProgramButton.MouseState = MaterialSkin.MouseState.HOVER;
            this.editProgramButton.Name = "editProgramButton";
            this.editProgramButton.Primary = true;
            this.editProgramButton.Size = new System.Drawing.Size(111, 36);
            this.editProgramButton.TabIndex = 3;
            this.editProgramButton.Text = "Szerkesztés";
            this.editProgramButton.UseVisualStyleBackColor = true;
            // 
            // searchProgramButton
            // 
            this.searchProgramButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.searchProgramButton.AutoSize = true;
            this.searchProgramButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.searchProgramButton.Depth = 0;
            this.searchProgramButton.Icon = null;
            this.searchProgramButton.Location = new System.Drawing.Point(3, 31);
            this.searchProgramButton.MouseState = MaterialSkin.MouseState.HOVER;
            this.searchProgramButton.Name = "searchProgramButton";
            this.searchProgramButton.Primary = true;
            this.searchProgramButton.Size = new System.Drawing.Size(70, 36);
            this.searchProgramButton.TabIndex = 2;
            this.searchProgramButton.Text = "Kersés";
            this.searchProgramButton.UseVisualStyleBackColor = true;
            // 
            // newProgramButton
            // 
            this.newProgramButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.newProgramButton.AutoSize = true;
            this.newProgramButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.newProgramButton.Depth = 0;
            this.newProgramButton.Icon = null;
            this.newProgramButton.Location = new System.Drawing.Point(3, 79);
            this.newProgramButton.MouseState = MaterialSkin.MouseState.HOVER;
            this.newProgramButton.Name = "newProgramButton";
            this.newProgramButton.Primary = true;
            this.newProgramButton.Size = new System.Drawing.Size(107, 36);
            this.newProgramButton.TabIndex = 0;
            this.newProgramButton.Text = "Új program";
            this.newProgramButton.UseVisualStyleBackColor = true;
            // 
            // programsDataGridView
            // 
            this.programsDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.programsDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.programsDataGridView.Location = new System.Drawing.Point(0, 0);
            this.programsDataGridView.Name = "programsDataGridView";
            this.programsDataGridView.Size = new System.Drawing.Size(913, 303);
            this.programsDataGridView.TabIndex = 3;
            // 
            // exercisesTab
            // 
            this.exercisesTab.Controls.Add(this.panel4);
            this.exercisesTab.Controls.Add(this.exercisesDataGridView);
            this.exercisesTab.Location = new System.Drawing.Point(4, 22);
            this.exercisesTab.Name = "exercisesTab";
            this.exercisesTab.Size = new System.Drawing.Size(913, 303);
            this.exercisesTab.TabIndex = 3;
            this.exercisesTab.Text = "Feladatok";
            this.exercisesTab.UseVisualStyleBackColor = true;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.cancelExerciseButton);
            this.panel4.Controls.Add(this.saveExerciseButton);
            this.panel4.Controls.Add(this.reloadExerciseButton);
            this.panel4.Controls.Add(this.deleteExerciseButton);
            this.panel4.Controls.Add(this.searchExerciseInput);
            this.panel4.Controls.Add(this.editExerciseButton);
            this.panel4.Controls.Add(this.searchExerciseButton);
            this.panel4.Controls.Add(this.newExerciseButton);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel4.Location = new System.Drawing.Point(719, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(194, 303);
            this.panel4.TabIndex = 6;
            // 
            // cancelExerciseButton
            // 
            this.cancelExerciseButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cancelExerciseButton.AutoSize = true;
            this.cancelExerciseButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.cancelExerciseButton.Depth = 0;
            this.cancelExerciseButton.Icon = null;
            this.cancelExerciseButton.Location = new System.Drawing.Point(3, 234);
            this.cancelExerciseButton.MouseState = MaterialSkin.MouseState.HOVER;
            this.cancelExerciseButton.Name = "cancelExerciseButton";
            this.cancelExerciseButton.Primary = true;
            this.cancelExerciseButton.Size = new System.Drawing.Size(66, 36);
            this.cancelExerciseButton.TabIndex = 7;
            this.cancelExerciseButton.Text = "Mégse";
            this.cancelExerciseButton.UseVisualStyleBackColor = true;
            this.cancelExerciseButton.Visible = false;
            // 
            // saveExerciseButton
            // 
            this.saveExerciseButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.saveExerciseButton.AutoSize = true;
            this.saveExerciseButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.saveExerciseButton.Depth = 0;
            this.saveExerciseButton.Icon = null;
            this.saveExerciseButton.Location = new System.Drawing.Point(3, 198);
            this.saveExerciseButton.MouseState = MaterialSkin.MouseState.HOVER;
            this.saveExerciseButton.Name = "saveExerciseButton";
            this.saveExerciseButton.Primary = true;
            this.saveExerciseButton.Size = new System.Drawing.Size(75, 36);
            this.saveExerciseButton.TabIndex = 6;
            this.saveExerciseButton.Text = "Mentés";
            this.saveExerciseButton.UseVisualStyleBackColor = true;
            this.saveExerciseButton.Visible = false;
            // 
            // reloadExerciseButton
            // 
            this.reloadExerciseButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.reloadExerciseButton.AutoSize = true;
            this.reloadExerciseButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.reloadExerciseButton.Depth = 0;
            this.reloadExerciseButton.Icon = null;
            this.reloadExerciseButton.Location = new System.Drawing.Point(3, 270);
            this.reloadExerciseButton.MouseState = MaterialSkin.MouseState.HOVER;
            this.reloadExerciseButton.Name = "reloadExerciseButton";
            this.reloadExerciseButton.Primary = true;
            this.reloadExerciseButton.Size = new System.Drawing.Size(86, 36);
            this.reloadExerciseButton.TabIndex = 5;
            this.reloadExerciseButton.Text = "Frissítés";
            this.reloadExerciseButton.UseVisualStyleBackColor = true;
            // 
            // deleteExerciseButton
            // 
            this.deleteExerciseButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.deleteExerciseButton.AutoSize = true;
            this.deleteExerciseButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.deleteExerciseButton.Depth = 0;
            this.deleteExerciseButton.Icon = null;
            this.deleteExerciseButton.Location = new System.Drawing.Point(3, 151);
            this.deleteExerciseButton.MouseState = MaterialSkin.MouseState.HOVER;
            this.deleteExerciseButton.Name = "deleteExerciseButton";
            this.deleteExerciseButton.Primary = true;
            this.deleteExerciseButton.Size = new System.Drawing.Size(71, 36);
            this.deleteExerciseButton.TabIndex = 4;
            this.deleteExerciseButton.Text = "Törlés";
            this.deleteExerciseButton.UseVisualStyleBackColor = true;
            // 
            // searchExerciseInput
            // 
            this.searchExerciseInput.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.searchExerciseInput.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.searchExerciseInput.Location = new System.Drawing.Point(3, 0);
            this.searchExerciseInput.Name = "searchExerciseInput";
            this.searchExerciseInput.Size = new System.Drawing.Size(188, 29);
            this.searchExerciseInput.TabIndex = 1;
            // 
            // editExerciseButton
            // 
            this.editExerciseButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.editExerciseButton.AutoSize = true;
            this.editExerciseButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.editExerciseButton.Depth = 0;
            this.editExerciseButton.Icon = null;
            this.editExerciseButton.Location = new System.Drawing.Point(3, 115);
            this.editExerciseButton.MouseState = MaterialSkin.MouseState.HOVER;
            this.editExerciseButton.Name = "editExerciseButton";
            this.editExerciseButton.Primary = true;
            this.editExerciseButton.Size = new System.Drawing.Size(111, 36);
            this.editExerciseButton.TabIndex = 3;
            this.editExerciseButton.Text = "Szerkesztés";
            this.editExerciseButton.UseVisualStyleBackColor = true;
            // 
            // searchExerciseButton
            // 
            this.searchExerciseButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.searchExerciseButton.AutoSize = true;
            this.searchExerciseButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.searchExerciseButton.Depth = 0;
            this.searchExerciseButton.Icon = null;
            this.searchExerciseButton.Location = new System.Drawing.Point(3, 31);
            this.searchExerciseButton.MouseState = MaterialSkin.MouseState.HOVER;
            this.searchExerciseButton.Name = "searchExerciseButton";
            this.searchExerciseButton.Primary = true;
            this.searchExerciseButton.Size = new System.Drawing.Size(70, 36);
            this.searchExerciseButton.TabIndex = 2;
            this.searchExerciseButton.Text = "Kersés";
            this.searchExerciseButton.UseVisualStyleBackColor = true;
            // 
            // newExerciseButton
            // 
            this.newExerciseButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.newExerciseButton.AutoSize = true;
            this.newExerciseButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.newExerciseButton.Depth = 0;
            this.newExerciseButton.Icon = null;
            this.newExerciseButton.Location = new System.Drawing.Point(3, 79);
            this.newExerciseButton.MouseState = MaterialSkin.MouseState.HOVER;
            this.newExerciseButton.Name = "newExerciseButton";
            this.newExerciseButton.Primary = true;
            this.newExerciseButton.Size = new System.Drawing.Size(99, 36);
            this.newExerciseButton.TabIndex = 0;
            this.newExerciseButton.Text = "Új feladat";
            this.newExerciseButton.UseVisualStyleBackColor = true;
            // 
            // exercisesDataGridView
            // 
            this.exercisesDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.exercisesDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.exercisesDataGridView.Location = new System.Drawing.Point(0, 0);
            this.exercisesDataGridView.Name = "exercisesDataGridView";
            this.exercisesDataGridView.Size = new System.Drawing.Size(913, 303);
            this.exercisesDataGridView.TabIndex = 5;
            // 
            // routinsTab
            // 
            this.routinsTab.Controls.Add(this.panel5);
            this.routinsTab.Controls.Add(this.routinesDataGridView);
            this.routinsTab.Location = new System.Drawing.Point(4, 22);
            this.routinsTab.Name = "routinsTab";
            this.routinsTab.Padding = new System.Windows.Forms.Padding(3);
            this.routinsTab.Size = new System.Drawing.Size(913, 303);
            this.routinsTab.TabIndex = 4;
            this.routinsTab.Text = "Rutinok";
            this.routinsTab.UseVisualStyleBackColor = true;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.cancelRoutineButton);
            this.panel5.Controls.Add(this.saveRoutineButton);
            this.panel5.Controls.Add(this.reloadRoutineButton);
            this.panel5.Controls.Add(this.deleteRoutineButton);
            this.panel5.Controls.Add(this.searchRoutineInput);
            this.panel5.Controls.Add(this.editRoutineButton);
            this.panel5.Controls.Add(this.searchRoutineButton);
            this.panel5.Controls.Add(this.newRoutineButton);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel5.Location = new System.Drawing.Point(716, 3);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(194, 297);
            this.panel5.TabIndex = 4;
            // 
            // cancelRoutineButton
            // 
            this.cancelRoutineButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cancelRoutineButton.AutoSize = true;
            this.cancelRoutineButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.cancelRoutineButton.Depth = 0;
            this.cancelRoutineButton.Icon = null;
            this.cancelRoutineButton.Location = new System.Drawing.Point(3, 228);
            this.cancelRoutineButton.MouseState = MaterialSkin.MouseState.HOVER;
            this.cancelRoutineButton.Name = "cancelRoutineButton";
            this.cancelRoutineButton.Primary = true;
            this.cancelRoutineButton.Size = new System.Drawing.Size(66, 36);
            this.cancelRoutineButton.TabIndex = 7;
            this.cancelRoutineButton.Text = "Mégse";
            this.cancelRoutineButton.UseVisualStyleBackColor = true;
            this.cancelRoutineButton.Visible = false;
            // 
            // saveRoutineButton
            // 
            this.saveRoutineButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.saveRoutineButton.AutoSize = true;
            this.saveRoutineButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.saveRoutineButton.Depth = 0;
            this.saveRoutineButton.Icon = null;
            this.saveRoutineButton.Location = new System.Drawing.Point(3, 192);
            this.saveRoutineButton.MouseState = MaterialSkin.MouseState.HOVER;
            this.saveRoutineButton.Name = "saveRoutineButton";
            this.saveRoutineButton.Primary = true;
            this.saveRoutineButton.Size = new System.Drawing.Size(75, 36);
            this.saveRoutineButton.TabIndex = 6;
            this.saveRoutineButton.Text = "Mentés";
            this.saveRoutineButton.UseVisualStyleBackColor = true;
            this.saveRoutineButton.Visible = false;
            // 
            // reloadRoutineButton
            // 
            this.reloadRoutineButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.reloadRoutineButton.AutoSize = true;
            this.reloadRoutineButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.reloadRoutineButton.Depth = 0;
            this.reloadRoutineButton.Icon = null;
            this.reloadRoutineButton.Location = new System.Drawing.Point(3, 264);
            this.reloadRoutineButton.MouseState = MaterialSkin.MouseState.HOVER;
            this.reloadRoutineButton.Name = "reloadRoutineButton";
            this.reloadRoutineButton.Primary = true;
            this.reloadRoutineButton.Size = new System.Drawing.Size(86, 36);
            this.reloadRoutineButton.TabIndex = 5;
            this.reloadRoutineButton.Text = "Frissítés";
            this.reloadRoutineButton.UseVisualStyleBackColor = true;
            // 
            // deleteRoutineButton
            // 
            this.deleteRoutineButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.deleteRoutineButton.AutoSize = true;
            this.deleteRoutineButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.deleteRoutineButton.Depth = 0;
            this.deleteRoutineButton.Icon = null;
            this.deleteRoutineButton.Location = new System.Drawing.Point(3, 151);
            this.deleteRoutineButton.MouseState = MaterialSkin.MouseState.HOVER;
            this.deleteRoutineButton.Name = "deleteRoutineButton";
            this.deleteRoutineButton.Primary = true;
            this.deleteRoutineButton.Size = new System.Drawing.Size(71, 36);
            this.deleteRoutineButton.TabIndex = 4;
            this.deleteRoutineButton.Text = "Törlés";
            this.deleteRoutineButton.UseVisualStyleBackColor = true;
            // 
            // searchRoutineInput
            // 
            this.searchRoutineInput.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.searchRoutineInput.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.searchRoutineInput.Location = new System.Drawing.Point(3, 0);
            this.searchRoutineInput.Name = "searchRoutineInput";
            this.searchRoutineInput.Size = new System.Drawing.Size(188, 29);
            this.searchRoutineInput.TabIndex = 1;
            // 
            // editRoutineButton
            // 
            this.editRoutineButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.editRoutineButton.AutoSize = true;
            this.editRoutineButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.editRoutineButton.Depth = 0;
            this.editRoutineButton.Icon = null;
            this.editRoutineButton.Location = new System.Drawing.Point(3, 115);
            this.editRoutineButton.MouseState = MaterialSkin.MouseState.HOVER;
            this.editRoutineButton.Name = "editRoutineButton";
            this.editRoutineButton.Primary = true;
            this.editRoutineButton.Size = new System.Drawing.Size(111, 36);
            this.editRoutineButton.TabIndex = 3;
            this.editRoutineButton.Text = "Szerkesztés";
            this.editRoutineButton.UseVisualStyleBackColor = true;
            // 
            // searchRoutineButton
            // 
            this.searchRoutineButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.searchRoutineButton.AutoSize = true;
            this.searchRoutineButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.searchRoutineButton.Depth = 0;
            this.searchRoutineButton.Icon = null;
            this.searchRoutineButton.Location = new System.Drawing.Point(3, 31);
            this.searchRoutineButton.MouseState = MaterialSkin.MouseState.HOVER;
            this.searchRoutineButton.Name = "searchRoutineButton";
            this.searchRoutineButton.Primary = true;
            this.searchRoutineButton.Size = new System.Drawing.Size(70, 36);
            this.searchRoutineButton.TabIndex = 2;
            this.searchRoutineButton.Text = "Kersés";
            this.searchRoutineButton.UseVisualStyleBackColor = true;
            // 
            // newRoutineButton
            // 
            this.newRoutineButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.newRoutineButton.AutoSize = true;
            this.newRoutineButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.newRoutineButton.Depth = 0;
            this.newRoutineButton.Icon = null;
            this.newRoutineButton.Location = new System.Drawing.Point(3, 79);
            this.newRoutineButton.MouseState = MaterialSkin.MouseState.HOVER;
            this.newRoutineButton.Name = "newRoutineButton";
            this.newRoutineButton.Primary = true;
            this.newRoutineButton.Size = new System.Drawing.Size(135, 36);
            this.newRoutineButton.TabIndex = 0;
            this.newRoutineButton.Text = "Új felhasználó";
            this.newRoutineButton.UseVisualStyleBackColor = true;
            // 
            // routinesDataGridView
            // 
            this.routinesDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.routinesDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.routinesDataGridView.Location = new System.Drawing.Point(3, 3);
            this.routinesDataGridView.Name = "routinesDataGridView";
            this.routinesDataGridView.Size = new System.Drawing.Size(907, 297);
            this.routinesDataGridView.TabIndex = 3;
            // 
            // staticsTab
            // 
            this.staticsTab.Controls.Add(this.statisticChart);
            this.staticsTab.Location = new System.Drawing.Point(4, 22);
            this.staticsTab.Name = "staticsTab";
            this.staticsTab.Size = new System.Drawing.Size(913, 303);
            this.staticsTab.TabIndex = 5;
            this.staticsTab.Text = "Statisztika";
            this.staticsTab.UseVisualStyleBackColor = true;
            // 
            // statisticChart
            // 
            this.statisticChart.AllowDrop = true;
            chartArea1.Name = "ChartArea1";
            this.statisticChart.ChartAreas.Add(chartArea1);
            this.statisticChart.Dock = System.Windows.Forms.DockStyle.Fill;
            legend1.Name = "Legend1";
            this.statisticChart.Legends.Add(legend1);
            this.statisticChart.Location = new System.Drawing.Point(0, 0);
            this.statisticChart.Name = "statisticChart";
            this.statisticChart.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.Bright;
            series1.ChartArea = "ChartArea1";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Doughnut;
            series1.Color = System.Drawing.Color.Blue;
            series1.Legend = "Legend1";
            series1.Name = "Összes felhasználó";
            series2.ChartArea = "ChartArea1";
            series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Doughnut;
            series2.Color = System.Drawing.Color.Lime;
            series2.Legend = "Legend1";
            series2.Name = "Családtagok";
            series2.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.EarthTones;
            this.statisticChart.Series.Add(series1);
            this.statisticChart.Series.Add(series2);
            this.statisticChart.Size = new System.Drawing.Size(913, 303);
            this.statisticChart.TabIndex = 0;
            this.statisticChart.Text = "chart1";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.DarkSlateGray;
            this.panel1.Controls.Add(this.adminLogoutUsersButton);
            this.panel1.Controls.Add(this.adminEmailLabel);
            this.panel1.Controls.Add(this.adminUsernameLabel);
            this.panel1.Controls.Add(this.adminProfPict);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 387);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(921, 65);
            this.panel1.TabIndex = 2;
            // 
            // adminLogoutUsersButton
            // 
            this.adminLogoutUsersButton.AutoSize = true;
            this.adminLogoutUsersButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.adminLogoutUsersButton.Depth = 0;
            this.adminLogoutUsersButton.Dock = System.Windows.Forms.DockStyle.Right;
            this.adminLogoutUsersButton.Icon = null;
            this.adminLogoutUsersButton.Location = new System.Drawing.Point(798, 0);
            this.adminLogoutUsersButton.MouseState = MaterialSkin.MouseState.HOVER;
            this.adminLogoutUsersButton.Name = "adminLogoutUsersButton";
            this.adminLogoutUsersButton.Primary = true;
            this.adminLogoutUsersButton.Size = new System.Drawing.Size(123, 65);
            this.adminLogoutUsersButton.TabIndex = 4;
            this.adminLogoutUsersButton.Text = "Kijelentkezés";
            this.adminLogoutUsersButton.UseVisualStyleBackColor = true;
            this.adminLogoutUsersButton.Click += new System.EventHandler(this.adminLogoutUsersButton_Click);
            // 
            // adminEmailLabel
            // 
            this.adminEmailLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.adminEmailLabel.AutoSize = true;
            this.adminEmailLabel.Location = new System.Drawing.Point(91, 25);
            this.adminEmailLabel.Name = "adminEmailLabel";
            this.adminEmailLabel.Size = new System.Drawing.Size(32, 13);
            this.adminEmailLabel.TabIndex = 3;
            this.adminEmailLabel.Text = "Email";
            // 
            // adminUsernameLabel
            // 
            this.adminUsernameLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.adminUsernameLabel.AutoSize = true;
            this.adminUsernameLabel.Location = new System.Drawing.Point(91, 6);
            this.adminUsernameLabel.Name = "adminUsernameLabel";
            this.adminUsernameLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.adminUsernameLabel.Size = new System.Drawing.Size(86, 13);
            this.adminUsernameLabel.TabIndex = 2;
            this.adminUsernameLabel.Text = "Felhastználómév";
            // 
            // adminProfPict
            // 
            this.adminProfPict.BackColor = System.Drawing.Color.LightCyan;
            this.adminProfPict.BackgroundImage = global::FamilyManager.Properties.Resources.adminIcon;
            this.adminProfPict.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.adminProfPict.Location = new System.Drawing.Point(9, 6);
            this.adminProfPict.Name = "adminProfPict";
            this.adminProfPict.Size = new System.Drawing.Size(75, 50);
            this.adminProfPict.TabIndex = 0;
            this.adminProfPict.TabStop = false;
            // 
            // menuTabSelector
            // 
            this.menuTabSelector.BaseTabControl = this.admonTabControl;
            this.menuTabSelector.Depth = 0;
            this.menuTabSelector.Dock = System.Windows.Forms.DockStyle.Fill;
            this.menuTabSelector.Location = new System.Drawing.Point(3, 3);
            this.menuTabSelector.MouseState = MaterialSkin.MouseState.HOVER;
            this.menuTabSelector.Name = "menuTabSelector";
            this.menuTabSelector.Size = new System.Drawing.Size(921, 43);
            this.menuTabSelector.TabIndex = 3;
            this.menuTabSelector.Text = "materialTabSelector1";
            this.menuTabSelector.Click += new System.EventHandler(this.menuTabSelector_Click);
            // 
            // AdminLoggedinForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(927, 514);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "AdminLoggedinForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "AdminLoggedinForm";
            this.Load += new System.EventHandler(this.AdminLoggedinForm_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.admonTabControl.ResumeLayout(false);
            this.usersTab.ResumeLayout(false);
            this.usersButtonsPanel.ResumeLayout(false);
            this.usersButtonsPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.usersDataGridView)).EndInit();
            this.familysTab.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.familysDataGridView)).EndInit();
            this.programsTab.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.programsDataGridView)).EndInit();
            this.exercisesTab.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.exercisesDataGridView)).EndInit();
            this.routinsTab.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.routinesDataGridView)).EndInit();
            this.staticsTab.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.statisticChart)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.adminProfPict)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TabPage usersTab;
        private System.Windows.Forms.TabPage familysTab;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TabPage programsTab;
        private System.Windows.Forms.TabPage exercisesTab;
        private System.Windows.Forms.PictureBox adminProfPict;
        private System.Windows.Forms.Label adminUsernameLabel;
        private System.Windows.Forms.Label adminEmailLabel;
        private System.Windows.Forms.DataGridView usersDataGridView;
        private System.Windows.Forms.TextBox userSearchInput;
        private System.Windows.Forms.Panel usersButtonsPanel;
        private MaterialSkin.Controls.MaterialRaisedButton cancelUsersButton;
        private MaterialSkin.Controls.MaterialRaisedButton saveUsersButton;
        private MaterialSkin.Controls.MaterialRaisedButton reloadFamily;
        private MaterialSkin.Controls.MaterialRaisedButton deleteUserButton;
        private MaterialSkin.Controls.MaterialRaisedButton editUserButton;
        private MaterialSkin.Controls.MaterialRaisedButton shearchUserButton;
        private MaterialSkin.Controls.MaterialRaisedButton newUserButton;
        private MaterialSkin.Controls.MaterialRaisedButton adminLogoutUsersButton;
        private System.Windows.Forms.Panel panel2;
        private MaterialSkin.Controls.MaterialRaisedButton cancelFamilyButton;
        private MaterialSkin.Controls.MaterialRaisedButton saveFamilyButton;
        private MaterialSkin.Controls.MaterialRaisedButton reloadFamilyButton;
        private MaterialSkin.Controls.MaterialRaisedButton deleteFamilyButton;
        private System.Windows.Forms.TextBox searchFamilyInput;
        private MaterialSkin.Controls.MaterialRaisedButton editFamilyButton;
        private MaterialSkin.Controls.MaterialRaisedButton searchFamilyButton;
        private MaterialSkin.Controls.MaterialRaisedButton addNewFamilyButton;
        private System.Windows.Forms.DataGridView familysDataGridView;
        private MaterialSkin.Controls.MaterialTabControl admonTabControl;
        private MaterialSkin.Controls.MaterialTabSelector menuTabSelector;
        private System.Windows.Forms.Panel panel3;
        private MaterialSkin.Controls.MaterialRaisedButton cancelProgramButton;
        private MaterialSkin.Controls.MaterialRaisedButton saveProgramButton;
        private MaterialSkin.Controls.MaterialRaisedButton reloadProgramsButton;
        private MaterialSkin.Controls.MaterialRaisedButton deleteProgramButton;
        private System.Windows.Forms.TextBox searchProgramInput;
        private MaterialSkin.Controls.MaterialRaisedButton editProgramButton;
        private MaterialSkin.Controls.MaterialRaisedButton searchProgramButton;
        private MaterialSkin.Controls.MaterialRaisedButton newProgramButton;
        private System.Windows.Forms.DataGridView programsDataGridView;
        private System.Windows.Forms.Panel panel4;
        private MaterialSkin.Controls.MaterialRaisedButton cancelExerciseButton;
        private MaterialSkin.Controls.MaterialRaisedButton saveExerciseButton;
        private MaterialSkin.Controls.MaterialRaisedButton reloadExerciseButton;
        private MaterialSkin.Controls.MaterialRaisedButton deleteExerciseButton;
        private System.Windows.Forms.TextBox searchExerciseInput;
        private MaterialSkin.Controls.MaterialRaisedButton editExerciseButton;
        private MaterialSkin.Controls.MaterialRaisedButton searchExerciseButton;
        private MaterialSkin.Controls.MaterialRaisedButton newExerciseButton;
        private System.Windows.Forms.DataGridView exercisesDataGridView;
        private System.Windows.Forms.TabPage routinsTab;
        private System.Windows.Forms.TabPage staticsTab;
        private System.Windows.Forms.Panel panel5;
        private MaterialSkin.Controls.MaterialRaisedButton cancelRoutineButton;
        private MaterialSkin.Controls.MaterialRaisedButton saveRoutineButton;
        private MaterialSkin.Controls.MaterialRaisedButton reloadRoutineButton;
        private MaterialSkin.Controls.MaterialRaisedButton deleteRoutineButton;
        private System.Windows.Forms.TextBox searchRoutineInput;
        private MaterialSkin.Controls.MaterialRaisedButton editRoutineButton;
        private MaterialSkin.Controls.MaterialRaisedButton searchRoutineButton;
        private MaterialSkin.Controls.MaterialRaisedButton newRoutineButton;
        private System.Windows.Forms.DataGridView routinesDataGridView;
        private System.Windows.Forms.DataVisualization.Charting.Chart statisticChart;
    }
}