﻿using MaterialSkin.Controls;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FamilyManager
{
    public partial class ConnectFamilyForm : MaterialForm
    {
        public string familyName;
        public int selectedFamilyID;

        public ConnectFamilyForm()
        {
            InitializeComponent();

            findedFamilysListView.Columns.Add("Családazonosító");
            findedFamilysListView.Columns.Add("Családkép");
            findedFamilysListView.Columns.Add("Családalapító");
            findedFamilysListView.Columns.Add("Családnév");

            findedFamilysListView.Columns[0].Width = -2;
            findedFamilysListView.Columns[1].Width = -2;
            findedFamilysListView.Columns[2].Width = -2;
            findedFamilysListView.Columns[3].Width = -2;
        }

        private void createTheFamilyButton_Click(object sender, EventArgs e)
        {
            findedFamilysListView.Items.Clear();

            familyName = searchFamilyNameInput.Text;
            Database db = new Database();
            db.OpenConnection();
            MySqlCommand command = db.connection.CreateCommand();
            command.CommandType = CommandType.Text;
            command.CommandText = "SELECT familyID, familyPicture, adminID, familyName FROM familys WHERE familyName ='" + familyName + "'";
            command.ExecuteNonQuery();
            DataTable dt = new DataTable();
            MySqlDataAdapter da = new MySqlDataAdapter(command);
            da.Fill(dt);
            int i = Convert.ToInt32(dt.Rows.Count.ToString());

            if (i > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    ListViewItem familyItem = new ListViewItem(row["familyID"].ToString());
                    familyItem.SubItems.Add(row["familyPicture"].ToString());
                    familyItem.SubItems.Add(row["adminID"].ToString());
                    familyItem.SubItems.Add(row["familyName"].ToString());
                    findedFamilysListView.Items.Add(familyItem);
                }
                tabControl.SelectTab(1);
            }
            else
            {
                errorProvider.SetError(searchFamilyNameInput, "Nem található család ezzel a névvel.");
            }
        }

        private void backToSearchButton_Click(object sender, EventArgs e)
        {
            tabControl.SelectTab(0);
        }

        private void connectToSelectedFamilyButton_Click(object sender, EventArgs e)
        {
            selectedFamilyID = Convert.ToInt32(findedFamilysListView.SelectedItems[0].SubItems[0].Text);
            createTheFamilyButton.DialogResult = DialogResult.OK;
        }
    }
}
