﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using FamilyManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FamilyManager.Tests
{
    [TestClass()]
    public class LoginPasswordValidatorTests
    {
        [TestMethod()]
        public void ValidPasswordTestEmpty()
        {
            LoginPasswordValidator PV = new LoginPasswordValidator("As");

            bool expected = true;
            bool actual = PV.ValidPasswordEmpty();
            Assert.AreEqual(expected, actual, "Nem ad vissza hamis értéket ha a jelszó mező ki van töltve!");
        }

        [TestMethod()]
        public void ValidPasswordTestEmptyFalse()
        {
            LoginPasswordValidator PV = new LoginPasswordValidator("");

            bool expected = false;
            bool actual = PV.ValidPasswordEmpty();
            Assert.AreEqual(expected, actual, "Nem ad vissza hamis értéket ha a jelszó mező nincs kitöltve!");
        }

        [TestMethod()]
        public void ValidPasswordTestLength()
        {
            LoginPasswordValidator PV = new LoginPasswordValidator("somethingpassword");

            bool expected = true;
            bool actual = PV.ValidPasswordAtleast();
            Assert.AreEqual(expected, actual, "Nem ad vissza hamis értéket ha a jelszó tartalmaz elég karaktert!");
        }

        [TestMethod()]
        public void ValidPasswordTestLengthFalse()
        {
            LoginPasswordValidator PV = new LoginPasswordValidator("some");

            bool expected = false;
            bool actual = PV.ValidPasswordAtleast();
            Assert.AreEqual(expected, actual, "Nem ad vissza hamis értéket ha a jelszó nem tartalmaz elég karaktert!");
        }

        public void ValidPasswordTestSpace()
        {
            LoginPasswordValidator PV = new LoginPasswordValidator("something");

            bool expected = true;
            bool actual = PV.ValidPasswordWhiteSpace();
            Assert.AreEqual(expected, actual, "Nem ad vissza hamis értéket ha a jelszó nem tartalmaz szóközt!");
        }

        [TestMethod()]
        public void ValidPasswordTestSpaceFalse()
        {
            LoginPasswordValidator PV = new LoginPasswordValidator("some thing");

            bool expected = false;
            bool actual = PV.ValidPasswordWhiteSpace();
            Assert.AreEqual(expected, actual, "Nem ad vissza hamis értéket ha a jelszó tartalmaz szóközt!");
        }

        [TestMethod()]
        public void ValidPasswordTestUppercase()
        {
            LoginPasswordValidator PV = new LoginPasswordValidator("Something");

            bool expected = true;
            bool actual = PV.ValidPasswordUpperLetter();
            Assert.AreEqual(expected, actual, "Nem ad vissza hamis értéket ha a jelszó tartalmaz nagybetűt!");
        }

        [TestMethod()]
        public void ValidPasswordTestUppercaseFalse()
        {
            LoginPasswordValidator PV = new LoginPasswordValidator("something");

            bool expected = false;
            bool actual = PV.ValidPasswordUpperLetter();
            Assert.AreEqual(expected, actual, "Nem ad vissza hamis értéket ha a jelszó nem tartalmaz nagybetűt!");
        }

        [TestMethod()]
        public void ValidPasswordTestLowercase()
        {
            LoginPasswordValidator PV = new LoginPasswordValidator("Something");

            bool expected = true;
            bool actual = PV.ValidPasswordLowerLetter();
            Assert.AreEqual(expected, actual, "Nem ad vissza hamis értéket ha a jelszó tartalmaz kisbetűt!");
        }

        [TestMethod()]
        public void ValidPasswordTestLowercaseFalse()
        {
            LoginPasswordValidator PV = new LoginPasswordValidator("SOMETHING");

            bool expected = false;
            bool actual = PV.ValidPasswordLowerLetter();
            Assert.AreEqual(expected, actual, "Nem ad vissza hamis értéket ha a jelszó nem tartalmaz kisbetűt!");
        }

        [TestMethod()]
        public void ValidPasswordTestNumbers()
        {
            LoginPasswordValidator PV = new LoginPasswordValidator("Something07");

            bool expected = true;
            bool actual = PV.ValidPasswordNumber();
            Assert.AreEqual(expected, actual, "Nem ad vissza hamis értéket ha a jelszó tartalmaz számot!");
        }

        [TestMethod()]
        public void ValidPasswordTestNumbersFalse()
        {
            LoginPasswordValidator PV = new LoginPasswordValidator("Something");

            bool expected = false;
            bool actual = PV.ValidPasswordNumber();
            Assert.AreEqual(expected, actual, "Nem ad vissza hamis értéket ha a jelszó nem tartalmaz számot!");
        }
    }
}