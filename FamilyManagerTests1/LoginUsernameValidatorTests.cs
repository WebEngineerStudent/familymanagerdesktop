﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using FamilyManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FamilyManager.Tests
{
    [TestClass()]
    public class LoginUsernameValidatorTests
    {
        [TestMethod()]
        public void ValidUsernameTestEmpty()
        {
            LoginUsernameValidator UV = new LoginUsernameValidator("As");

            bool expected = true;
            bool actual = UV.ValidUsernameEmpty();
            Assert.AreEqual(expected, actual, "Nem ad vissza hamis értéket ha a felhasználónév nem üres!");
        }

        [TestMethod()]
        public void ValidUsernameTestEmptyFalse()
        {
            LoginUsernameValidator UV = new LoginUsernameValidator("");

            bool expected = false;
            bool actual = UV.ValidUsernameEmpty();
            Assert.AreEqual(expected, actual, "Nem ad vissza hamis értéket ha a felhasználónév üres!");
        }

        [TestMethod()]
        public void ValidUsernameTestLength()
        {
            LoginUsernameValidator UV = new LoginUsernameValidator("somethingusername");

            bool expected = true;
            bool actual = UV.ValidUsernameAtleast();
            Assert.AreEqual(expected, actual, "Nem ad vissza hamis értéket ha a felhasználónév elég karaktert tartalmaz!");
        }

        [TestMethod()]
        public void ValidUsernameTestLengthFalse()
        {
            LoginUsernameValidator UV = new LoginUsernameValidator("som");

            bool expected = false;
            bool actual = UV.ValidUsernameAtleast();
            Assert.AreEqual(expected, actual, "Nem ad vissza hamis értéket ha a felhasználónév nem tartalmaz elég karaktert!");
        }

        [TestMethod()]
        public void ValidUsernameTestSpace()
        {
            LoginUsernameValidator UV = new LoginUsernameValidator("something");

            bool expected = true;
            bool actual = UV.ValidUsernameWhiteSpace();
            Assert.AreEqual(expected, actual, "Nem ad vissza hamis értéket ha a felhasználónév nem tartalmaz szóközt!");
        }

        [TestMethod()]
        public void ValidUsernameTestSpaceFalse()
        {
            LoginUsernameValidator UV = new LoginUsernameValidator("some thing");

            bool expected = false;
            bool actual = UV.ValidUsernameWhiteSpace();
            Assert.AreEqual(expected, actual, "Nem ad vissza hamis értéket ha a felhasználónév tartalmaz szóközt!");
        }

        [TestMethod()]
        public void ValidUsernameTestUppercase()
        {
            LoginUsernameValidator UV = new LoginUsernameValidator("Something");

            bool expected = true;
            bool actual = UV.ValidUsernameUpperLetter();
            Assert.AreEqual(expected, actual, "Nem ad vissza hamis értéket ha a felhasználónév tartalmaz nagybetűt");
        }

        [TestMethod()]
        public void ValidUsernameTestUppercaseFalse()
        {
            LoginUsernameValidator UV = new LoginUsernameValidator("something");

            bool expected = false;
            bool actual = UV.ValidUsernameUpperLetter();
            Assert.AreEqual(expected, actual, "Nem ad vissza hamis értéket ha a felhasználónév nem tartalmaz nagybetűt!");
        }

        [TestMethod()]
        public void ValidUsernameTestLowercase()
        {
            LoginUsernameValidator UV = new LoginUsernameValidator("something");

            bool expected = true;
            bool actual = UV.ValidUsernameLowerLetter();
            Assert.AreEqual(expected, actual, "Nem ad vissza hamis értéket ha a felhasználónév tartalmaz kisbetűt!");
        }

        [TestMethod()]
        public void ValidUsernameTestLowercaseFalse()
        {
            LoginUsernameValidator UV = new LoginUsernameValidator("SOMETHING");

            bool expected = false;
            bool actual = UV.ValidUsernameLowerLetter();
            Assert.AreEqual(expected, actual, "Nem ad vissza hamis értéket ha a felhasználónév nem tartalmaz kisbetűt!");
        }
    }
}